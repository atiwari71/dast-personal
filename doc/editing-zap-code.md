# Editing ZAP code

## Overview

This document aims to outline the steps necessary to make changes to ZAP, and
how to incorporate them into DAST so they can be tested. This can be useful for:

- Testing upstream changes that you would like to propose to the ZAP team.
- Adding log statements to help you understand how ZAP works, and
- Creating a custom plugin (add-on) to extend ZAP functionality.

## Prerequisites

### DAST installed

The DAST code should be cloned from GitLab and setup on your local machine.
The [test documentation](./testing.md) describes how to install DAST and test requirements.

This step is considered complete when the command `invoke -l` lists all DAST tasks when run
from a shell.

### Install Java 11

A recommended approach is to install [SDKMAN!](https://sdkman.io) and install the latest patch of
Java 11 AdoptOpenJDK. For example:

1. Run `sdk list java` to see available versions of Java.
1. Run `sdk install java [version]` to install the desired version of Java. These instructions have been
tested with version `11.0.4.hs-adpt`.

### ZAP code downloaded and assembled

In a shell, navigate to the DAST directory. Run the command `invoke zap.project.install`.

Assuming a fast internet connection, this will take around 15 minutes to download and assemble the ZAP code base.
By default, the code will be installed to `$HOME/dev/zap`. You can choose to install to a different
directory by setting the environment variable `ZAP_PROJECT_PATH`.

## Updating a ZAP add-on

Opening the ZAP code in your favourite Java IDE will show two folders, `zaproxy` and `zap-extensions`.

1. Navigate to the add-on you would like to update in `zap-extensions/addOns/[add-on]`
1. Make the changes you desire to the add-on
1. In a shell at the DAST code directory, run `invoke zap.project.fetch-local-addon --addon [add-on]`. This will build the add-on `.zap` file and copy it to DAST.
1. Verify the ZAP file appears in the DAST `resources/zap_plugins` folder.
1. In the DAST `Dockerfile`, remove the `wget` download of the add-on you've updated. This will ensure there are not two competing add-ons with the same name.
1. Run `invoke dast.build` to rebuild the DAST Docker image. Append the option `--set-zap-and-browserker-uid` if you're developing on Linux.
1. Run DAST and test your changes.

## Creating a new add-on

Open the ZAP code in your favourite Java IDE, and follow these steps:

1. Add a new folder to `zap-extensions/addOns/[add-on]`, where `add-on` is the name of your new add-on.
1. Add a new file, `zap-extensions/addOns/[add-on]/[add-on].gradle.kts` with the following contents:

   ```gradle
   version = "1.0.0"
   description = "[enter a description]"

   zapAddOn {
       addOnName.set("[add-on]")
       zapVersion.set("2.9.0")

       manifest {
           author.set("[your-name]")
           url.set("https://www.gitlab.com")
       }
   }
   ```

1. Add a new folder, `zap-extensions/addOns/[add-on]/src/main/java/com/gitlab`
1. Add a new Java file to the folder, called `AddOnExtension.java`, with the following contents:

   ```java
   package com.gitlab;

   import org.apache.log4j.Logger;
   import org.parosproxy.paros.extension.ExtensionAdaptor;
   import org.parosproxy.paros.extension.ExtensionHook;

   public class AddOnExtension extends ExtensionAdaptor {

      private static final Logger logger = Logger.getLogger(AddOnExtension.class);

       @Override
       public void hook(ExtensionHook extensionHook) {
           super.hook(extensionHook);
           logger.info("Registering [add-on] add-on");
           extensionHook.addScannerHook(...);
       }
   }
   ```

1. Implement the rest of the functionality to your add-on.
1. Update `zap-extensions/settings.gradle.kts` and add the name of your add-on to the `addOns` list.
1. Follow the [Updating a ZAP add-on](#updating-a-zap-add-on) steps above to incorporate the add-on into DAST.

## What kind of add-ons can you create?

The `org.parosproxy.paros.extension.ExtensionHook` class contains functions that let you provide your own implementations,
for example, `addScannerHook`, `addHttpSenderListener`, `getCommandLineArgument`, etc.

Alternatively, some classes can be accessed via the singleton pattern and extended. For example, a custom spider implementation
can be added:

```java
ExtensionSpider spider = (ExtensionSpider) Control.getSingleton()
                                                  .getExtensionLoader()
                                                  .getExtension(ExtensionSpider.NAME);

if (spider != null) {
    spider.addCustomParser(new YourInterestingNewSpiderParser());
}
```

A good approach would be to explore an existing add-on that has similar functionality and seeing how it works.
