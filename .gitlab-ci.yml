stages:
  - build
  - test
  - notify
  - clean
  - release
  - post-release-test

variables:
  FF_NETWORK_PER_BUILD: 1
  MAX_IMAGE_SIZE_BYTE: 3145728000
  MAX_SCAN_DURATION_SECONDS: 66
  SAST_EXCLUDED_ANALYZERS: "eslint,semgrep"
  DS_DEFAULT_ANALYZERS: "gemnasium-python"

workflow:
  rules:
    - if: $CI_COMMIT_BRANCH

default:
  tags:
    - gitlab-org

.use-docker-in-docker:
  image: docker:20.10.5
  services:
    - docker:20.10.5-dind
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: ""
  tags: [gitlab-org-docker]

.use-docker-in-docker-python3.9:
  image: registry.gitlab.com/gitlab-org/security-products/dast/build-python3.9
  services:
    - docker:20.10.5-dind
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: ""
  tags: [gitlab-org-docker]

.invoke-job:
  image: python:3.9.1
  stage: test
  before_script:
    - apt-get update
    - apt-get install gcc
    - pip install -r requirements-test.txt

.build:
  extends: .use-docker-in-docker
  stage: build
  before_script:
    - export IMAGE=$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHA:current
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build -t $IMAGE .
    - docker push $IMAGE
    - current_size=$(docker image inspect $IMAGE --format {{.Size}})
    - echo ${current_size} >> built_image_size.txt
    - echo ${IMAGE} > built_image.txt
  artifacts:
    paths: [built_image.txt, built_image_size.txt]

build branch:
  extends: .build
  only:
    - branches
  except:
    - main

build edge:
  extends: .build
  before_script:
    - export IMAGE=$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
  after_script:
    - export BUILT_IMAGE=$(cat built_image.txt)
    - docker tag $BUILT_IMAGE $CI_REGISTRY_IMAGE:edge
    - docker push $CI_REGISTRY_IMAGE:edge
  only:
    - main

build future:
  extends: .use-docker-in-docker
  stage: build
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - export IMAGE=$CI_REGISTRY_IMAGE/future:$CI_COMMIT_SHA
    - docker build --build-arg BUILDING_FOR=future --build-arg BASE_IMAGE=$BASE_IMAGE -t $IMAGE .
    - docker push $IMAGE
    - echo ${IMAGE} > built_future_image.txt
  variables:
    BASE_IMAGE: "registry.gitlab.com/security-products/dast/browserker:1.0.0-alpha"
  artifacts:
    paths: [built_future_image.txt]
  only:
    - main

build branch future:
  extends: .use-docker-in-docker
  stage: build
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - export IMAGE=$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHA:future
    - docker build --build-arg BUILDING_FOR=future --build-arg BASE_IMAGE=$BASE_IMAGE -t $IMAGE .
    - docker push $IMAGE
    - echo ${IMAGE} > built_future_image.txt
  variables:
    BASE_IMAGE: "registry.gitlab.com/security-products/dast/browserker:1.0.0-alpha"
  artifacts:
    paths: [built_future_image.txt]
  only:
    - branches
  except:
    - main

intentionally fail:
  image: alpine
  stage: test
  script:
    - exit 1
  rules:
    - if: $CI_INTENTIONALLY_FAIL
      when: always
    - when: never

notify slack fail:
  before_script:
    - apk update && apk add git curl bash
  image: alpine
  stage: notify
  script:
    - scripts/notify-dast-team.sh "':rotating_light:' (╯°□°)╯︵┻━┻  Pipeline on \`$CI_BUILD_REF_NAME\` failed! Commit \`$(git log -1 --oneline | sed 's|\"|\\\\\"|g')\` See <https://gitlab.com/gitlab-org/security-products/dast/commit/"$CI_BUILD_REF"/pipelines>"
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
      when: on_failure
    - if: $CI_INTENTIONALLY_FAIL
      when: on_failure
    - when: never

clean registry:
  extends: .use-docker-in-docker
  stage: clean
  before_script:
    - apk add bash curl jq
  script:
    - ./scripts/clean-registry.sh
  when: always
  except:
    - main

check image size:
  stage: test
  image: alpine:3.9
  script:
    - export CURRENT_IMAGE_SIZE=$(cat built_image_size.txt)
    - echo $CURRENT_IMAGE_SIZE
    - echo $MAX_IMAGE_SIZE_BYTE
    - test $MAX_IMAGE_SIZE_BYTE -gt $CURRENT_IMAGE_SIZE
  allow_failure: true

danger-review:
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:danger
  stage: test
  only:
    - branches
  except:
    refs:
      - main
    variables:
      - $DANGER_DISABLED == "true"
  variables:
    DANGER_BOT_DAST_FILE_URL: https://gitlab.com/gitlab-org/security-products/danger-bot/-/raw/v0.0.5/dast/Dangerfile
    DANGER_BOT_META_DATA_FILES_URL: https://gitlab.com/gitlab-org/security-products/danger-bot/-/raw/v0.10.0/metadata/Dangerfile
    DANGER_BOT_CHANGELOG_FILES_URL: https://gitlab.com/gitlab-org/security-products/danger-bot/-/raw/v0.0.5/changelog/Dangerfile
    DANGER_BOT_CHANGES_SIZE_FILES_URL: https://gitlab.com/gitlab-org/security-products/danger-bot/-/raw/v0.0.5/changes_size/Dangerfile
  script:
    - git version
    - wget $DANGER_BOT_DAST_FILE_URL -P .
    - wget $DANGER_BOT_META_DATA_FILES_URL -P danger/metadata/
    - wget $DANGER_BOT_CHANGELOG_FILES_URL -P danger/changelog/
    - wget $DANGER_BOT_CHANGES_SIZE_FILES_URL -P danger/changes_size/
    - danger

unit-test:
  extends: .invoke-job
  script: [invoke test.unit]

integration-test:
  extends: .invoke-job
  script:
    - apt-get --no-install-recommends --assume-yes install default-jre
    - invoke test.integration

zap-addons-up-to-date:
  extends: .use-docker-in-docker-python3.9
  stage: test
  before_script:
    - apk add bash curl jq gcc g++
    - pip install -r requirements.txt
    - pip install invoke
  script:
    - export BUILT_IMAGE=$(cat built_image.txt)
    - ./scripts/identify-addon-updates.sh -i

code-style:
  image: python:3.9.1
  stage: test
  script:
    - apt-get update
    - apt-get install shellcheck gcc jq --assume-yes
    - pip install -r requirements-test.txt
    - ./test/test-code-style.sh

mypy:
  extends: .invoke-job
  script: [invoke lint.typing-progress]
  artifacts:
    reports:
      metrics: artifacts/metrics.txt

markdown-lint:
  image: "registry.gitlab.com/gitlab-org/gitlab-docs:lint"
  stage: test
  script:
    - markdownlint --config .markdownlint.json **/*.md

.end-to-end-test:
  extends: .use-docker-in-docker-python3.9
  stage: test
  before_script:
    - apk add bash curl gcc libxml2-dev libxslt-dev musl-dev g++
    - pip install -r requirements-test.txt
    - bash -c "bash <(curl -s https://raw.githubusercontent.com/pgrange/bash_unit/master/install.sh)"
    - mv bash_unit /usr/local/bin
  artifacts:
    paths:
      - test/end-to-end/output
    when: always

end-to-end-test 1/6:
  extends: .end-to-end-test
  script:
    - export BUILT_IMAGE=$(cat built_image.txt)
    - bash_unit ./test/end-to-end/test-ajax-spider.sh ./test/end-to-end/test-baseline.sh ./test/end-to-end/test-cli-options.sh ./test/end-to-end/test-api-scan.sh ./test/end-to-end/test-offline-scans.sh ./test/end-to-end/test-license-check.sh ./test/end-to-end/test-requests-are-not-tagged-by-zap.sh

end-to-end-test 2/6:
  extends: .end-to-end-test
  script:
    - export BUILT_IMAGE=$(cat built_image.txt)
    - bash_unit ./test/end-to-end/test-browserker.sh ./test/end-to-end/test-browserker-exclude-urls.sh ./test/end-to-end/test-browserker-authenticated-scan.sh

end-to-end-test 3/6:
  extends: .end-to-end-test
  script:
    - export BUILT_IMAGE=$(cat built_image.txt)
    - bash_unit ./test/end-to-end/test-url-scan.sh ./test/end-to-end/test-authenticated-scan.sh ./test/end-to-end/test-non-zap-user.sh ./test/end-to-end/test-full-scan.sh
  artifacts:
    reports:
      metrics: profiling/mprofile-test_webgoat_full_scan.txt
    paths:
      - profiling/mprofile-test_webgoat_full_scan.png
      - profiling/mprofile-test_webgoat_full_scan.dat
      - test/end-to-end/output

end-to-end-test 4/6:
  extends: .end-to-end-test
  script:
    - export BUILT_IMAGE=$(cat built_image.txt)
    - bash_unit ./test/end-to-end/test-merge-secure-report.sh ./test/end-to-end/test-rule-selection.sh ./test/end-to-end/test-browserker-login-path.sh

end-to-end-test 5/6:
  extends: .end-to-end-test
  script:
    - export BUILT_IMAGE=$(cat built_image.txt)
    - bash_unit ./test/end-to-end/test-mutual-tls.sh

end-to-end-test future:
  extends: .end-to-end-test
  script:
    - export BUILT_IMAGE=$(cat built_future_image.txt)
    - bash_unit ./test/end-to-end/test-future-baseline.sh ./test/end-to-end/test-future-full-scan.sh

# This is a long running test, so only run it on main.
end-to-end-test 6/6:
  extends: .end-to-end-test
  script:
    - export BUILT_IMAGE=$(cat built_image.txt)
    - bash_unit ./test/end-to-end/test-memory-spikes.sh
  only:
    - main

release:
  extends: .use-docker-in-docker
  stage: release
  allow_failure: false
  before_script:
    - apk add bash curl jq nodejs
  script:
    - ./scripts/release.sh
  when: manual
  only:
    - main

regenerate-e2e-expected-json:
  extends: .end-to-end-test
  stage: test
  allow_failure: true
  script:
    - apk add git
    - invoke test.regenerate-e2e-expected-json
    - git diff > regenerate-e2e-expected-json.patch
  artifacts:
    paths:
      - regenerate-e2e-expected-json.patch
  when: manual
  only:
    - branches

post-release-test:
  stage: post-release-test
  trigger:
    project: gitlab-org/security-products/tests/dast-e2e
    branch: main
    strategy: depend
  only:
    - main

post-release-test:
  stage: post-release-test
  trigger:
    project: gitlab-org/security-products/tests/dast-e2e
    branch: test-dast-latest-gitlab-ci-yml
    strategy: depend
  only:
    - main

post-release-benchmark:
  stage: post-release-test
  allow_failure: true
  trigger:
    project: gitlab-org/security-products/dast-benchmark
    branch: master
    strategy: depend
  only:
    - main

include:
  - template: Container-Scanning.gitlab-ci.yml
  - template: SAST.gitlab-ci.yml
  - template: Dependency-Scanning.gitlab-ci.yml
  - template: Code-Quality.gitlab-ci.yml
  - template: License-Scanning.gitlab-ci.yml
  - template: Secret-Detection.gitlab-ci.yml

code_quality:
  tags:
    - gitlab-org-docker

container_scanning:
  dependencies:
  before_script:
     - export DOCKER_IMAGE=$(cat built_image.txt)
