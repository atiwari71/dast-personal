#!/usr/bin/env bash

mkdir -p /zap/wrk

# Tail log files
truncate -s 0 zap.out
tail -f zap.out | sed "s/^/[zap_server] /" &

if [ -n "$GITLAB_FEATURES" ] && [[ "$GITLAB_FEATURES" != *"dast"* ]]; then
    echo "Error: Your GitLab project is not licensed for DAST."
    exit 1
fi

# Prepare chromium for use by ZAP Crawljax
export CHROMIUM_FLAGS="--no-sandbox --disable-dev-shm-usage --disable-client-side-phishing-detection --disable-component-update --disable-infobars --disable-ntp-popular-sites --disable-ntp-most-likely-favicons-from-server --disable-sync-app-list --disable-domain-reliability --disable-background-networking --disable-sync --disable-new-browser-first-run --disable-default-apps --disable-popup-blocking --disable-features=TranslateUI --disable-gpu --no-first-run --safebrowsing-disable-auto-update --safebrowsing-disable-download-protection"

if [ "$DAST_DEBUG" == "true" ] || [ "$DAST_DEBUG" == "TRUE" ] || [ "$DAST_DEBUG" == "1" ]; then
    export PYTHONFAULTHANDLER="true"
fi

if [ "$DAST_PYTHON_MEMORY_PROFILE_REPORT" ]; then
    /app/scripts/profile-memory-usage "$@"
else
    /app/analyze.py "$@"
fi

EXIT_CODE=$?

# Copy DAST and ZAP artifacts to /output
cp -r /zap/wrk/* . >/dev/null 2>&1

# Cleanup background processes. Needed for Gitlab runner running on Kubernetes
pkill tail

exit $EXIT_CODE
