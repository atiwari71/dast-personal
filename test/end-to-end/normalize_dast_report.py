#!/usr/bin/env python
import functools
import json
import re
import sys

if len(sys.argv) != 2:
    print('Usage: ./normalize_dast_report.py <path-to-json>')
    exit(1)


# dig into the dictionary, to return the last object and the field name
def dig(obj, *fields):
    if len(fields) == 0:
        raise RuntimeError('must supply a field')

    if len(fields) == 1:
        return obj, fields[0]

    base_obj = functools.reduce(lambda nxt, field_name: nxt.get(field_name, {}), fields[:-1], obj)
    field = fields[-1]
    return base_obj, field


def dig_set_value(obj, value, *fields):
    base_obj, field = dig(obj, *fields)
    base_obj[field] = value


def dig_regexp_replace(obj, regex, value, num, *fields):
    base_obj, field = dig(obj, *fields)
    base_obj[field] = re.sub(regex, value, base_obj[field], num)


with open(sys.argv[1]) as json_file:
    report = json.loads(json_file.read())
    report.pop('@generated', None)
    report.pop('@version', None)

    report['scan']['analyzer']['version'] = '__REMOVED__'
    report['scan']['start_time'] = '__REMOVED__'
    report['scan']['end_time'] = '__REMOVED__'

    for vulnerability in report['vulnerabilities']:
        if 'id' in vulnerability:
            del vulnerability['id']

        if 'discovered_at' in vulnerability:
            del vulnerability['discovered_at']

        dig_set_value(vulnerability, [], 'evidence', 'request', 'headers')
        dig_set_value(vulnerability, [], 'evidence', 'response', 'headers')
        dig_regexp_replace(vulnerability,
                           r'Expires=.+?;',
                           'Expires=__REMOVED__;',
                           2,
                           'evidence', 'summary')

    sys.stdout.write(json.dumps(report, indent=2, sort_keys=True))
