#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

BUILT_IMAGE=${BUILT_IMAGE:-dast}
MAX_SCAN_DURATION_SECONDS=${MAX_SCAN_DURATION_SECONDS:-66}

# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

setup_suite() {
  setup_test_dependencies
  run_basic_site
  true
}

teardown_suite() {
  docker rm --force nginx  >/dev/null 2>&1
  docker rm --force opt-param-test >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_merge_browserker_report() {
  docker run --rm \
    -v "${PWD}":/output \
    -v "${PWD}/fixtures/scripts":/home/zap/custom-scripts \
    --network test \
    --env DAST_BROWSER_SCAN=true \
    "${BUILT_IMAGE}" /analyze -d --ff-browser-passive-scan-mode false -t http://nginx >output/test_merge_browserker_report.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < gl-dast-report.json > output/report_test_merge_browserker_report.json

  diff -u <(./normalize_dast_report.py expect/test_merge_browserker_report.json) \
          <(./normalize_dast_report.py output/report_test_merge_browserker_report.json)
  assert_equals "0" "$?" "Analyze results differ from expectations"

  ./verify-dast-schema.py output/report_test_merge_browserker_report.json
}
