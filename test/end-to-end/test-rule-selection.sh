#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

BUILT_IMAGE=${BUILT_IMAGE:-dast}

# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

setup_suite() {
  setup_test_dependencies
  run_basic_site
  true
}

teardown_suite() {
  docker rm --force nginx  >/dev/null 2>&1
  docker rm --force opt-param-test >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_includes_only_configured_rules() {
  docker run --rm \
    -v "${PWD}":/output \
    --network test \
    --env DAST_DEBUG=1 \
    --env DAST_ONLY_INCLUDE_RULES=10020,10202 \
    --env DAST_WEBSITE=http://nginx \
    "${BUILT_IMAGE}" /analyze >output/test_includes_only_configured_rules.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  grep -q 'WARN: Anti-clickjacking Header \[10020\]' output/test_includes_only_configured_rules.log && \
  grep -q 'WARN: Absence of Anti-CSRF Tokens \[10202\]' output/test_includes_only_configured_rules.log
  assert_equals "0" "$?" "Rules were not included as intended"

  grep -q 'SKIP: X-Content-Type-Options Header Missing \[10021\]' output/test_includes_only_configured_rules.log
  assert_equals "0" "$?" "Rules were not skipped as intended"
}
