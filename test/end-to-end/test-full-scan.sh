#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

BUILT_IMAGE=${BUILT_IMAGE:-dast}

setup_suite() {
  setup_test_dependencies
  docker network create test >/dev/null
  setup_webgoat

  true
}

teardown_suite() {
  docker rm -f goat vulnerabletestserver >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_webgoat_full_scan() {
  # /logout is excluded because it invalidates the session, causing many pages to not be spidered
  # css files are excluded to make the test run faster
  # bootstrap is removed because it causes an intermittent vulnerability result
  # Don't fuzz the Agent header (10104)
  # Don't check for suspicious comments (too many fuzzing options)
  docker run --rm \
    --env DAST_FULL_SCAN_ENABLED=true \
    --env DAST_EXCLUDE_RULES=10104,10027,20012,10031,90033 \
    --env DAST_PYTHON_MEMORY_PROFILE_REPORT=mprofile-test_webgoat_full_scan \
    -v "${PWD}":/output \
    -v "${PWD}/../../profiling":/output/profiling \
    --network test \
    "${BUILT_IMAGE}" /analyze -d -j -t http://vulnerabletestserver/WebGoat/attack \
    --auth-url http://vulnerabletestserver/WebGoat/login \
    --auth-username "someone" \
    --auth-password "P@ssw0rd" \
    --auth-username-field "id:exampleInputEmail1" \
    --auth-password-field "id:exampleInputPassword1" \
    --auth-submit-field "button[type=submit]" \
    --auth-exclude-urls 'http://vulnerabletestserver/WebGoat/js/respond.min.js,http://vulnerabletestserver/.*.css,http://vulnerabletestserver/WebGoat/plugins/bootstrap/css/bootstrap.min.css' \
    --auth-verification-url 'http://vulnerabletestserver/WebGoat/start.mvc#lesson/WebGoatIntroduction.lesson' \
    >output/test_webgoat_full_scan.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < gl-dast-report.json > output/report_test_webgoat_full_scan.json

  diff -u <(./normalize_dast_report.py expect/test_webgoat_full_scan.json) \
          <(./normalize_dast_report.py gl-dast-report.json)
  assert_equals "0" "$?" "Analyze results differ from expectations"

  grep -q 'The following [0-9]* URLs were scanned' output/test_webgoat_full_scan.log
  assert_equals "0" "$?" "Logged output missing scanned URLs count"
  grep -q 'Active scan rule SQL Injection \[40018\] \[Complete\] ran' output/test_webgoat_full_scan.log
  assert_equals "0" "$?" "Logged output missing active scan rule"

  grep -q 'SKIP: Anti-CSRF Tokens Check \[20012\]' output/test_webgoat_full_scan.log && \
  grep -q 'SKIP: User Agent Fuzzer \[10104\]' output/test_webgoat_full_scan.log && \
  grep -q 'SKIP: Information Disclosure - Suspicious Comments \[10027\]' output/test_webgoat_full_scan.log && \
  grep -q 'SKIP: Timestamp Disclosure \[10096\]' output/test_webgoat_full_scan.log
  assert_equals "0" "$?" "Rules were not skipped as intended"

  ./verify-dast-schema.py output/report_test_webgoat_full_scan.json
  assert_equals "0" "$?" "DAST report does not conform to DAST schema"
}
