#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

BUILT_IMAGE=${BUILT_IMAGE:-dast}

setup_suite() {
  setup_test_dependencies

  docker network create test >/dev/null

  docker run \
  --name djangopoll \
  --network test \
  -v "${PWD}/fixtures/djangopoll":/app/djangopoll \
  -v "${PWD}/../../requirements.txt":/app/requirements.txt \
  -v "${PWD}/../../requirements-test.txt":/app/requirements-test.txt \
  -d \
  python:3.9.1 \
  bash -c "cd /app && pip install -r requirements-test.txt && cd djangopoll && rm -f db.sqlite3 && ./manage.py migrate && ./manage.py runserver 0:8000" \
  >/dev/null 2>&1

  true
}

teardown_suite() {
  docker rm --force djangopoll >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_browserker_login_path() {
  # deliberately exclude DAST_SUBMIT_FIELD
  docker run --rm \
    -v "${PWD}":/output \
    --network test \
    --env DAST_BROWSER_SCAN=true \
    --env DAST_AUTH_URL="http://djangopoll:8000" \
    --env DAST_USERNAME="user" \
    --env DAST_PASSWORD="password" \
    --env DAST_BROWSER_PATH_TO_LOGIN_FORM=".login-form" \
    --env DAST_USERNAME_FIELD="[name=username]" \
    --env DAST_PASSWORD_FIELD="[name=password]" \
    "${BUILT_IMAGE}" \
    /analyze -d -t http://djangopoll:8000 >output/test_browserker_login_path.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  sed -E 's/csrftoken=[a-zA-Z0-9]+/csrftoken=__REMOVED__/g' gl-dast-report.json > temp.json && mv temp.json gl-dast-report.json

  jq . < gl-dast-report.json > output/report_test_browserker_login_path.json

  diff -u <(./normalize_dast_report.py expect/test_browserker_login_path.json) \
          <(./normalize_dast_report.py output/report_test_browserker_login_path.json)
  assert_equals "0" "$?" "Analyze results differ from expectations"
}
