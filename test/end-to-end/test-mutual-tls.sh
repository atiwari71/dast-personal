#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

BUILT_IMAGE=${BUILT_IMAGE:-dast}

# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

setup_suite() {
  setup_test_dependencies
  run_mutual_tls_site
  true
}

teardown_suite() {
  docker rm --force nginx  >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_mutual_tls() {
  docker run --rm \
    -v "${PWD}":/output \
    --env DAST_PKCS12_CERTIFICATE_BASE64="$(cat "${PWD}/fixtures/mutual-tls/certs/client.pfx.base64")" \
    --env DAST_PKCS12_PASSWORD="dast" \
    --network test \
    "${BUILT_IMAGE}" /analyze -d -t https://nginx >output/test_mutual_tls.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < gl-dast-report.json > output/report_test_mutual_tls.json

  diff -u <(./normalize_dast_report.py expect/test_mutual_tls.json) \
          <(./normalize_dast_report.py gl-dast-report.json)
  assert_equals "0" "$?" "Analyze results differ from expectations"

  ./verify-dast-schema.py output/report_test_mutual_tls.json
  assert_equals "0" "$?" "DAST report does not conform to DAST schema"
}
