#!/usr/bin/env bash

set -e

setup_test_dependencies() {
  mkdir -p output

  # install jq if not present
  command -v jq >/dev/null || apk add jq
}

run_basic_site() {
  docker network create test >/dev/null

  docker run \
    --name nginx \
    -v "${PWD}/fixtures/basic-site":/usr/share/nginx/html:ro \
    -v "${PWD}/fixtures/basic-site/nginx.conf":/etc/nginx/conf.d/default.conf \
    -v "${PWD}/../unit/fixtures/certificates/self-signed.crt":/etc/nginx/self-signed.crt \
    -v "${PWD}/../unit/fixtures/certificates/self-signed.key":/etc/nginx/self-signed.key \
    --network test -d nginx:1.17.6 >/dev/null
}

run_ajax_spider_site() {
    docker network create test >/dev/null

    docker run --rm \
    -v "${PWD}/fixtures/ajax-spider":/usr/share/nginx/html:ro \
    -v "${PWD}/fixtures/ajax-spider/nginx.conf":/etc/nginx/conf.d/default.conf \
    --name nginx \
    --network test \
    -d nginx:1.19.7 >/dev/null
  true
}

run_multi_page_site() {
  docker network create test >/dev/null

  docker run \
    --name nginx \
    -v "${PWD}/fixtures/basic-multi-page-site":/usr/share/nginx/html:ro \
    -v "${PWD}/fixtures/basic-multi-page-site/nginx.conf":/etc/nginx/conf.d/default.conf \
    -v "${PWD}/../unit/fixtures/certificates/self-signed.crt":/etc/nginx/self-signed.crt \
    -v "${PWD}/../unit/fixtures/certificates/self-signed.key":/etc/nginx/self-signed.key \
    --network test -d nginx:1.17.6 >/dev/null
}

run_mutual_tls_site() {
  docker network create test >/dev/null

  docker run --rm \
    --name nginx \
    -v "${PWD}/fixtures/mutual-tls/src":/usr/share/nginx/html:ro \
    -v "${PWD}/fixtures/mutual-tls/nginx.conf":/etc/nginx/conf.d/default.conf \
    -v "${PWD}/fixtures/mutual-tls/certs":/etc/nginx/certs \
    --network test -d nginx:1.17.6 >/dev/null
}

setup_webgoat() {
  docker run --rm \
    -v "${PWD}/fixtures/webgoat-8.0.0.M21":/home/webgoat/.webgoat-8.0.0.M21 \
    --name goat \
    --network test \
    -d \
    registry.gitlab.com/gitlab-org/security-products/dast/webgoat-8.0@sha256:bc09fe2e0721dfaeee79364115aeedf2174cce0947b9ae5fe7c33312ee019a4e >/dev/null

  # start nginx container to use as proxy for domain validation
  docker run --rm \
    -v "${PWD}/fixtures/domain-validation/nginx.conf":/etc/nginx/conf.d/default.conf \
    --name vulnerabletestserver \
    --network test \
    -d \
    nginx:1.17.6-alpine >/dev/null
}
