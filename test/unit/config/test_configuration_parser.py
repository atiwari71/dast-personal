from unittest import TestCase
from unittest.mock import patch

import yaml

from src.config import InvalidConfigurationError
from src.config.configuration_parser import ConfigurationParser
from src.config.gitlab_exclude_rules import EXCLUDE_RULES_YML
from src.models import APIScanPolicy
from test.unit import utilities


def merge(a, b):
    result = b.copy()
    result.update(a)
    return result


class TestConfigurationParser(TestCase):
    DEFAULT_ENV = {'DAST_WEBSITE': 'http://website'}
    DEFAULT_AUTH_ENV = merge({
        'DAST_AUTH_URL': 'http://website/login',
        'DAST_USERNAME': 'username',
        'DAST_PASSWORD': 'my_password'}, DEFAULT_ENV)

    def setUp(self) -> None:
        self.system_patcher = patch('src.config.configuration_parser.System')
        self.mock_system = self.system_patcher.start()
        self.mock_system.return_value.dast_version.return_value = '1.6.2'
        self.parser = ConfigurationParser()

        with open(EXCLUDE_RULES_YML) as exclude_rules_file:
            exclude_rules = yaml.safe_load(exclude_rules_file)

            self.exclude_rules = [str(rule['rule_id']) for rule in exclude_rules['exclude_rules']]

    def tearDown(self):
        self.system_patcher.stop()

    def test_can_set_target_using_option(self):
        config = self.parser.parse(['-t', 'http://website'], {})
        self.assertEqual(config.target, 'http://website')

    def test_can_set_target_using_environment(self):
        config = self.parser.parse([], {'DAST_WEBSITE': 'http://website'})
        self.assertEqual(config.target, 'http://website')

    def test_raises_error_when_target_not_a_valid_url(self):
        with self.assertRaises(SystemExit) as system_exit, utilities.capture_output() as output:
            self.parser.parse(['-t', 'not a URL'], {})

        self.assertEqual(system_exit.exception.code, 2)
        self.assertIn('error: argument -t: not a URL is not a valid URL', output.stderr.getvalue())

    def test_can_set_spider_start_to_host_using_option(self):
        config = self.parser.parse(['-t', 'http://website', '--spider-start-at-host', 'False'], {})
        self.assertFalse(config.spider_start_at_host)

    def test_can_set_spider_start_to_host_using_environment(self):
        config = self.parser.parse(['-t', 'http://website'], {'DAST_SPIDER_START_AT_HOST': 'False'})
        self.assertFalse(config.spider_start_at_host)

    def test_spider_start_at_host_defaults_to_false_in_dast_2(self):
        with patch('src.config.configuration_parser.path') as path:
            path.isfile.return_value = False
            self.mock_system.return_value.dast_version.return_value = '2.6.1'

            config = self.parser.parse([], self.DEFAULT_ENV)

        self.assertFalse(config.spider_start_at_host)

    def test_aggregate_vulnerabilities_defaults_to_true(self):
        config = self.parser.parse(['-t', 'http://website'], {})
        self.assertTrue(config.aggregate_vulnerabilities)

    def test_can_set_aggregate_vulnerabilities_using_option(self):
        config = self.parser.parse(['-t', 'http://website', '--aggregate-vulnerabilities', 'False'], {})
        self.assertFalse(config.aggregate_vulnerabilities)

    def test_can_set_aggregate_vulnerabilities_using_environment(self):
        config = self.parser.parse(['-t', 'http://website'],
                                   {'DAST_AGGREGATE_VULNERABILITIES': 'False'})
        self.assertFalse(config.aggregate_vulnerabilities)

    def test_max_urls_per_vulnerability_defaults_to_50(self):
        config = self.parser.parse(['-t', 'http://website'], {})
        self.assertEqual(config.max_urls_per_vulnerability, 50)

    def test_can_set_max_urls_per_vulnerability_using_option(self):
        config = self.parser.parse(['-t', 'http://website', '--max-urls-per-vulerability', '10'], {})
        self.assertEqual(config.max_urls_per_vulnerability, 10)

    def test_can_set_max_urls_per_vulnerability_using_environment(self):
        config = self.parser.parse(['-t', 'http://website'], {'DAST_MAX_URLS_PER_VULNERABILITY': '10'})
        self.assertEqual(config.max_urls_per_vulnerability, 10)

    def test_max_urls_per_vulnerability_can_only_be_a_positive_int(self):
        with self.assertRaises(SystemExit) as system_exit, utilities.capture_output() as output:
            self.parser.parse(['-t', 'http://website', '--max-urls-per-vulerability', '-1'], {})

        self.assertEqual(system_exit.exception.code, 2)
        self.assertIn('cannot be negative', output.stderr.getvalue())

    def test_can_set_paths_to_scan_file_path_using_environment(self):
        with patch('src.config.configuration_parser.URLScanConfigurationParser.parse',
                   return_value=['http://website/1']):
            config = self.parser.parse(['-t', 'http://website'],
                                       {'DAST_PATHS_FILE': 'test/end-to-end/fixtures/url-scan/paths_to_scan.txt'})

        self.assertEqual(config.urls_to_scan, ['http://website/1'])

    def test_can_set_paths_to_scan_file_path_using_option(self):
        with patch('src.config.configuration_parser.URLScanConfigurationParser.parse',
                   return_value=['http://website/1']):
            config = self.parser.parse(['-t', 'http://website',
                                        '--paths-to-scan-file',
                                        'test/end-to-end/fixtures/url-scan/paths_to_scan.txt'], {})

        self.assertEqual(config.urls_to_scan, ['http://website/1'])

    def test_can_set_paths_to_scan_using_enviroment(self):
        config = self.parser.parse(['-t', 'http://website'],
                                   {'DAST_PATHS': '/,/1,/2?arg=1'})
        self.assertEqual(config.urls_to_scan, ['http://website/', 'http://website/1', 'http://website/2?arg=1'])
        self.assertEqual(config.paths_to_scan_list, ['/', '/1', '/2?arg=1'])

    def test_can_set_paths_to_scan_using_option(self):
        config = self.parser.parse(['-t', 'http://website',
                                    '--paths-to-scan', '/,/1,/2'], {})
        self.assertEqual(config.urls_to_scan, ['http://website/', 'http://website/1', 'http://website/2'])

    def test_urls_to_scan_are_correctly_built_when_target_ends_in_slash(self):
        config = self.parser.parse(['-t', 'http://website/',
                                    '--paths-to-scan', '/,/1,/2'], {})
        self.assertEqual(config.urls_to_scan, ['http://website/', 'http://website/1', 'http://website/2'])

    def test_urls_to_scan_are_correctly_built_when_path_does_not_start_with_slash(self):
        config = self.parser.parse(['-t', 'http://website',
                                    '--paths-to-scan', 'page1,page2'], {})
        self.assertEqual(config.urls_to_scan, ['http://website/page1', 'http://website/page2'])

    def test_urls_to_scan_are_an_empty_list_by_default(self):
        config = self.parser.parse(['-t', 'http://website'], {})
        self.assertEqual(config.urls_to_scan, [])

    def test_urls_to_scan_error_when_url_is_invalid(self):
        with self.assertRaises(SystemExit) as system_exit, utilities.capture_output() as output:
            self.parser.parse(['-t', '//website',
                               '--paths-to-scan', '$[page1],page2'], {})

        self.assertEqual(system_exit.exception.code, 2)
        self.assertIn('error: argument -t: //website is not a valid URL', output.stderr.getvalue())

    def test_white_space_is_removed_from_urls_to_scan(self):
        config = self.parser.parse(['-t', 'http://website',
                                    '--paths-to-scan', ' /,  /1,/2  '], {})
        self.assertEqual(config.urls_to_scan, ['http://website/', 'http://website/1', 'http://website/2'])

    def test_option_value_overrides_environment(self):
        config = self.parser.parse(['-t', 'http://website'], {'DAST_WEBSITE': 'http://another.website'})
        self.assertEqual(config.target, 'http://website')

    def test_can_set_api_specification_using_option(self):
        config = self.parser.parse(['--api-specification', 'http://website/api.spec'], {})
        self.assertEqual(config.api_specification, 'http://website/api.spec')
        self.assertTrue(config.is_api_scan)

    def test_can_set_api_specification_using_environment_alias(self):
        config = self.parser.parse([], {'DAST_API_SPECIFICATION': 'http://website/api.spec'})
        self.assertEqual(config.api_specification, 'http://website/api.spec')
        self.assertTrue(config.is_api_scan)

    def test_cannot_set_api_specification_using_DAST_API_SPECIFICATION_after_DAST_3(self):
        self.mock_system.return_value.dast_version.return_value = '3.0.0'

        with self.assertRaises(InvalidConfigurationError):
            self.parser.parse([], {'DAST_API_SPECIFICATION': 'http://website/api.spec'})

    def test_can_set_api_specification_using_DAST_API_OPENAPI(self):
        config = self.parser.parse([], {'DAST_API_OPENAPI': 'http://website/api.spec'})
        self.assertEqual(config.api_specification, 'http://website/api.spec')
        self.assertTrue(config.is_api_scan)

    def test_is_not_api_scan_when_api_format_not_specified(self):
        config = self.parser.parse(['-t', 'http://website'], {})
        self.assertFalse(config.is_api_scan)

    def test_domain_override_can_be_set_using_option(self):
        config = self.parser.parse(['-O', 'my.host.com', '--api-specification', 'http://api.spec'], {})
        self.assertEqual(config.zap_api_host_override, 'my.host.com')

    def test_domain_override_can_be_set_using_environment(self):
        config = self.parser.parse([], merge({'DAST_API_HOST_OVERRIDE': 'my.host.com'}, self.DEFAULT_ENV))
        self.assertEqual(config.zap_api_host_override, 'my.host.com')

    def test_can_not_set_invalid_url_as_auth_url(self):
        with self.assertRaises(SystemExit) as system_exit, utilities.capture_output() as output:
            self.parser.parse(['--auth-url', 'loremipsum'], {})

        self.assertEqual(system_exit.exception.code, 2)
        self.assertIn('argument --auth-url: loremipsum is not a valid URL', output.stderr.getvalue())

    def test_can_set_auth_url_using_option(self):
        config = self.parser.parse(['--auth-url', 'http://website'], self.DEFAULT_AUTH_ENV)
        self.assertEqual(config.auth_url, 'http://website')

    def test_can_set_auth_url_using_environment(self):
        config = self.parser.parse([], merge({'DAST_AUTH_URL': 'http://my.website'}, self.DEFAULT_AUTH_ENV))
        self.assertEqual(config.auth_url, 'http://my.website')

    def test_can_set_auth_url_using_legacy_environment(self):
        config = self.parser.parse([], merge({'AUTH_URL': 'http://website'}, self.DEFAULT_AUTH_ENV))
        self.assertEqual(config.auth_url, 'http://website')

    def test_can_not_set_invalid_url_as_auth_verification_url(self):
        with self.assertRaises(SystemExit) as system_exit, utilities.capture_output() as output:
            self.parser.parse(['--auth-verification-url', 'loremipsum'], {})

        self.assertEqual(system_exit.exception.code, 2)
        self.assertIn('argument --auth-verification-url: loremipsum is not a valid URL', output.stderr.getvalue())

    def test_can_set_auth_verification_url_using_option(self):
        config = self.parser.parse(['--auth-verification-url', 'http://website'], self.DEFAULT_ENV)
        self.assertEqual(config.auth_verification_url, 'http://website')

    def test_can_set_auth_verification_url_using_environment(self):
        config = self.parser.parse([], merge({'DAST_AUTH_VERIFICATION_URL': 'http://website'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_verification_url, 'http://website')

    def test_can_set_auth_username_using_option(self):
        config = self.parser.parse(['--auth-username', 'username'], self.DEFAULT_AUTH_ENV)
        self.assertEqual(config.auth_username, 'username')

    def test_can_set_auth_username_using_environment(self):
        config = self.parser.parse([], merge({'DAST_USERNAME': 'username'}, self.DEFAULT_AUTH_ENV))
        self.assertEqual(config.auth_username, 'username')

    def test_can_set_auth_username_using_legacy_environment(self):
        config = self.parser.parse([], merge({'AUTH_USERNAME': 'username'}, self.DEFAULT_AUTH_ENV))
        self.assertEqual(config.auth_username, 'username')

    def test_can_set_auth_password_using_option(self):
        config = self.parser.parse(['--auth-password', 'password'], self.DEFAULT_AUTH_ENV)
        self.assertEqual(config.auth_password, 'password')

    def test_can_set_auth_password_using_environment(self):
        config = self.parser.parse([], merge({'DAST_PASSWORD': 'password'}, self.DEFAULT_AUTH_ENV))
        self.assertEqual(config.auth_password, 'password')

    def test_can_set_auth_password_using_legacy_environment(self):
        config = self.parser.parse([], merge({'AUTH_PASSWORD': 'password'}, self.DEFAULT_AUTH_ENV))
        self.assertEqual(config.auth_password, 'password')

    def test_can_set_username_field_using_option(self):
        config = self.parser.parse(['--auth-username-field', 'username'], self.DEFAULT_AUTH_ENV)
        self.assertEqual(config.auth_username_field, 'username')

    def test_can_set_username_field_using_environment(self):
        config = self.parser.parse([], merge({'DAST_USERNAME_FIELD': 'username'}, self.DEFAULT_AUTH_ENV))
        self.assertEqual(config.auth_username_field, 'username')

    def test_can_set_username_field_using_legacy_environment(self):
        config = self.parser.parse([], merge({'AUTH_USERNAME_FIELD': 'username'}, self.DEFAULT_AUTH_ENV))
        self.assertEqual(config.auth_username_field, 'username')

    def test_can_set_password_field_using_option(self):
        config = self.parser.parse(['--auth-password-field', 'password'], self.DEFAULT_AUTH_ENV)
        self.assertEqual(config.auth_password_field, 'password')

    def test_can_set_password_field_using_environment(self):
        config = self.parser.parse([], merge({'DAST_PASSWORD_FIELD': 'password'}, self.DEFAULT_AUTH_ENV))
        self.assertEqual(config.auth_password_field, 'password')

    def test_can_set_password_field_using_legacy_environment(self):
        config = self.parser.parse([], merge({'AUTH_PASSWORD_FIELD': 'password'}, self.DEFAULT_AUTH_ENV))
        self.assertEqual(config.auth_password_field, 'password')

    def test_fail_if_authentication_setup_without_url(self):
        auth_settings = {'DAST_USERNAME': 'username', 'DAST_PASSWORD': 'password'}

        with self.assertRaises(InvalidConfigurationError) as error:
            self.parser.parse([], merge(auth_settings, self.DEFAULT_ENV))

        self.assertIn('authentication misconfigured, login URL has not been set.', str(error.exception))

    def test_fail_if_authentication_setup_without_username(self):
        auth_settings = {'DAST_AUTH_URL': 'http://website/login', 'DAST_PASSWORD': 'password'}

        with self.assertRaises(InvalidConfigurationError) as error:
            self.parser.parse([], merge(auth_settings, self.DEFAULT_ENV))

        self.assertIn('authentication misconfigured, username has not been set.', str(error.exception))

    def test_fail_if_authentication_setup_without_password(self):
        auth_settings = {'DAST_AUTH_URL': 'http://website/login', 'DAST_USERNAME': 'username'}

        with self.assertRaises(InvalidConfigurationError) as error:
            self.parser.parse([], merge(auth_settings, self.DEFAULT_ENV))

        self.assertIn('authentication misconfigured, password has not been set.', str(error.exception))

    def test_can_set_auth_submit_field_using_option(self):
        config = self.parser.parse(['--auth-submit-field', 'field'], self.DEFAULT_AUTH_ENV)
        self.assertEqual(config.auth_submit_field, 'field')

    def test_can_set_auth_submit_field_using_environment(self):
        config = self.parser.parse([], merge({'DAST_SUBMIT_FIELD': 'field'}, self.DEFAULT_AUTH_ENV))
        self.assertEqual(config.auth_submit_field, 'field')

    def test_can_set_auth_submit_field_using_legacy_environment(self):
        config = self.parser.parse([], merge({'AUTH_SUBMIT_FIELD': 'field'}, self.DEFAULT_AUTH_ENV))
        self.assertEqual(config.auth_submit_field, 'field')

    def test_can_set_auth_first_submit_field_using_option(self):
        config = self.parser.parse(['--auth-first-submit-field', 'field'], self.DEFAULT_AUTH_ENV)
        self.assertEqual(config.auth_first_submit_field, 'field')

    def test_can_set_auth_first_submit_field_using_environment(self):
        config = self.parser.parse([], merge({'DAST_FIRST_SUBMIT_FIELD': 'field'}, self.DEFAULT_AUTH_ENV))
        self.assertEqual(config.auth_first_submit_field, 'field')

    def test_can_set_auth_first_submit_field_using_legacy_environment(self):
        config = self.parser.parse([], merge({'AUTH_FIRST_SUBMIT_FIELD': 'field'}, self.DEFAULT_AUTH_ENV))
        self.assertEqual(config.auth_first_submit_field, 'field')

    def test_can_set_auth_display_using_option(self):
        config = self.parser.parse(['--auth-display', 'True'], self.DEFAULT_ENV)
        self.assertEqual(config.auth_display, True)

    def test_can_set_auth_display_using_environment(self):
        config = self.parser.parse([], merge({'DAST_AUTH_DISPLAY': 'True'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_display, True)

    def test_can_set_auth_display_using_legacy_environment(self):
        config = self.parser.parse([], merge({'AUTH_DISPLAY': 'True'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_display, True)

    def test_can_set_auth_auto_using_option(self):
        config = self.parser.parse(['--auth-auto', 'true'], self.DEFAULT_ENV)
        self.assertEqual(config.auth_auto, True)

    def test_can_set_auth_auto_using_environment(self):
        config = self.parser.parse([], merge({'DAST_AUTH_AUTO': 'true'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_auto, True)

    def test_can_set_auth_auto_using_legacy_environment(self):
        config = self.parser.parse([], merge({'AUTH_AUTO': 'true'}, self.DEFAULT_ENV))
        self.assertEqual(config.auth_auto, True)

    def test_exclude_urls_is_empty_by_default(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.exclude_urls, [])

    def test_can_not_set_exclude_urls_to_have_any_invalid_url(self):
        with self.assertRaises(SystemExit) as system_exit, utilities.capture_output() as output:
            self.parser.parse(['--auth-exclude-urls', 'http://website1 , http://website2   , invalidurl'],
                              self.DEFAULT_ENV)

        self.assertEqual(system_exit.exception.code, 2)
        self.assertIn('argument --auth-exclude-urls: invalidurl is not a valid URL', output.stderr.getvalue())

    def test_can_set_exclude_urls_using_option(self):
        config = self.parser.parse(['--auth-exclude-urls', 'http://website1 , http://website2  '], self.DEFAULT_ENV)
        self.assertEqual(config.exclude_urls, ['http://website1', 'http://website2'])

    def test_can_set_exclude_urls_using_legacy_environment(self):
        environment = merge({'DAST_AUTH_EXCLUDE_URLS': 'http://website1,http://website2'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.exclude_urls, ['http://website1', 'http://website2'])

    def test_can_set_exclude_urls_using_second_legacy_environment(self):
        environment = merge({'AUTH_EXCLUDE_URLS': 'http://website1,http://website2'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.exclude_urls, ['http://website1', 'http://website2'])

    def test_can_not_set_vars_using_legacy_environment_after_version_2(self):
        self.mock_system.return_value.dast_version.return_value = '2.6.1'

        environment = merge({
            'DAST_AUTH_EXCLUDE_URLS': 'http://website1,http://website2',
            'AUTH_URL': 'http://website/login',
            'AUTH_USERNAME': 'example@test.com',
            'AUTH_PASSWORD': 'abc123',
            'AUTH_USERNAME_FIELD': 'username',
            'AUTH_PASSWORD_FIELD': 'password',
            'AUTH_SUBMIT_FIELD': 'submit',
            'AUTH_FIRST_SUBMIT_FIELD': 'submit1',
            'AUTH_AUTO': 'True',
            'DAST_REQUEST_HEADER': 'Cache-control: no-cache',
        }, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.exclude_urls, [])
        self.assertIsNone(config.auth_url)
        self.assertIsNone(config.auth_username)
        self.assertIsNone(config.auth_password)
        self.assertIsNone(config.auth_username_field)
        self.assertIsNone(config.auth_password_field)
        self.assertIsNone(config.auth_submit_field)
        self.assertIsNone(config.auth_auto)
        self.assertEqual(config.request_headers, {})

    def test_can_set_exclude_urls_using_environment(self):
        environment = merge({'DAST_EXCLUDE_URLS': 'http://website1,http://website2'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.exclude_urls, ['http://website1', 'http://website2'])

    def test_enable_silent_when_auto_update_addons_is_false(self):
        config = self.parser.parse(['--auto-update-addons', 'False'], self.DEFAULT_ENV)
        self.assertEqual(config.silent, True)

    def test_enable_silent_when_auto_update_addons_is_true_and_silent_is_true(self):
        config = self.parser.parse(['--auto-update-addons', 'True', '-z-silent'], self.DEFAULT_ENV)
        self.assertEqual(config.silent, True)

    def test_silent_disabled_when_auto_update_addons_is_true_and_silent_is_not_set(self):
        config = self.parser.parse(['--auto-update-addons', 'True'], self.DEFAULT_ENV)
        self.assertEqual(config.silent, False)

    def test_enable_auto_update_addons_using_option(self):
        config = self.parser.parse(['--auto-update-addons', 'True'], self.DEFAULT_ENV)
        self.assertEqual(config.auto_update_addons, True)

    def test_can_set_exclude_rules_using_option(self):
        config = self.parser.parse(['--exclude-rules', ' 10001 , 20002  '], self.DEFAULT_ENV)
        self.assertEqual(config.exclude_rules, ['10001', '20002'] + self.exclude_rules)

    def test_can_set_exclude_rules_using_environment(self):
        environment = merge({'DAST_EXCLUDE_RULES': '56,57'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.exclude_rules, ['56', '57'] + self.exclude_rules)

    def test_only_gitlab_exclude_rules_are_excluded_by_default(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.exclude_rules, self.exclude_rules)

    def test_includes_api_scan_exclude_rules_if_api_full_scan(self):
        config = self.parser.parse([], {'DAST_API_SPECIFICATION': 'api-spec.json', 'DAST_FULL_SCAN_ENABLED': 'true'})

        self.assertEqual(config.exclude_rules, self.exclude_rules + APIScanPolicy.exclude_rules())

    def test_can_set_rules_with_DAST_ONLY_INCLUDE_RULES(self):
        env = merge({'DAST_ONLY_INCLUDE_RULES': '10000,10001'}, self.DEFAULT_ENV)
        config = self.parser.parse([], env)

        self.assertEqual(config.only_include_rules, ['10000', '10001'])

    def test_can_set_rules_with_only_include_rules_option(self):
        config = self.parser.parse(['--only-include-rules', '10000,10001'], self.DEFAULT_ENV)

        self.assertEqual(config.only_include_rules, ['10000', '10001'])

    def test_errors_if_exclude_rules_and_only_include_rules_both_set(self):
        env = merge({'DAST_ONLY_INCLUDE_RULES': '10000', 'DAST_EXCLUDE_RULES': '10001'}, self.DEFAULT_ENV)

        with self.assertRaises(InvalidConfigurationError) as error:
            self.parser.parse([], env)

        self.assertIn('DAST_EXCLUDE_RULES cannot be used when DAST_ONLY_INCLUDE_RULES is set.', str(error.exception))

    def test_can_set_request_headers_using_option(self):
        config = self.parser.parse(['--request-headers', ' Authorization: Bearer 123456789 , Accept: */*  '],
                                   self.DEFAULT_ENV)
        self.assertDictEqual(config.request_headers, {'Authorization': 'Bearer 123456789', 'Accept': '*/*'})

    def test_can_set_request_headers_using_environment(self):
        environment = merge({'DAST_REQUEST_HEADERS': 'Cache-control: no-cache'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertDictEqual(config.request_headers, {'Cache-control': 'no-cache'})

    def test_no_request_headers_are_set_by_default(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertDictEqual(config.request_headers, {})

    def test_disable_auto_update_addons_using_option(self):
        config = self.parser.parse(['--auto-update-addons', 'False'], self.DEFAULT_ENV)
        self.assertEqual(config.auto_update_addons, False)

    def test_auto_update_addons_is_false_by_default(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.auto_update_addons, False)

    def test_disable_auto_update_addons_using_environment(self):
        config = self.parser.parse([], merge({'DAST_AUTO_UPDATE_ADDONS': '0'}, self.DEFAULT_ENV))
        self.assertEqual(config.auto_update_addons, False)

    def test_can_set_full_scan_using_option_passing_true(self):
        config = self.parser.parse(['--full-scan', 'True'], self.DEFAULT_ENV)
        self.assertEqual(config.full_scan, True)

    def test_can_set_full_scan_using_option_passing_1(self):
        config = self.parser.parse(['--full-scan', '1'], self.DEFAULT_ENV)
        self.assertEqual(config.full_scan, True)

    def test_can_set_full_scan_using_option_passing_false(self):
        config = self.parser.parse(['--full-scan', 'false'], self.DEFAULT_ENV)
        self.assertEqual(config.full_scan, False)

    def test_can_set_full_scan_using_option_passing_0(self):
        config = self.parser.parse(['--full-scan', '0'], self.DEFAULT_ENV)
        self.assertEqual(config.full_scan, False)

    def test_can_set_full_scan_using_option(self):
        config = self.parser.parse(['--full-scan', 'true'], self.DEFAULT_ENV)
        self.assertEqual(config.full_scan, True)

    def test_can_set_full_scan_should_by_falsy_when_junk_is_passed_in(self):
        config = self.parser.parse(['--full-scan', 'not-a-boolean-value'], self.DEFAULT_ENV)
        self.assertEqual(config.full_scan, False)

    def test_can_set_full_scan_using_environment(self):
        environment = merge({'DAST_FULL_SCAN_ENABLED': 'true'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.full_scan, True)

    def test_can_set_write_addons_to_update_file_using_option_passing_true(self):
        config = self.parser.parse(['--write-addons-to-update-file', 'True'], self.DEFAULT_ENV)
        self.assertEqual(config.write_addons_to_update_file, True)

    def test_can_set_write_addons_to_update_file_using_option_passing_false(self):
        config = self.parser.parse(['--write-addons-to-update-file', 'false'], self.DEFAULT_ENV)
        self.assertEqual(config.write_addons_to_update_file, False)

    def test_can_set_write_addons_to_update_file_using_option(self):
        config = self.parser.parse(['--write-addons-to-update-file', 'true'], self.DEFAULT_ENV)
        self.assertEqual(config.write_addons_to_update_file, True)

    def test_can_set_validate_domain_using_option_passing_true(self):
        config = self.parser.parse(['--validate-domain', 'True'], self.DEFAULT_ENV)
        self.assertEqual(config.full_scan_domain_validation_required, True)

    def test_can_set_validate_domain_using_option_passing_1(self):
        config = self.parser.parse(['--validate-domain', '1'], self.DEFAULT_ENV)
        self.assertEqual(config.full_scan_domain_validation_required, True)

    def test_can_set_validate_domain_using_option_passing_false(self):
        config = self.parser.parse(['--validate-domain', 'false'], self.DEFAULT_ENV)
        self.assertEqual(config.full_scan_domain_validation_required, False)

    def test_can_set_validate_domain_using_option_passing_0(self):
        config = self.parser.parse(['--validate-domain', '0'], self.DEFAULT_ENV)
        self.assertEqual(config.full_scan_domain_validation_required, False)

    def test_can_set_domain_validation_using_option(self):
        config = self.parser.parse(['--validate-domain', 'true'], self.DEFAULT_ENV)
        self.assertEqual(config.full_scan_domain_validation_required, True)

    def test_can_set_full_scan_domain_validation_required_using_environment(self):
        environment = merge({'DAST_FULL_SCAN_DOMAIN_VALIDATION_REQUIRED': 'true'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.full_scan_domain_validation_required, True)

    def test_can_set_availability_timeout_using_option(self):
        config = self.parser.parse(['--availability-timeout', '10'], self.DEFAULT_ENV)
        self.assertEqual(config.availability_timeout, 10)

    def test_can_set_availability_timeout_using_environment(self):
        environment = merge({'DAST_TARGET_AVAILABILITY_TIMEOUT': '20'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.availability_timeout, 20)

    def test_default_availability_timeout(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.availability_timeout, 60)

    def test_can_set_skip_target_check_using_option(self):
        config = self.parser.parse(['--skip-target-check', 'true'], self.DEFAULT_ENV)
        self.assertTrue(config.skip_target_check)

    def test_can_set_skip_target_check_using_environment(self):
        environment = merge({'DAST_SKIP_TARGET_CHECK': 'true'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertTrue(config.skip_target_check)

    def test_default_skip_target_check_is_false(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertFalse(config.skip_target_check)

    def test_can_set_zap_option_mins(self):
        config = self.parser.parse(['-m', '5'], self.DEFAULT_ENV)
        self.assertEqual(config.spider_mins, 5)

    def test_can_set_zap_option_mins_using_environment_variable(self):
        environment = merge({'DAST_SPIDER_MINS': '0'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.spider_mins, 0)

    def test_default_spider_mins_when_passive_scan_and_not_set(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.spider_mins, 1)

    def test_unlimited_default_spider_mins_when_full_scan_and_not_set(self):
        config = self.parser.parse(['--full-scan', 'true'], self.DEFAULT_ENV)
        self.assertEqual(config.spider_mins, 0)

    def test_can_set_zap_option_report_html(self):
        config = self.parser.parse(['-r', 'report_html'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_report_html, 'report_html')

    def test_can_set_zap_long_option_report_html(self):
        config = self.parser.parse(['--html-report', 'report_html'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_report_html, 'report_html')

    def test_can_set_zap_option_report_html_using_environment(self):
        environment = merge({'DAST_HTML_REPORT': 'report.html'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.zap_report_html, 'report.html')

    def test_can_set_zap_option_report_md(self):
        config = self.parser.parse(['-w', 'report_md'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_report_md, 'report_md')

    def test_can_set_zap_long_option_report_md(self):
        config = self.parser.parse(['--markdown-report', 'report_md'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_report_md, 'report_md')

    def test_can_set_zap_option_report_md_using_environment(self):
        environment = merge({'DAST_MARKDOWN_REPORT': 'report.md'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.zap_report_md, 'report.md')

    def test_can_set_zap_option_report_xml(self):
        config = self.parser.parse(['-x', 'report_xml'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_report_xml, 'report_xml')

    def test_can_set_zap_long_option_report_xml(self):
        config = self.parser.parse(['--xml-report', 'report_xml'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_report_xml, 'report_xml')

    def test_can_set_zap_option_report_xml_using_environment(self):
        environment = merge({'DAST_XML_REPORT': 'report.xml'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.zap_report_xml, 'report.xml')

    def test_defaults_to_not_a_browserker_scan(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_scan, False)

    def test_can_trigger_browserker_scan(self):
        config = self.parser.parse(['--browser-scan'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_scan, True)

    def test_can_trigger_browserker_scan_using_environment_variables(self):
        environment = merge({'DAST_BROWSER_SCAN': 'true'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_scan, True)

    def test_defaults_to_no_custom_hash_attributes(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_hash_attributes, [])

    def test_can_set_custom_hash_attributes(self):
        config = self.parser.parse(['--browser-hash-attributes', 'data-id,  data-name'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_hash_attributes, ['data-id', 'data-name'])

    def test_can_set_custom_hash_attributes_using_environment_variables(self):
        environment = merge({'DAST_BROWSER_HASH_ATTRIBUTES': 'data-id'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_hash_attributes, ['data-id'])

    def test_defaults_to_no_browser_include_only_rules(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_include_only_rules, [])

    def test_can_set_browser_include_only_rules(self):
        config = self.parser.parse(['--browser-include-only-rules', '1, 2,   3'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_include_only_rules, ['1', '2', '3'])

    def test_can_set_browser_include_only_rules_using_environment_variables(self):
        environment = merge({'DAST_BROWSER_INCLUDE_ONLY_RULES': '100.1'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_include_only_rules, ['100.1'])

    def test_defaults_to_standard_browser_file_log(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_file_log_path, '/output/browserker-debug.log')

    def test_can_set_browser_file_log(self):
        config = self.parser.parse(['--browser-file-log-path', '/zap/wrk/file.log'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_file_log_path, '/zap/wrk/file.log')

    def test_can_set_browser_file_log_using_environment_variables(self):
        environment = merge({'DAST_BROWSER_FILE_LOG_PATH': '/zap/wrk/file.log'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_file_log_path, '/zap/wrk/file.log')

    def test_defaults_to_not_log_browser_error_report(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_log_request_error_report, False)

    def test_can_set_to_log_browser_error_report_using_cli(self):
        config = self.parser.parse(['--browser-log-request-error-report'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_log_request_error_report, True)

    def test_can_set_to_log_browser_error_report_using_environment_variable(self):
        environment = merge({'DAST_BROWSER_LOG_REQUEST_ERROR_REPORT': 'true'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_log_request_error_report, True)

    def test_defaults_to_no_browser_secure_report_extra_info(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_secure_report_extra_info, False)

    def test_can_set_browser_secure_report_extra_info(self):
        config = self.parser.parse(['--browser-secure-report-extra-info'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_secure_report_extra_info, True)

    def test_can_set_browser_secure_report_extra_info_using_environment_variables(self):
        environment = merge({'DAST_BROWSER_SECURE_REPORT_EXTRA_INFO': 'true'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_secure_report_extra_info, True)

    def test_defaults_to_not_logging_chromium_output(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_log_chromium_output, False)

    def test_can_enable_logging_chromium_output(self):
        config = self.parser.parse(['--browser-log-chromium-output'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_log_chromium_output, True)

    def test_can_enable_logging_chromium_output_using_environment_variables(self):
        environment = merge({'DAST_BROWSER_LOG_CHROMIUM_OUTPUT': 'true'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_log_chromium_output, True)

    def test_defaults_to_not_using_browserker_cache(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_cache, False)

    def test_can_enable_browserker_cache(self):
        config = self.parser.parse(['--browser-cache'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_cache, True)

    def test_can_enable_browserker_cache_using_environment_variables(self):
        environment = merge({'DAST_BROWSER_CACHE': 'true'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_cache, True)

    def test_can_trigger_browserker_scan_using_legacy_cli(self):
        with patch('src.config.configuration_parser.path') as path:
            path.isfile.return_value = False
            self.mock_system.return_value.dast_version.return_value = '2.2.1'

            config = self.parser.parse(['--browserker-scan'], self.DEFAULT_ENV)
            self.assertEqual(config.browserker_scan, True)

    def test_can_trigger_browserker_scan_using_legacy_environment_variables(self):
        with patch('src.config.configuration_parser.path') as path:
            path.isfile.return_value = False
            self.mock_system.return_value.dast_version.return_value = '2.2.1'

            environment = merge({'DAST_BROWSER_SCAN': 'true'}, self.DEFAULT_ENV)
            config = self.parser.parse([], environment)
            self.assertEqual(config.browserker_scan, True)

    def test_raises_error_if_ajax_spider_scan_and_browserker_scan_configured(self):
        with self.assertRaises(InvalidConfigurationError) as error:
            options = {'DAST_BROWSER_SCAN': 'true', 'DAST_USE_AJAX_SPIDER': 'true'}
            self.parser.parse([], merge(options, self.DEFAULT_ENV))

        self.assertIn('Browserker cannot be used with an AJAX spider scan', str(error.exception))

    def test_raises_error_if_api_scan_and_browserker_scan_configured(self):
        with self.assertRaises(InvalidConfigurationError) as error:
            options = {'DAST_BROWSER_SCAN': 'true', 'DAST_API_SPECIFICATION': 'api_spec.yml'}
            self.parser.parse([], options)

        self.assertIn('Browserker cannot be used with an API scan', str(error.exception))

    def test_sets_default_max_actions(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_max_actions, 10000)

    def test_can_set_browserker_max_actions_using_cli(self):
        config = self.parser.parse(['--browser-max-actions', '456'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_max_actions, 456)

    def test_can_set_browserker_max_actions_using_environment_variables(self):
        environment = merge({'DAST_BROWSER_MAX_ACTIONS': '224'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_max_actions, 224)

    def test_sets_default_max_depth(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_max_depth, 10)

    def test_can_set_browserker_max_depth_using_cli(self):
        config = self.parser.parse(['--browser-max-depth', '7'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_max_depth, 7)

    def test_can_set_browserker_max_depth_using_environment_variables(self):
        environment = merge({'DAST_BROWSER_MAX_DEPTH': '15'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_max_depth, 15)

    def test_sets_default_number_of_browsers(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_number_of_browsers, 3)

    def test_can_set_browserker_number_of_browsers_using_cli(self):
        config = self.parser.parse(['--browser-number-of-browsers', '2'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_number_of_browsers, 2)

    def test_can_set_browserker_number_of_browsers_using_environment_variables(self):
        environment = merge({'DAST_BROWSER_NUMBER_OF_BROWSERS': '1'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_number_of_browsers, 1)

    def test_sets_default_always_relogin(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_always_relogin, False)

    def test_can_set_browserker_always_relogin_using_cli(self):
        config = self.parser.parse(['--browser-always-relogin'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_always_relogin, True)

    def test_can_set_browserker_always_relogin_using_environment_variables(self):
        environment = merge({'DAST_BROWSER_ALWAYS_RELOGIN': 'true'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_always_relogin, True)

    def test_can_set_browserker_auth_verification_selector_using_cli(self):
        config = self.parser.parse(['--browser-auth-verification-selector', 'css:.home'], self.DEFAULT_ENV)
        self.assertEqual(config.auth_verification_selector, 'css:.home')

    def test_can_set_browserker_auth_verification_selector_using_environment_variables(self):
        environment = merge({'DAST_AUTH_VERIFICATION_SELECTOR': 'id:username'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.auth_verification_selector, 'id:username')

    def test_can_set_browserker_auth_verification_selector_using_legacy_environment_variable(self):
        environment = merge({'DAST_BROWSER_AUTH_VERIFICATION_SELECTOR': 'id:username'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.auth_verification_selector, 'id:username')

    def test_can_set_browserker_auth_cookies_using_cli(self):
        config = self.parser.parse(['--browser-auth-cookies', 'jwt, refresh'], self.DEFAULT_ENV)
        self.assertEqual(config.auth_cookies, ['jwt', 'refresh'])

    def test_can_set_browserker_auth_cookies_using_environment_variables(self):
        environment = merge({'DAST_AUTH_COOKIES': 'sessionID'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.auth_cookies, ['sessionID'])

    def test_can_set_default_browserker_auth_cookies(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.auth_cookies, [])

    def test_sets_default_login_form_verification(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.auth_verification_login_form, False)

    def test_can_set_default_login_form_verification_using_cli(self):
        config = self.parser.parse(['--browser-auth-verification-login-form'], self.DEFAULT_ENV)
        self.assertEqual(config.auth_verification_login_form, True)

    def test_can_set_default_login_form_verification_using_environment_variables(self):
        environment = merge({'DAST_AUTH_VERIFICATION_LOGIN_FORM': 'true'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.auth_verification_login_form, True)

    def test_can_set_default_login_form_verification_using_legacy_environment_variables(self):
        environment = merge({'DAST_BROWSER_AUTH_VERIFICATION_LOGIN_FORM': 'true'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.auth_verification_login_form, True)

    def test_sets_default_browserk_crawl_report(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_crawl_report, False)

    def test_can_set_default_browserk_crawl_report_using_cli(self):
        config = self.parser.parse(['--browser-crawl-report'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_crawl_report, True)

    def test_can_set_default_browserk_crawl_report_using_environment_variables(self):
        environment = merge({'DAST_BROWSER_CRAWL_REPORT': 'true'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_crawl_report, True)

    def test_sets_default_auth_report(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.auth_report, False)

    def test_can_set_default_auth_report_using_cli(self):
        config = self.parser.parse(['--browser-auth-report'], self.DEFAULT_ENV)
        self.assertEqual(config.auth_report, True)

    def test_can_set_default_auth_report_using_environment_variables(self):
        environment = merge({'DAST_AUTH_REPORT': 'true'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.auth_report, True)

    def test_can_set_default_auth_report_using_legacy_environment_variables(self):
        environment = merge({'DAST_BROWSER_AUTH_REPORT': 'true'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.auth_report, True)

    def test_sets_default_path_to_login_form(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.auth_path_to_login_form, [])

    def test_can_set_browserker_path_to_login_form_using_cli(self):
        config = self.parser.parse(['--browser-path-to-login-form', 'css:.menu, css:.show-login-modal   '],
                                   self.DEFAULT_ENV)
        self.assertEqual(config.auth_path_to_login_form, ['css:.menu', 'css:.show-login-modal'])

    def test_can_set_browserker_path_to_login_form_using_environment_variables(self):
        environment = merge({'DAST_BROWSER_PATH_TO_LOGIN_FORM': '.show-login-modal'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.auth_path_to_login_form, ['.show-login-modal'])

    def test_sets_default_excluded_hosts(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_excluded_hosts, [])

    def test_can_set_browserker_excluded_hosts_using_cli(self):
        config = self.parser.parse(['--browser-excluded-hosts', '  domain-a.com , domain-b.com'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_excluded_hosts, ['domain-a.com', 'domain-b.com'])

    def test_can_set_browserker_excluded_hosts_using_environment_variables(self):
        environment = merge({'DAST_BROWSER_EXCLUDED_HOSTS': 'site.com'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_excluded_hosts, ['site.com'])

    def test_sets_default_ignored_hosts(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_ignored_hosts, [])

    def test_can_set_browserker_ignored_hosts_using_cli(self):
        config = self.parser.parse(['--browser-ignored-hosts', '  domain-a.com , domain-b.com'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_ignored_hosts, ['domain-a.com', 'domain-b.com'])

    def test_can_set_browserker_ignored_hosts_using_environment_variables(self):
        environment = merge({'DAST_BROWSER_IGNORED_HOSTS': 'site.com'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_ignored_hosts, ['site.com'])

    def test_sets_default_allowed_hosts_to_target(self):
        environment = merge({'DAST_WEBSITE': 'http://site.com', 'DAST_BROWSER_SCAN': 'true'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_allowed_hosts, ['site.com'])

    def test_sets_default_allowed_hosts_to_target_when_is_authenticated_scan(self):
        environment = merge({'DAST_WEBSITE': 'http://site.com'}, self.DEFAULT_AUTH_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_allowed_hosts, ['site.com'])

    def test_can_set_browserker_allowed_hosts_using_cli(self):
        config = self.parser.parse([
            '-t', 'http://site.com',
            '--browser-allowed-hosts', '  domain-a.com , domain-b.com',
            '--browser-scan',
        ], self.DEFAULT_ENV)

        self.assertEqual(config.browserker_allowed_hosts, ['domain-a.com', 'domain-b.com', 'site.com'])

    def test_can_set_browserker_allowed_hosts_using_environment_variables(self):
        environment = merge({'DAST_BROWSER_ALLOWED_HOSTS': 'site.com', 'DAST_BROWSER_SCAN': 'true'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_allowed_hosts, ['site.com', 'website'])

    def test_can_set_browserker_excluded_elements_using_cli(self):
        config = self.parser.parse(['--browser-excluded-elements', '  xpath://body/a, css:.forms'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_excluded_elements, ['xpath://body/a', 'css:.forms'])

    def test_can_set_browserker_excluded_elements_using_environment_variables(self):
        environment = merge({'DAST_BROWSER_EXCLUDED_ELEMENTS': 'css:.navigation>*'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_excluded_elements, ['css:.navigation>*'])

    def test_sets_default_max_attack_failures(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_max_attack_failures, 5)

    def test_can_set_browserker_max_attack_failures_using_cli(self):
        config = self.parser.parse(['--browser-max-attack-failures', '2'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_max_attack_failures, 2)

    def test_can_set_browserker_max_attack_failures_using_environment_variables(self):
        environment = merge({'DAST_BROWSER_MAX_ATTACK_FAILURES': '1'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_max_attack_failures, 1)

    def test_can_set_browserker_cookies_using_option(self):
        config = self.parser.parse(['--browser-cookies', 'abtesting_group:3,region:locked'],
                                   self.DEFAULT_ENV)
        self.assertDictEqual(config.browserker_cookies, {'abtesting_group': '3', 'region': 'locked'})

    def test_can_set_browserker_cookies_using_environment(self):
        environment = merge({'DAST_BROWSER_COOKIES': 'name:Archer'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertDictEqual(config.browserker_cookies, {'name': 'Archer'})

    def test_no_browserker_cookies_are_set_by_default(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertDictEqual(config.browserker_cookies, {})

    def test_can_set_navigation_timeout_using_option(self):
        config = self.parser.parse(['--browser-navigation-timeout', '5s'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_navigation_timeout, '5s')

    def test_can_set_navigation_timeout_using_environment(self):
        environment = merge({'DAST_BROWSER_NAVIGATION_TIMEOUT': '5s'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_navigation_timeout, '5s')

    def test_can_set_action_timeout_using_option(self):
        config = self.parser.parse(['--browser-action-timeout', '5s'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_action_timeout, '5s')

    def test_can_set_action_timeout_using_environment(self):
        environment = merge({'DAST_BROWSER_ACTION_TIMEOUT': '5s'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_action_timeout, '5s')

    def test_can_set_stability_timeout_using_option(self):
        config = self.parser.parse(['--browser-stability-timeout', '5s'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_stability_timeout, '5s')

    def test_can_set_stability_timeout_using_environment(self):
        environment = merge({'DAST_BROWSER_STABILITY_TIMEOUT': '5s'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_stability_timeout, '5s')

    def test_can_set_navigation_stability_timeout_using_option(self):
        config = self.parser.parse(['--browser-navigation-stability-timeout', '5s'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_navigation_stability_timeout, '5s')

    def test_can_set_navigation_stability_timeout_using_environment(self):
        environment = merge({'DAST_BROWSER_NAVIGATION_STABILITY_TIMEOUT': '5s'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_navigation_stability_timeout, '5s')

    def test_can_set_action_stability_timeout_using_option(self):
        config = self.parser.parse(['--browser-action-stability-timeout', '5s'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_action_stability_timeout, '5s')

    def test_can_set_action_stability_timeout_using_environment(self):
        environment = merge({'DAST_BROWSER_ACTION_STABILITY_TIMEOUT': '5s'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_action_stability_timeout, '5s')

    def test_can_set_search_element_timeout_using_option(self):
        config = self.parser.parse(['--browser-search-element-timeout', '5s'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_search_element_timeout, '5s')

    def test_can_set_search_element_using_environment(self):
        environment = merge({'DAST_BROWSER_SEARCH_ELEMENT_TIMEOUT': '5s'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_search_element_timeout, '5s')

    def test_can_set_extract_element_timeout_using_option(self):
        config = self.parser.parse(['--browser-extract-element-timeout', '5s'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_extract_element_timeout, '5s')

    def test_can_set_extract_element_timeout_using_environment(self):
        environment = merge({'DAST_BROWSER_EXTRACT_ELEMENT_TIMEOUT': '5s'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_extract_element_timeout, '5s')

    def test_can_set_element_timeout_using_option(self):
        config = self.parser.parse(['--browser-element-timeout', '5s'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_element_timeout, '5s')

    def test_can_set_element_timeout_using_environment(self):
        environment = merge({'DAST_BROWSER_ELEMENT_TIMEOUT': '5s'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_element_timeout, '5s')

    def test_can_set_zap_option_report_include_alpha_rules(self):
        config = self.parser.parse(['-a'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_include_alpha, True)

    def test_can_set_zap_option_report_include_alpha_vulns_using_environment(self):
        environment = merge({'DAST_INCLUDE_ALPHA_VULNERABILITIES': 'false'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.zap_include_alpha, False)

    def test_can_set_zap_option_report_show_debug_msgs(self):
        config = self.parser.parse(['-d'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_debug, True)

    def test_can_set_zap_option_report_show_debug_msgs_using_environment(self):
        environment = merge({'DAST_DEBUG': 'true'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.zap_debug, True)

    def test_can_set_zap_option_port(self):
        config = self.parser.parse(['-P', '2001'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_port, 2001)

    def test_zap_port_defaults_to_a_free_port_if_not_set(self):
        self.mock_system.return_value.get_free_port.return_value = 43001
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.zap_port, 43001)

    def test_can_set_zap_option_delay_in_seconds(self):
        config = self.parser.parse(['-D', '12'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_delay_in_seconds, '12')

    def test_can_set_zap_option_default_info(self):
        config = self.parser.parse(['-i'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_default_info, True)

    def test_can_set_zap_option_no_fail_on_warn(self):
        config = self.parser.parse(['-I'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_no_fail_on_warn, True)

    def test_can_set_zap_ajax_spider_using_zap_env_variable(self):
        environment = merge({'DAST_ZAP_USE_AJAX_SPIDER': True}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.zap_use_ajax_spider, True)

    def test_can_set_zap_ajax_spider_using_env_variable(self):
        environment = merge({'DAST_USE_AJAX_SPIDER': True}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.zap_use_ajax_spider, True)

    def test_can_set_zap_option_use_ajax_spider(self):
        config = self.parser.parse(['-j'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_use_ajax_spider, True)

    def test_can_set_zap_option_min_level(self):
        config = self.parser.parse(['-l', 'INFO'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_min_level, 'INFO')

    def test_can_set_zap_option_context(self):
        config = self.parser.parse(['-n', 'context.file'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_context_file, 'context.file')

    def test_can_set_zap_option_progress(self):
        config = self.parser.parse(['-p', 'progress.file'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_progress_file, 'progress.file')

    def test_can_set_zap_option_short_format(self):
        config = self.parser.parse(['-s'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_short_format, True)

    def test_can_set_zap_connection_attempts_using_option(self):
        config = self.parser.parse(['--zap-max-connection-attempts', '1'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_max_connection_attempts, 1)

    def test_can_set_zap_connection_attempts_using_environment(self):
        environment = merge({'DAST_ZAP_MAX_CONNECTION_ATTEMPTS': '34'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.zap_max_connection_attempts, 34)

    def test_connection_attempts_has_default(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.zap_max_connection_attempts, 600)

    def test_zap_connection_attempts_does_not_allow_negative_integers(self):
        with self.assertRaises(SystemExit) as system_exit, utilities.capture_output() as output:
            self.parser.parse(['--zap-max-connection-attempts', '-1'], {})

        self.assertEqual(system_exit.exception.code, 2)
        self.assertIn('cannot be negative', output.stderr.getvalue())

    def test_ignores_deprecated_option_if_other_timeouts_have_been_set(self):
        config = self.parser.parse(['-T', '1', '--zap-max-connection-attempts', '2'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_max_connection_attempts, 2)
        self.assertEqual(config.passive_scan_max_wait_time, 600)

    def test_can_set_connection_attempts_and_passive_scan_wait_time_using_option(self):
        config = self.parser.parse(['-T', '5', '--zap-connect-sleep-seconds', '5'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_connect_sleep_seconds, 5)
        self.assertEqual(config.zap_max_connection_attempts, 60)
        self.assertEqual(config.passive_scan_max_wait_time, 5)

    def test_can_set_connection_attempts_and_passive_scan_wait_time_when_no_sleep_seconds(self):
        config = self.parser.parse(['-T', '5', '--zap-connect-sleep-seconds', '0'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_max_connection_attempts, 300)
        self.assertEqual(config.passive_scan_max_wait_time, 5)

    def test_can_set_zap_sleep_seconds_using_option(self):
        config = self.parser.parse(['--zap-connect-sleep-seconds', '1'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_connect_sleep_seconds, 1)

    def test_can_set_zap_sleep_seconds_using_environment(self):
        environment = merge({'DAST_ZAP_CONNECT_SLEEP_SECONDS': '60'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.zap_connect_sleep_seconds, 60)

    def test_sleep_seconds_has_default(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.zap_connect_sleep_seconds, 1)

    def test_can_set_passive_scan_wait_time_using_option(self):
        config = self.parser.parse(['--passive-scan-max-wait-time', '1'], self.DEFAULT_ENV)
        self.assertEqual(config.passive_scan_max_wait_time, 1)

    def test_can_set_passive_scan_wait_time_using_environment(self):
        environment = merge({'DAST_PASSIVE_SCAN_MAX_WAIT_TIME': '34'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.passive_scan_max_wait_time, 34)

    def test_passive_scan_wait_time_has_default(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.passive_scan_max_wait_time, 600)

    def test_can_set_zap_option_other_options(self):
        config = self.parser.parse(['-z', 'funtimes=3'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_other_options, 'funtimes=3')

    def test_can_set_zap_option_other_options_using_environment(self):
        environment = merge({'DAST_ZAP_CLI_OPTIONS': 'badtimes=0'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.zap_other_options, 'badtimes=0')

    def test_can_set_zap_option_log_configuration(self):
        config = self.parser.parse(['--zap-log-configuration', 'opts'], self.DEFAULT_ENV)
        self.assertEqual(config.zap_log_configuration, 'opts')

    def test_can_set_zap_option_log_configuration_using_environment(self):
        environment = merge({'DAST_ZAP_LOG_CONFIGURATION': 'opts'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.zap_log_configuration, 'opts')

    def test_can_set_http_headers_to_mask_using_option(self):
        config = self.parser.parse(['--mask-http-headers', ' Authorization   , X-Vault-Token'], self.DEFAULT_ENV)
        self.assertEqual(config.http_headers_to_mask, ['Authorization', 'X-Vault-Token'])

    def test_can_set_http_headers_to_mask_using_environment(self):
        environment = merge({'DAST_MASK_HTTP_HEADERS': 'Authorization'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.http_headers_to_mask, ['Authorization'])

    def test_can_set_no_http_headers_to_mask_when_environment_variable_is_empty(self):
        environment = merge({'DAST_MASK_HTTP_HEADERS': ''}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.http_headers_to_mask, [])

    def test_uses_default_values_for_http_headers_to_mask_when_not_set(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertIn('Authorization', config.http_headers_to_mask)

    def test_can_set_script_directories_using_option(self):
        config = self.parser.parse(['--script-dirs', '/home/zap/dast-scripts , /build/my-scripts  '], self.DEFAULT_ENV)
        self.assertIn('/home/zap/dast-scripts', config.script_dirs)
        self.assertIn('/build/my-scripts', config.script_dirs)

    def test_can_set_script_directories_using_environment_variable(self):
        environment = merge({'DAST_SCRIPT_DIRS': '/home/zap/scripts,/home/zap/more-scripts'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertIn('/home/zap/scripts', config.script_dirs)
        self.assertIn('/home/zap/more-scripts', config.script_dirs)

    def test_script_dirs_contains_dast_api_scripts(self):
        environment = {'DAST_SCRIPT_DIRS': '/home/zap/scripts', 'DAST_API_SPECIFICATION': 'http://website/api.spec'}
        config = self.parser.parse([], environment)
        self.assertIn(ConfigurationParser.DAST_API_SCAN_SCRIPTS_DIR, config.script_dirs)

    def test_script_dirs_contains_dast_normal_scan_scripts(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertIn(ConfigurationParser.DAST_NORMAL_SCAN_SCRIPTS_DIR, config.script_dirs)

    def test_raises_error_when_DAST_PATHS_is_specificed_but_DAST_WEBSITE_is_not(self):
        with self.assertRaises(Exception) as error:
            self.parser.parse([], {'DAST_PATHS': '/1'})

        self.assertIn('When DAST_PATHS is defined DAST_WEBSITE must also be set.', str(error.exception))

    def test_raises_error_when_DAST_PATHS_FILE_is_specificed_but_DAST_WEBSITE_is_not(self):
        with self.assertRaises(Exception) as error:
            self.parser.parse([], {'DAST_PATHS_FILE': 'test/end-to-end/fixtures/url-scan/paths_to_scan.txt'})

        self.assertIn('When DAST_PATHS_FILE is defined DAST_WEBSITE must also be set.', str(error.exception))

    def test_raises_error_when_DAST_PATHS_and_DAST_PATHS_FILE_are_both_specified(self):
        with self.assertRaises(Exception) as error:
            self.parser.parse(['-t', 'http://website'], {'DAST_PATHS': '/1', 'DAST_PATHS_FILE': '/path/to/file.txt'})

        self.assertIn('DAST_PATHS and DAST_PATHS_FILE can not be defined at the same time.', str(error.exception))

    def test_raises_error_when_no_target_or_api_specification(self):
        with self.assertRaises(Exception) as error:
            self.parser.parse([], {})

        self.assertIn(
            'Either DAST_WEBSITE, DAST_API_OPENAPI, or DAST_API_SPECIFICATION (deprecated) must be set',
            str(error.exception),
        )

    def test_raises_error_if_ajax_spider_and_api_scan_both_configured(self):
        with self.assertRaises(InvalidConfigurationError) as error:
            self.parser.parse([], {'DAST_USE_AJAX_SPIDER': True, 'DAST_API_SPECIFICATION': 'api_spec.yml'})

        self.assertIn(
            'The AJAX Spider (configured with DAST_USE_AJAX_SPIDER, DAST_ZAP_USE_AJAX_SPIDER, --use-ajax-spider, '
            'or -j) cannot be used with an API scan (configured with DAST_API_OPENAPI, DAST_API_SPECIFICATION '
            '(deprecated) or --api-specification)',
            str(error.exception),
        )

    def test_raise_error_when_deprecated_argument_is_passed_after_version_2(self):
        self.mock_system.return_value.dast_version.return_value = '2.6.1'

        for argument in [
            ['-T', '1'],
            ['--zap_timeout', '1'],
            ['-s'],
            ['-n', 'context.file'],
            ['-p', 'progress.file'],
            ['-D', '12'],
            ['--auth-display', 'True'],
            ['--validate-domain', 'True'],
        ]:
            with self.assertRaises(SystemExit) as system_exit, utilities.capture_output() as output:
                self.parser.parse(argument, self.DEFAULT_ENV)

            self.assertEqual(system_exit.exception.code, 2)
            self.assertIn(f'unrecognized arguments: {" ".join(argument)}', output.stderr.getvalue())

    def test_dast_major_version_is_changelog_version_when_no_future_file_present(self):
        with patch('src.config.configuration_parser.path') as path:
            path.isfile.return_value = False
            self.mock_system.return_value.dast_version.return_value = '2.6.1'

            config = self.parser.parse([], self.DEFAULT_ENV)
            self.assertEqual(config.dast_major_version, 2)

    def test_dast_major_version_is_one_plus_changelog_version_when_future_file_present(self):
        with patch('src.config.configuration_parser.path') as path:
            path.isfile.return_value = True
            self.mock_system.return_value.dast_version.return_value = '2.6.1'

            config = self.parser.parse([], self.DEFAULT_ENV)
            self.assertEqual(config.dast_major_version, 3)

    def test_aborts_when_cannot_determine_major_version(self):
        self.mock_system.return_value.dast_version.return_value = 'not.a.valid.version'

        with self.assertRaises(RuntimeError) as error:
            self.parser.parse([], self.DEFAULT_ENV)

        self.assertEqual(str(error.exception), 'Unable to parse CHANGELOG.md version not.a.valid.version, aborting.')

    def test_can_set_browserker_log_using_cli(self):
        config = self.parser.parse(['--browser-log', '  brows :debug ,  auth: error'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_log, {'brows': 'debug', 'auth': 'error'})

    def test_can_set_browserker_log_using_environment(self):
        environment = merge({'DAST_BROWSER_LOG': 'brows:info'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_log, {'brows': 'info'})

    def test_can_set_browserker_log_is_empty_when_not_supplied(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_log, {})

    def test_can_set_browserker_file_log_using_cli(self):
        config = self.parser.parse(['--browser-file-log', '  brows :debug ,  auth: error'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_file_log, {'brows': 'debug', 'auth': 'error'})

    def test_can_set_browserker_file_log_using_environment(self):
        environment = merge({'DAST_BROWSER_FILE_LOG': 'brows:info'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_file_log, {'brows': 'info'})

    def test_browserker_file_log_is_empty_when_not_supplied(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_file_log, {})

    def test_can_set_browserker_devtools_log_using_cli(self):
        config = self.parser.parse(['--browser-devtools-log',
                                    '  Default:messageAndBody,truncate:50; Network: suppress'],
                                   self.DEFAULT_ENV)
        self.assertEqual(config.browserker_devtools_log,
                         {'Default': 'messageAndBody,truncate:50', 'Network': 'suppress'})

    def test_can_set_browserker_devtools_log_using_environment(self):
        environment = merge({'DAST_BROWSER_DEVTOOLS_LOG': 'DOM.getDocument:message'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_devtools_log, {'DOM.getDocument': 'message'})

    def test_browserker_devtools_log_is_empty_when_not_supplied(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_devtools_log, {})

    def test_can_set_auth_password_using_base64_option(self):
        config = self.parser.parse(['--auth-password-base64', 'cGFzc3dvcmQ='], self.DEFAULT_AUTH_ENV)
        self.assertEqual(config.auth_password, 'password')

    def test_can_override_auth_password_using_base64_option(self):
        cliopts = ['--auth-password', 'secret', '--auth-password-base64', 'cGFzc3dvcmQ=']
        config = self.parser.parse(cliopts, self.DEFAULT_AUTH_ENV)
        self.assertEqual(config.auth_password, 'password')

    def test_can_set_auth_password_using_base64_environment(self):
        config = self.parser.parse([], merge({'DAST_PASSWORD_BASE64': 'cGFzc3dvcmQ='}, self.DEFAULT_AUTH_ENV))
        self.assertEqual(config.auth_password, 'password')

    def test_can_set_request_headers_using_base64_option(self):
        config = self.parser.parse(['--request-headers-base64', 'QWNjZXB0OiAqLyo='], self.DEFAULT_ENV)
        self.assertDictEqual(config.request_headers, {'Accept': '*/*'})

    def test_can_override_request_headers_using_base64_option(self):
        cliopts = ['--request-headers', 'Authorization: token', '--request-headers-base64', 'QWNjZXB0OiAqLyo=']
        config = self.parser.parse(cliopts, self.DEFAULT_ENV)
        self.assertDictEqual(config.request_headers, {'Accept': '*/*'})

    def test_can_set_request_headers_using_base64_environment(self):
        config = self.parser.parse([], merge({'DAST_REQUEST_HEADERS_BASE64': 'QWNjZXB0OiAqLyo='}, self.DEFAULT_ENV))
        self.assertDictEqual(config.request_headers, {'Accept': '*/*'})

    def test_sets_default_browserker_crawl_graph(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_crawl_graph, False)

    def test_can_set_default_browserker_crawl_graph_using_cli(self):
        config = self.parser.parse(['--browser-crawl-graph'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_crawl_graph, True)

    def test_can_set_default_browserker_crawl_graph_using_environment_variables(self):
        environment = merge({'DAST_BROWSER_CRAWL_GRAPH': 'true'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_crawl_graph, True)

    def test_raises_error_if_both_target_and_api_spec_set(self):
        env = {'DAST_WEBSITE': 'http://dast.target', 'DAST_API_SPECIFICATION': 'api_spec.json'}

        with self.assertRaises(InvalidConfigurationError) as error:
            self.parser.parse([], env)

        self.assertIn(
            'DAST_API_OPENAPI and DAST_API_SPECIFICATION (deprecated) cannot be used when DAST_WEBSITE is set. If you '
            'want to run an API scan, please remove DAST_WEBSITE',
            str(error.exception),
        )

    def test_raises_error_if_host_override_given_with_file_api_spec(self):
        env = {'DAST_API_HOST_OVERRIDE': 'http://dast.target', 'DAST_API_SPECIFICATION': 'api_spec.json'}

        with self.assertRaises(InvalidConfigurationError) as error:
            self.parser.parse([], env)

        self.assertIn(
            'DAST_API_HOST_OVERRIDE cannot be used when DAST_API_OPENAPI or DAST_API_SPECIFICATION (deprecated) is a '
            'file. If you want to use DAST_API_HOST_OVERRIDE, please use a URL for DAST_API_OPENAPI or '
            'DAST_API_SPECIFICATION',
            str(error.exception),
        )

    def test_sets_max_response_size_default(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_max_response_size_mb, 0)

    def test_sets_max_response_size_using_cli(self):
        config = self.parser.parse(['--browser-max-response-size-mb', '5'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_max_response_size_mb, 5)

    def test_sets_max_response_size_using_environment_variables(self):
        environment = merge({'DAST_MAX_RESPONSE_SIZE_MB': '5'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_max_response_size_mb, 5)

    def test_sets_chrome_debug_log_default(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_chrome_debug_log_dir, '')

    def test_sets_chrome_debug_log_using_cli(self):
        config = self.parser.parse(['--browser-chrome-debug-log-dir', '/path/dir'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_chrome_debug_log_dir, '/path/dir')

    def test_sets_chrome_debug_log_using_environment_variables(self):
        environment = merge({'DAST_CHROME_DEBUG_LOG_DIR': '/my/path'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_chrome_debug_log_dir, '/my/path')

    def test_sets_page_ready_selector_default(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_page_ready_selector, None)

    def test_sets_page_ready_selector_cli(self):
        config = self.parser.parse(['--browser-page-ready-selector', 'css:#page-ready'], self.DEFAULT_ENV)
        self.assertEqual(config.browserker_page_ready_selector, 'css:#page-ready')

    def test_sets_page_ready_selector_using_environment_variables(self):
        environment = merge({'DAST_BROWSER_PAGE_READY_SELECTOR': 'css:#page-ready'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.browserker_page_ready_selector, 'css:#page-ready')

    def test_sets_advertise_scan_default(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.advertise_scan, False)

    def test_sets_advertise_scan_using_cli(self):
        config = self.parser.parse(['--advertise-scan'], self.DEFAULT_ENV)
        self.assertEqual(config.advertise_scan, True)

    def test_sets_advertise_scan_using_environment_variables(self):
        environment = merge({'DAST_ADVERTISE_SCAN': 'true'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.advertise_scan, True)

    def test_sets_advertise_to_off_using_environment_variables(self):
        environment = merge({'DAST_ADVERTISE_SCAN': 'false'}, self.DEFAULT_ENV)
        config = self.parser.parse([], environment)
        self.assertEqual(config.advertise_scan, False)

    def test_sets_pkcs12_cert_default(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertIsNone(config.pkcs12_cert)
        self.assertIsNone(config.pkcs12_password)

    def test_sets_pkcs12_cert_using_cli(self):
        config = self.parser.parse(['--pkcs12-cert-base64', 'Y2VydGlmaWNhdGU=', '--pkcs12-password', 'password'],
                                   self.DEFAULT_ENV)
        self.assertEqual(config.pkcs12_cert, b'certificate')
        self.assertEqual(config.pkcs12_password, 'password')

    def test_sets_pkcs12_cert_using_environment_variables(self):
        environment = merge({
            'DAST_PKCS12_CERTIFICATE_BASE64': 'Y2VydGlmaWNhdGU=',
            'DAST_PKCS12_PASSWORD': 'my.secret',
        }, self.DEFAULT_ENV)

        config = self.parser.parse([], environment)
        self.assertEqual(config.pkcs12_cert, b'certificate')
        self.assertEqual(config.pkcs12_password, 'my.secret')

    def test_sets_pkcs12_password_must_be_set_if_using_certs(self):
        environment = merge({
            'DAST_PKCS12_CERTIFICATE_BASE64': 'Y2VydGlmaWNhdGU=',
        }, self.DEFAULT_ENV)

        with self.assertRaises(InvalidConfigurationError):
            self.parser.parse([], environment)

    def test_sets_pkcs12_password_can_be_set_to_empty_string(self):
        environment = merge({
            'DAST_PKCS12_CERTIFICATE_BASE64': 'Y2VydGlmaWNhdGU=',
            'DAST_PKCS12_PASSWORD': '',
        }, self.DEFAULT_ENV)

        config = self.parser.parse([], environment)
        self.assertEqual(config.pkcs12_cert, b'certificate')
        self.assertEqual(config.pkcs12_password, '')

    def test_sets_pkcs12_cert_must_be_set_if_using_certs(self):
        environment = merge({
            'DAST_PKCS12_PASSWORD': 'secrety-secret',
        }, self.DEFAULT_ENV)

        with self.assertRaises(InvalidConfigurationError):
            self.parser.parse([], environment)

    def test_sets_ff_browser_passive_scan_mode_default(self):
        config = self.parser.parse([], self.DEFAULT_ENV)
        self.assertEqual(config.ff_browser_passive_scan_mode, True)

    def test_sets_ff_browser_passive_scan_mode_using_cli(self):
        config = self.parser.parse(['--ff-browser-passive-scan-mode', 'false'], self.DEFAULT_ENV)
        self.assertEqual(config.ff_browser_passive_scan_mode, False)

    def test_setting_ff_browser_passive_scan_mode_disables_replaced_rules(self):
        config = self.parser.parse(['--ff-browser-passive-scan-mode', 'true', '--browser-scan'], self.DEFAULT_ENV)
        self.assertIn('10010', config.exclude_rules)
        self.assertIn('10021', config.exclude_rules)
        self.assertIn('10036', config.exclude_rules)
        self.assertIn('10037', config.exclude_rules)
        self.assertIn('10039', config.exclude_rules)
        self.assertIn('10061', config.exclude_rules)
        self.assertIn('10062', config.exclude_rules)
