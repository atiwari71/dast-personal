from unittest import TestCase
from unittest.mock import MagicMock

from src.zap_gateway.zap_server import ZAPServer
from test.unit.mock_config import ToConfig


class TestZAPServer(TestCase):

    def setUp(self) -> None:
        self.config = ToConfig()
        self.config.zap_other_options = None
        self.config.auto_update_addons = None
        self.config.silent = None

        self.system = MagicMock()
        self.request_headers_configuration_builder = MagicMock()
        self.request_headers_configuration_builder.build.return_value = ''

        self.user_configuration_builder = MagicMock()
        self.user_configuration_builder.build.return_value = ''

        self.zap_server = ZAPServer(self.config, self.system,
                                    self.request_headers_configuration_builder,
                                    self.user_configuration_builder)

    def test_should_start_zap_with_configured_port(self):
        self.config.zap_port = 5001
        zap_daemon = self.zap_server.start()

        for value in ['/zap/zap.sh', 'proxy.reverseProxy.httpPort=5001']:
            self.assertIn(value, self.system.run.mock_calls[0].kwargs['parameters'])

        self.assertEqual(ZAPServer.LOG_FILE, self.system.run.mock_calls[0].kwargs['output_file_name'])
        self.assertIsNotNone(zap_daemon)

    def test_should_run_with_sensible_defaults(self):
        self.zap_server.start()

        params = ' '.join(self.system.run.mock_calls[0].kwargs['parameters'])
        self.assertIn('-config selenium.firefoxDriver', params)
        self.assertNotIn('-addonupdate', params)
        self.assertNotIn('-silent', params)

    def test_should_auto_update_if_set(self):
        self.config.auto_update_addons = True
        self.zap_server.start()

        params = ' '.join(self.system.run.mock_calls[0].kwargs['parameters'])
        self.assertIn('-addonupdate', params)

    def test_should_default_to_unlimited_spider_mins_if_full_scan(self):
        self.config.full_scan = True
        self.zap_server.start()

        params = ' '.join(self.system.run.mock_calls[0].kwargs['parameters'])
        self.assertIn('-config spider.maxDuration=0', params)

    def test_should_default_to_one_minute_of_spidering_if_passive_scan(self):
        self.config.full_scan = False
        self.config.spider_mins = None
        self.zap_server.start()

        params = ' '.join(self.system.run.mock_calls[0].kwargs['parameters'])
        self.assertIn('-config spider.maxDuration=1', params)

    def test_should_allow_spider_mins_to_be_configurable(self):
        self.config.full_scan = True
        self.config.spider_mins = 23
        self.zap_server.start()

        params = ' '.join(self.system.run.mock_calls[0].kwargs['parameters'])
        self.assertIn('-config spider.maxDuration=23', params)

    def test_should_set_to_silent_if_set(self):
        self.config.silent = True
        self.zap_server.start()

        params = ' '.join(self.system.run.mock_calls[0].kwargs['parameters'])
        self.assertIn('-silent', params)

    def test_should_set_client_certificate_if_set(self):
        self.config.pkcs12_cert_filename = '/foo/bar/cert.pfx'
        self.config.pkcs12_password = 'secrety-secret'
        self.zap_server.start()

        params = ' '.join(self.system.run.mock_calls[0].kwargs['parameters'])
        self.assertIn('certificate.use=true', params)
        self.assertIn('certificate.pkcs12.path=/foo/bar/cert.pfx', params)
        self.assertIn('certificate.pkcs12.password=secrety-secret', params)

    def test_not_should_set_client_certificate_if_not_set(self):
        self.config.pkcs12_cert_filename = ''
        self.config.pkcs12_password = ''
        self.zap_server.start()

        params = ' '.join(self.system.run.mock_calls[0].kwargs['parameters'])
        self.assertNotIn('certificate.use', params)
        self.assertNotIn('certificate.pkcs12.path', params)
        self.assertNotIn('certificate.pkcs12.password', params)

    def test_should_include_request_headers_configuration(self):
        self.request_headers_configuration_builder.build.return_value = ['-config', 'request.headers']
        self.zap_server.start()

        params = ' '.join(self.system.run.mock_calls[0].kwargs['parameters'])
        self.assertIn('-config request.headers', params)

    def test_should_include_user_configuration(self):
        self.user_configuration_builder.build.return_value = ['-config', 'user.rules']
        self.zap_server.start()

        params = ' '.join(self.system.run.mock_calls[0].kwargs['parameters'])
        self.assertIn('-config user.rules', params)

    def test_should_include_alpha_rules(self):
        self.config.zap_include_alpha = True
        self.zap_server.start()

        params = ' '.join(self.system.run.mock_calls[0].kwargs['parameters'])
        self.assertIn('-addoninstall pscanrulesAlpha', params)
        self.assertIn('-addoninstall ascanrulesAlpha', params)

    def test_should_exclude_alpha_rules(self):
        self.config.zap_include_alpha = False
        self.zap_server.start()

        params = ' '.join(self.system.run.mock_calls[0].kwargs['parameters'])
        self.assertNotIn('pscanrulesAlpha', params)
        self.assertNotIn('ascanrulesAlpha', params)
