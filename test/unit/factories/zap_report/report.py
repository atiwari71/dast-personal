from .alert import alert as alert_factory


def report(alerts=[alert_factory()], name='http://nginx'):
    return {
        '@generated': 'Tue, 22 Oct 2019 01:01:55',
        '@version': 'D-2019-09-23',
        'site': [
            {
                '@port': '80',
                '@host': 'nginx',
                '@name': name,
                'alerts': alerts,
                '@ssl': 'false',
            },
        ],
    }


def report_with_alert(alert_values={}, **kwargs):
    return report(alerts=[alert_factory(**alert_values)], **kwargs)
