from typing import Dict, List


def scanners(*scnrs) -> List[Dict[str, str]]:
    if not scnrs:
        return [scanner()]

    return list(scnrs)


def scanner(plugin_id='10020',
            enabled='true',
            alert_threshold='DEFAULT',
            name='X-Frame-Options Header Scanner',
            quality='release') -> Dict[str, str]:
    return {'alertThreshold': alert_threshold,
            'name': name,
            'id': plugin_id,
            'enabled': enabled,
            'quality': quality}
