from src.http_header import HttpHeader
from src.http_headers import HttpHeaders


class HttpHeaderBuilder:

    def __init__(self):
        self.headers = []

    def add(self, name, value):
        self.headers.append(HttpHeader(name, value))
        return self

    def build(self):
        return HttpHeaders(self.headers)


def http_headers():
    return HttpHeaderBuilder()
