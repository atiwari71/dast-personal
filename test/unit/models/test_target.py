from unittest import TestCase

from src.models import Target
from src.models.errors import InvalidTargetError


class TestTarget(TestCase):

    def test_is_host_url_return_false_if_target_has_more_than_two_slashes(self):
        target = Target('http://target.test/some_path')

        self.assertFalse(target.is_host_url())

    def test_is_host_url_return_false_if_target_has_less_than_two_slashes(self):
        target = Target('http://target.test')

        self.assertTrue(target.is_host_url())

    def test_reset_to_host_removes_path_from_host_url(self):
        target = Target('http://target.test/some_path')

        self.assertEqual(target.reset_to_host(), 'http://target.test')

    def test_build_url_combines_the_target_and_path(self):
        target = Target('http://target.test')

        self.assertEqual(target.build_url('/path/1.html'), 'http://target.test/path/1.html')

    def test_build_url_strips_unnecessary_white_space(self):
        target = Target('http://target.test')

        self.assertEqual(target.build_url('  /path/1.html  '), 'http://target.test/path/1.html')

    def test_can_parse_hostname(self):
        self.assertEqual(Target('http://my.site.com').hostname(), 'my.site.com')
        self.assertEqual(Target('https://my.site.com').hostname(), 'my.site.com')
        self.assertEqual(Target('https://my.site.com:8080').hostname(), 'my.site.com')
        self.assertEqual(Target('https://site.com:8080/section/page?sort=true').hostname(), 'site.com')

    def test_raises_error_if_no_hostname(self):
        target = Target('http://')

        with self.assertRaises(InvalidTargetError) as error:
            target.hostname()

        self.assertIn(str(error.exception), 'Target must have a valid hostname')

    def test_when_url_is_none_target_should_be_None(self):
        self.assertFalse(Target(None))

    def test_regex_returns_target_escaped_for_regex(self):
        target = Target('http://url.test')

        self.assertEqual(target.regex, r'http://url\.test')

    def test_external_js_returns_regex_matching_js_paths_outside_the_target(self):
        target = Target('http://url.test')

        self.assertEqual(target.external_js(), r'^(?!http://url\.test).*\.js')

    def test_external_css_returns_regex_matching_css_paths_outside_the_target(self):
        target = Target('http://url.test')

        self.assertEqual(target.external_css(), r'^(?!http://url\.test).*\.css')
