from unittest import TestCase

from src.models.http.http_messages import HttpMessages
from test.unit.factories.models import http as factories


class TestHttpMessages(TestCase):

    def test_should_find_message_with_id(self):
        messages = HttpMessages([factories.http_message('1'),
                                 factories.http_message('12')])

        self.assertEqual('12', str(messages.message_with_id('12').message_id))
        self.assertEqual('1', str(messages.message_with_id('1').message_id))
        self.assertIsNone(messages.message_with_id('666'))

    def test_should_return_number_of_messages(self):
        messages = HttpMessages([factories.http_message(), factories.http_message()])
        self.assertEqual(2, len(messages))

    def test_should_return_message_at_index(self):
        messages = HttpMessages([factories.http_message('1'),
                                 factories.http_message('12')])
        self.assertEqual('1', str(messages[0].message_id))
        self.assertEqual('12', str(messages[1].message_id))

    def test_should_return_request_methods_with_url(self):
        messages = HttpMessages([factories.http_message(request=factories.http_request(method='POST',
                                                                                       url='http://nginx/page1')),
                                 factories.http_message(request=factories.http_request(method='GET',
                                                                                       url='http://nginx/page2')),
                                 factories.http_message(request=factories.http_request(method='GET',
                                                                                       url='http://nginx/page2')),
                                 factories.http_message(request=factories.http_request(method='POST',
                                                                                       url='http://nginx/page2'))])

        summaries = messages.request_summaries()

        self.assertEqual(3, len(summaries))
        self.assertEqual({'method': 'POST', 'url': 'http://nginx/page1'}, summaries[0])
        self.assertEqual({'method': 'GET', 'url': 'http://nginx/page2'}, summaries[1])
        self.assertEqual({'method': 'POST', 'url': 'http://nginx/page2'}, summaries[2])

    def test_request_summaries_should_exclude_invalid_data(self):
        messages = HttpMessages([factories.http_message(request=factories.http_request(method='',
                                                                                       url='http://nginx/page1')),
                                 factories.http_message(request=factories.http_request(method='GET',
                                                                                       url=''))])

        summaries = messages.request_summaries()

        self.assertEqual(0, len(summaries))

    def test_human_readable_urls(self):
        messages = HttpMessages([factories.http_message(request=factories.http_request(method='POST',
                                                                                       url='http://site/a')),
                                 factories.http_message(request=factories.http_request(method='GET',
                                                                                       url='http://site/b')),
                                 factories.http_message(request=factories.http_request(method='GET',
                                                                                       url='http://site/b')),
                                 factories.http_message(request=factories.http_request(method='GET',
                                                                                       url='http://site/a'))])

        lines = messages.human_readable_requests().split('\n')

        self.assertEqual(3, len(lines))
        self.assertEqual('GET http://site/a', lines[0])
        self.assertEqual('GET http://site/b', lines[1])
        self.assertEqual('POST http://site/a', lines[2])
