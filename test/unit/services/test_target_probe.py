import unittest
from unittest.mock import Mock, call, patch

from requests.exceptions import ConnectTimeout, ConnectionError, ReadTimeout

from src.models import Target
from src.services.target_probe import TargetProbe
from test.unit.mock_config import ToConfig
from test.unit.utilities import httpserver


class TestTargetProbe(unittest.TestCase):

    # site_is_available
    def test_site_is_available_when_response_is_ok(self):
        with httpserver.new().respond(status=200).build() as server:
            target_probe = TargetProbe(
                Target(server.address()),
                ToConfig(availability_timeout=1),
            )
            site_check = target_probe.send_ping()

            self.assertEqual(site_check.is_available(), True)

    def test_site_is_available_when_response_is_401(self):
        with httpserver.new().respond(status=401).build() as server:
            target_probe = TargetProbe(
                Target(server.address()),
                ToConfig(availability_timeout=1),
            )
            site_check = target_probe.send_ping()

            self.assertEqual(site_check.is_available(), True)

    def test_site_is_available_when_response_is_redirect(self):
        with httpserver.new().respond(status=302).build() as server:
            target_probe = TargetProbe(
                Target(server.address()),
                ToConfig(availability_timeout=1),
            )
            site_check = target_probe.send_ping()

            self.assertEqual(site_check.is_available(), True)

    def test_site_is_unavailable_when_response_is_not_found(self):
        with httpserver.new().respond(status=404).build() as server, \
             patch('src.services.target_probe.logging'):
            target_probe = TargetProbe(
                Target(server.address()),
                ToConfig(availability_timeout=1),
            )
            site_check = target_probe.send_ping()

            self.assertEqual(site_check.is_available(), False)

    def test_site_is_unavailable_when_cannot_connect_to_server(self):
        port = httpserver.get_free_port()
        target = f'http://127.0.0.1:{port}/'
        target_probe = TargetProbe(Target(target), ToConfig(availability_timeout=1))

        with patch('src.services.target_probe.logging'):
            site_check = target_probe.send_ping()

            self.assertEqual(site_check.is_available(), False)

    def test_site_is_unavailable_when_server_responds_slowly(self):
        with httpserver.new().respond(delay=2, status=200).build() as server, \
             patch('src.services.target_probe.logging'):
            target_probe = TargetProbe(
                Target(server.address()),
                ToConfig(availability_timeout=1),
            )
            target_probe.REQUEST_TIMEOUT = 1
            site_check = target_probe.send_ping()

            self.assertEqual(site_check.is_available(), False)

    def test_site_is_available_when_using_invalid_https_certificate(self):
        self_signed_cert = 'test/unit/fixtures/certificates/self-signed'

        with httpserver.new().https(self_signed_cert).respond(status=200).build() as server:
            target_probe = TargetProbe(
                Target(server.address()),
                ToConfig(availability_timeout=5),
            )
            site_check = target_probe.send_ping()

            self.assertEqual(site_check.is_available(), True)

    def test_send_ping_uses_custom_ProxyError_message(self):
        target_probe = TargetProbe(
            'http://test.site',
            ToConfig(availability_timeout=1),
            proxy='http://zaproxy:8001',
        )

        with patch('src.services.target_probe.logging') as mock_logging:
            target_probe.send_ping()

        mock_logging.info.assert_has_calls([
            call('Requesting access to http://test.site...'),
            call('ProxyError: failed to connect to ZAP'),
        ])

    def test_send_ping_uses_custom_ConnectionError_message(self):
        target_probe = TargetProbe(
            'http://test.site',
            ToConfig(availability_timeout=1),
        )

        with patch('src.services.target_probe.get') as mock_get, \
             patch('src.services.target_probe.logging') as mock_logging:
            mock_get.side_effect = self._mock_exception(ConnectionError)

            target_probe.send_ping()

        mock_logging.info.assert_has_calls([
            call('Requesting access to http://test.site...'),
            call('ConnectionError: failed to connect to target'),
        ])

    def test_send_ping_uses_custom_ReadTimeout_message(self):
        target_probe = TargetProbe('http://test.site', ToConfig(availability_timeout=1))

        with patch('src.services.target_probe.get') as mock_get, \
             patch('src.services.target_probe.logging') as mock_logging:
            mock_get.side_effect = self._mock_exception(ReadTimeout)

            target_probe.send_ping()

            mock_logging.info.assert_has_calls([
                call('Requesting access to http://test.site...'),
                call('ReadTimeout: request timed out while waiting for data from server'),
            ])

    def test_send_ping_uses_custom_ConnectTimeout_message(self):
        target_probe = TargetProbe('http://test.site', ToConfig(availability_timeout=1))

        with patch('src.services.target_probe.get') as mock_get, \
             patch('src.services.target_probe.logging') as mock_logging:
            mock_get.side_effect = self._mock_exception(ConnectTimeout)

            target_probe.send_ping()

        mock_logging.info.assert_has_calls([
            call('Requesting access to http://test.site...'),
            call('ConnectTimeout: request failed'),
        ])

    def test_send_ping_adds_detail_to_HttpError(self):
        with httpserver.new().respond(status=404, content='Where is Ash?').build() as server, \
             patch('src.services.target_probe.logging') as mock_logging:
            target_probe = TargetProbe(server.address(), ToConfig(availability_timeout=1))

            target_probe.send_ping()

            mock_logging.assert_has_calls([
                call.info(f'Requesting access to {server.address()}...'),
                call.info('HTTPError: 404'),
                call.debug('Where is Ash?'),
            ])

    def test_send_ping_follows_redirects_by_default(self):
        target_probe = TargetProbe('http://website.redirect', ToConfig(availability_timeout=1))

        with patch('src.services.target_probe.get') as mock_get:
            target_probe.send_ping()

        mock_get.assert_called_once_with(
            'http://website.redirect', timeout=5, verify=False, allow_redirects=True,
        )

    def test_send_ping_does_not_follow_redirects_if_asked(self):
        proxy = Mock()
        target_probe = TargetProbe(
            'http://website.redirect', ToConfig(availability_timeout=1), proxy=proxy, follow_redirects=False,
        )

        with patch('src.services.target_probe.get') as mock_get:
            target_probe.send_ping()

        mock_get.assert_called_once_with(
            'http://website.redirect',
            proxies={'http': proxy, 'https': proxy},
            timeout=5,
            verify=False,
            allow_redirects=False,
        )

    def _mock_exception(self, exception, **exception_kwargs):
        def mock_raise(*args, **kwargs):
            raise exception(exception_kwargs)

        return mock_raise
