from unittest import TestCase

from src.services.browserker import BrowserkerScanResult


class TestBrowserkerScanResult(TestCase):

    def test_scanned_urls(self):
        scanned_resources = [
            {
                'method': 'GET',
                'type': 'url',
                'url': 'http://pancakes/',
            },
            {
                'method': 'POST',
                'type': 'url',
                'url': 'http://pancakes/1.html',
            },
        ]
        result = BrowserkerScanResult(scanned_resources, [])
        self.assertEqual(['http://pancakes/', 'http://pancakes/1.html'], result.scanned_urls)
