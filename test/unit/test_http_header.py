import unittest

from src.http_header import HttpHeader


class TestHttpHeader(unittest.TestCase):

    def test_returns_header_as_dict(self):
        http_header = HttpHeader('Accept', '*/*')
        self.assertEqual(http_header.to_dict(), {'name': 'Accept', 'value': '*/*'})

    def test_should_mask_simple_header(self):
        http_header = HttpHeader('Authorization', 'Bearer 12345678987654321').mask()
        self.assertEqual(http_header.name, 'Authorization')
        self.assertEqual(http_header.value, HttpHeader.MASK)

    def test_should_mask_values_in_set_cookie(self):
        http_header = HttpHeader('Set-Cookie', 'cookie-name=cookie-value').mask()
        self.assertEqual(http_header.name, 'Set-Cookie')
        self.assertEqual(http_header.value, f'cookie-name={HttpHeader.MASK}')

    def test_should_mask_values_in_set_cookie_independent_of_case(self):
        http_header = HttpHeader('set-cookie', 'cookie-name=cookie-value').mask()
        self.assertEqual(http_header.name, 'set-cookie')
        self.assertEqual(http_header.value, f'cookie-name={HttpHeader.MASK}')

    def test_should_mask_values_in_set_cookie_when_there_is_a_semi_colon_at_end(self):
        http_header = HttpHeader('Set-Cookie', 'id=cookie-value;').mask()
        self.assertEqual(http_header.name, 'Set-Cookie')
        self.assertEqual(http_header.value, f'id={HttpHeader.MASK};')

    def test_should_not_mask_settings_in_set_cookie(self):
        http_header = HttpHeader('Set-Cookie', 'name=value; Domain=site.com; Secure; HttpOnly').mask()
        self.assertEqual(http_header.value, f'name={HttpHeader.MASK}; Domain=site.com; Secure; HttpOnly')

        http_header = HttpHeader('Set-Cookie', 'id=a3fWa; Expires=Wed, 21 Oct 2015 07:28:00 GMT').mask()
        self.assertEqual(http_header.value, f'id={HttpHeader.MASK}; Expires=Wed, 21 Oct 2015 07:28:00 GMT')

    def test_should_mask_value_in_cookie(self):
        http_header = HttpHeader('Cookie', 'name=value').mask()
        self.assertEqual(http_header.name, 'Cookie')
        self.assertEqual(http_header.value, f'name={HttpHeader.MASK}')

    def test_should_mask_values_in_cookie_independent_of_case(self):
        http_header = HttpHeader('cookie', 'session=123456789').mask()
        self.assertEqual(http_header.name, 'cookie')
        self.assertEqual(http_header.value, f'session={HttpHeader.MASK}')

    def test_should_mask_values_in_cookie_when_there_is_a_semi_colon_at_end(self):
        http_header = HttpHeader('Cookie', 'id=value;').mask()
        self.assertEqual(http_header.value, f'id={HttpHeader.MASK};')

    def test_should_mask_multiple_values_in_cookie(self):
        http_header = HttpHeader('Cookie', 'name=value; name2=value2; name3=value3').mask()
        self.assertEqual(http_header.value, f'name={HttpHeader.MASK}; name2={HttpHeader.MASK}; name3={HttpHeader.MASK}')
