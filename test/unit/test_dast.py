from unittest import TestCase
from unittest.mock import MagicMock, patch

from src.dast import DAST


class TestDAST(TestCase):

    def setUp(self) -> None:
        self._dast_error_handler = MagicMock()

        self._config = MagicMock()
        self._scan = MagicMock()

        self._container = MagicMock()
        self._container.config = self._config
        self._container.scan = self._scan

        self.dast = DAST(lambda: self._container)

    @patch('src.dast.DASTErrorHandler')
    @patch('src.dast.exit')
    def test_should_handle_errors(self, mock_exit, dast_error_handler_class):
        error = RuntimeError('simulated.error')
        self._scan.run.side_effect = error
        dast_error_handler = MagicMock()
        dast_error_handler_class.return_value = dast_error_handler
        dast_error_handler.handle.return_value = 1

        self.dast.start()

        dast_error_handler.handle.assert_called_once_with(error)
        mock_exit.assert_called_once_with(1)

    def test_should_initialize_dast_logs(self):
        with patch('src.dast.initialize_dast_logs') as mock_initialize:
            self.dast.start()

        mock_initialize.assert_called_once_with(self._config)
