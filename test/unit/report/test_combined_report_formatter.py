import json
import unittest
from collections import OrderedDict
from unittest.mock import MagicMock

from src.report import CombinedReportFormatter


class TestCombinedReportFormatter(unittest.TestCase):

    def test_should_combine_output_from_formatters(self):
        formatter_a = MagicMock(format_report=MagicMock(return_value=OrderedDict({'animal': 'platypus'})))
        formatter_b = MagicMock(format_report=MagicMock(return_value=OrderedDict({'reptile': 'crocodile'})))

        report = CombinedReportFormatter(formatter_a, formatter_b).format_report(
            'version', [], '', None, 'spider', 'spider_result')
        self.assertEqual(json.dumps(report), '{"animal": "platypus", "reptile": "crocodile"}')

    def test_should_combine_output_from_formatters_in_alphabetical_order(self):
        formatter_a = MagicMock(format_report=MagicMock(return_value=OrderedDict({'a': 1})))
        formatter_b = MagicMock(format_report=MagicMock(return_value=OrderedDict({'c': 1})))
        formatter_c = MagicMock(format_report=MagicMock(return_value=OrderedDict({'b': 1})))

        formatter = CombinedReportFormatter(formatter_a, formatter_b, formatter_c)
        report = formatter.format_report('version', [], '', None, 'spdr', 'spdr_result')
        self.assertEqual(json.dumps(report), '{"a": 1, "b": 1, "c": 1}')

    def test_should_pass_parameters_to_formatters(self):
        formatter_a = MagicMock(format_report=MagicMock(return_value=OrderedDict()))
        formatter_b = MagicMock(format_report=MagicMock(return_value=OrderedDict()))

        formatter = CombinedReportFormatter(formatter_a, formatter_b)
        formatter.format_report('version', [], '', None, 'zap_spider', 'zap_spider_result')

        formatter_a.format_report.assert_called_with('version', [], '', None, 'zap_spider', 'zap_spider_result')
        formatter_b.format_report.assert_called_with('version', [], '', None, 'zap_spider', 'zap_spider_result')

    def test_should_barf_when_formatters_return_json_keys_with_same_name(self):
        formatter_a = MagicMock(format_report=MagicMock(return_value=OrderedDict({'value': 1})))
        formatter_b = MagicMock(format_report=MagicMock(return_value=OrderedDict({'value': 2})))

        try:
            CombinedReportFormatter(formatter_a, formatter_b).format_report(
                'version', [], '', None, 'spider', 'spider_result',
            )
            self.assertTrue(False)
        except RuntimeError as e:
            self.assertEqual(str(e), "Cannot create combined report as multiple formatters produce property 'value'")
