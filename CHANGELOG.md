# GitLab DAST changelog

## v3.0.1
- Upgrade Browserker to version `0.0.78` (!601)
  - Upgrade vulnerability checks to version `1.0.18` [browserker!618](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/618)
    - Update `16.1`, `16.2`, `16.3`, `16.4`, `16.5`, `16.6`, and `693.1` to require exact header names [dast-cwe-checks!106](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/106)
    - Updates `16.2` to be more restrictive on version matching [dast-cwe-checks!107](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/107)
    - Remove unnecessary `has_authentication_cookie` requirement from check `614.1` [dast-cwe-checks!105](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/105)
    - Remove unnecessary `has_authentication_cookie` requirement from check `1004.1` [dast-cwe-checks!105](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/105)
    - Update `352.1` uniqueness template so that findings will be created for each `request_path` and `request_method` [dast-cwe-checks!108](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/108)
    - Update `359.1` and `359.2` to bring inline with GitLab documentation guidelines [dast-cwe-checks!110](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/110)
  - Fix stability issues by caching resources when the entire HTTP message is parsed [browserker!621](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/621)
  - Upgrade vulnerability checks to version `1.0.19` [browserker!620](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/620)
    - Update `359.1` and `359.2` uniqueness template so that findings will be created for each `request_method` [dast-cwe-checks!112](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/112)
- Replace [ZAP rule 10010](https://www.zaproxy.org/docs/alerts/10010/) with [1004.1](https://docs.gitlab.com/ee/user/application_security/dast/checks/1004.1.html) in browser based scan (!588)

## v3.0.0
- Upgrade ZAP add-on `Active scanner rules` to [46.0.0](https://github.com/zaproxy/zap-extensions/releases/ascanrules-v46) (!598)
- Upgrade ZAP add-on `Active scanner rules (beta)` to [40.0.0](https://github.com/zaproxy/zap-extensions/releases/ascanrulesBeta-v40) (!598)
- Upgrade ZAP add-on `Automation Framework` to [0.15.0](https://github.com/zaproxy/zap-extensions/releases/automation-v0.15.0) (!598)
- Upgrade ZAP add-on `Common Library` to [1.9.0](https://github.com/zaproxy/zap-extensions/releases/commonlib-v1.9.0) (!598)
- Upgrade ZAP add-on `GraphQL Support` to [0.9.0](https://github.com/zaproxy/zap-extensions/releases/graphql-v0.9.0) (!598)
- Upgrade ZAP add-on `Network` to [0.2.0](https://github.com/zaproxy/zap-extensions/releases/network-v0.2.0) (!598)
- Upgrade ZAP add-on `OpenAPI Support` to [27.0.0](https://github.com/zaproxy/zap-extensions/releases/openapi-v27) (!598)
- Upgrade ZAP add-on `Passive scanner rules` to [40.0.0](https://github.com/zaproxy/zap-extensions/releases/pscanrules-v40) (!598)
- Upgrade ZAP add-on `Passive scanner rules (beta)` to [29.0.0](https://github.com/zaproxy/zap-extensions/releases/pscanrulesBeta-v29) (!598)
- Upgrade ZAP add-on `Report Generation` to [0.13.0](https://github.com/zaproxy/zap-extensions/releases/reports-v0.13.0) (!598)
- Upgrade ZAP add-on `Retire.js` to [0.11.0](https://github.com/zaproxy/zap-extensions/releases/retire-v0.11.0) (!598)
- Upgrade ZAP add-on `Script Console` to [30.0.0](https://github.com/zaproxy/zap-extensions/releases/scripts-v30) (!598)
- Upgrade ZAP add-on `Selenium` to [15.8.0](https://github.com/zaproxy/zap-extensions/releases/selenium-v15.8.0) (!598)
- Upgrade ZAP add-on `Linux WebDrivers` to [38.0.0](https://github.com/zaproxy/zap-extensions/releases/webdriverlinux-v38) (!598)
- Upgrade ZAP add-on `WebSockets` to [25.0.0](https://github.com/zaproxy/zap-extensions/releases/websocket-v25) (!598)

## v2.28.0
- Upgrade Browserker to version `0.0.77` (!599)
  - Don't create a finding when there is no HTTP message [browserker!617](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/617)
  - Update `352.1` passive check logic [browserker!616](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/616)

## v2.27.0
- Upgrade Browserker to version `0.0.76` (!597)
  - Upgrade vulnerability checks to version `1.0.17` [browserker!602](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/602)
    - Checks `16.2`, `16.3`, `16.4`, `16.5`, and `16.6` use a named matcher for report uniqueness [dast-cwe-checks!104](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/104)
  - Fix bug in response handling of cached headers [browserker!599](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/599)
  - Upgrade GCD to version `2.2.5` [browserker!606](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/606)
  - Enable `359.1` passive check [browserker!590](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/590)
  - Sort headers in vulnerabilities evidence to encourage deterministic results [browserker!610](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/610)
  - User can print orphan DevTools event summary when `LogRequestErrorReport` is turned on [browserker!605](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/605)
  - Fix error where building HTTP messages from resources prints a warning when DevTools events are not present [browserker!611](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/611)
  - Enable `359.2` passive check [browserker!609](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/609)
  - Fix `829.1` passive check to only match `link` tags with the `rel` attribute of `stylesheet` and `script` [browserker!604](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/604)
- Replace [ZAP rule 10062](https://www.zaproxy.org/docs/alerts/10062/) with [359.1](https://docs.gitlab.com/ee/user/application_security/dast/checks/359.1.html) and [359.2](https://docs.gitlab.com/ee/user/application_security/dast/checks/359.2.html) in browser based scan (!586)

## v2.26.0
- Upgrade Browserker to version `0.0.75` (!594)
  - Upgrade vulnerability checks to version `1.0.16` [browserker!596](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/596)
  - Update `1004.1` uniqueness template to fix incorrect variable from `request_url` to `request_path` [dast-cwe-checks!103](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/103)
  - Upgrade vulnerability checks to version `1.0.15` [browserker!596](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/596)
    - Add `16.8` Content-Security-Policy analysis [dast-cwe-checks!101](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/101)
    - Add `16.9` Content-Security-Policy-Report-Only analysis [dast-cwe-checks!101](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/101)
    - Add `16.10` Content-Security-Policy violations [dast-cwe-checks!101](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/101)
    - Update `1004.1` uniqueness template so that findings will be created for each `request_url` and `request_method` [dast-cwe-checks!102](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/102)
  - Upgrade vulnerability checks to version `1.0.14` [browserker!581](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/581)
    - Add `319.1` Mixed Content [dast-cwe-checks!97](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/97)
    - Update `829.1` uniqueness template so that findings will be created for each matching tag [dast-cwe-checks!99](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/99)
  - Matchers can return a non-applicable state to reduce false positives [browserker!587](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/587)
  - Add `request_method` placeholder to allow `Findings` to be created per request method [browserker!594](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/594)
  - Fix error where building HTTP messages from cached resources prints a warning to the log [browserker!600](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/600)
  - Fix error that caused a panic when persisting navigations captured during authentication [browserker!601](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/601)
  - User can configure `LogRequestErrorReport` to output a report containing missing DevTool events [browserker!598](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/598)
- Add `DAST_BROWSER_LOG_REQUEST_ERROR_REPORT` to allows users to output report containing requests that did not load correctly (!594)
- Reinstate check `614.1` as the false positive issue has been resolved (!595)

## v2.25.0
- Upgrade Browserker to version `0.0.73` (!589)
  - Persist navigation results captured during the authentication phase [browserker!558](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/558)
  - Enable `829.2` passive check [browserker!573](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/573)
  - Associate all response headers with a HTTP response [browserker!576](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/576)
  - Check navigations captured during authentication for vulnerability findings [browserker!577](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/577)
  - Enable `829.1` passive check [browserker!569](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/569)
  - Upgrade vulnerability checks to version `1.0.13` [browserker!579](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/579)
    - Remove unnecessary matchers from `598.1` [dast-cwe-checks!95](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/95)
  - Upgrade vulnerability checks to version `1.0.12` [browserker!579](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/579)
    - Add `metadata` to schema to allow for arbitrary key-values on matchers [dast-cwe-checks!91](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/91)
    - Add `209.1` Generation of error message containing sensitive information [dast-cwe-checks!82](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/82)
    - Add `209.2` Generation of database error message containing sensitive information [dast-cwe-checks!82](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/82)
    - Change `829.2` uniqueness template to use console text [dast-cwe-checks!93](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/93)
  - Authentication cookie attribute matches only match for responses that set the authentication cookie [browserker!579](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/579)
- Fix issue where the request headers were used as response headers in finding evidence (!591)
- Upgrade Browserker to version `0.0.74` (!590)
  - Sort aggregated vulnerabilities by summary for more deterministic results [browserker!578](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/578)
- Replace [ZAP rule 10017](https://www.zaproxy.org/docs/alerts/10017/) and [ZAP rule 90003](https://www.zaproxy.org/docs/alerts/90003/) with [829.1](https://docs.gitlab.com/ee/user/application_security/dast/checks/829.1.html) and [829.2](https://docs.gitlab.com/ee/user/application_security/dast/checks/829.2.html) in browser based scan (!590)

## v2.24.0
- Split out `598.1` passive check from into two distinct checks (`598.1` and `598.3`)
- Upgrade Browserker to version `0.0.72` (!587)
  - Add support for `request_url_contains_password` matcher in vulnerability checks [browserker!561](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/561)
  - Add support for `response_has_valid_strict_transport_security_header` matcher in vulnerability checks [browserker!561](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/561)
  - Enable `598.3` passive check [browserker!560](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/560)
  - Upgrade vulnerability checks to version `1.0.11` [browserker!560](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/560)
    - Convert `16.7` and `598.{1,2,3}` to standard checks [dast-cwe-checks!90](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/90)

## v2.23.0
- Replace [ZAP rule 10024](https://www.zaproxy.org/docs/alerts/10024/) with [598.2](https://docs.gitlab.com/ee/user/application_security/dast/checks/598.2.html) in browser based scan (!586)
- Upgrade Browserker to version `0.0.71` (!586)
  - Enable `598.2` passive check [browserker!553](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/553)
  - Upgrade vulnerability checks to version `1.0.10` [browserker!553](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/553)
    - Add `598.3` Use of GET request method with sensitive query strings (Authorization header details) [dast-cwe-checks!86](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/86)
    - Convert `598.2` to implementer defined check [dast-cwe-checks!87](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/87)

## v2.22.0
- Replace [ZAP rule 3](https://www.zaproxy.org/docs/alerts/3/) with [598.1](https://docs.gitlab.com/ee/user/application_security/dast/checks/598.1.html) in browser based scan (!543)

## v2.21.0
- Upgrade Browserker to version `0.0.70` (!583)
  - Upgrade vulnerability checks to version `1.0.9` [browserker!542](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/542)
    - Update 614.1 uniqueness template so that findings will be created for each matching cookie [dast-cwe-checks!80](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/80)
    - Update 1004.1 uniqueness template so that findings will be created for each matching cookie [dast-cwe-checks!80](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/80)
  - User can add debug details of vulnerability findings to the Secure report [browserker!549](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/549)
- Add `DAST_BROWSER_FILE_LOG_PATH` to allows users to configure where the browser-based scan file log is written (!583)
- Add `DAST_BROWSER_INCLUDE_ONLY_RULES` to allows users to configure which rules are run by the browser-based scan (!583)
- Add `DAST_BROWSER_SECURE_REPORT_EXTRA_INFO` to allows users to add debug information to vulnerability findings (!583)
- Upgrade JPype to version `1.3.0` (!581)

## v2.20.0
- Add additional logging around JVM actions (!579)
- Log the Python segmentation fault stack trace when `DAST_DEBUG` is `true` (!580)
- Add `DAST_BROWSER_CRAWL_GRAPH` to generate a svg (!560)

## v2.19.0
- Disable check `614.1` as it is creating a false positive on every request (!578)

## v2.18.0
- Users can configure the names of authentication cookies for a browser-based scan using `DAST_AUTH_COOKIES` (!575)
- Upgrade Browserker to version 0.0.69 (!577)
  - A passive check can create multiple findings [browserker!524](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/524)
  - Add support for `has_authorization_header` matcher in vulnerability checks [browserker!530](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/530)
  - Add support for `is_authenticated` matcher in vulnerability checks [browserker!526](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/526)
  - Add support for `request_url_contains_session_id` matcher in vulnerability checks [browserker!532](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/532)
  - Add support for `request_url_contains_authorization_header_value` matcher in vulnerability checks [browserker!535](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/535)
  - Each finding is saved in a separate transaction to avoid large transaction errors [browserker!533](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/533)
  - Limit the number of vulnerability findings in `gl-dast-report.json` to 100,000 [browserker!527](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/527)
  - Log when the number of vulnerability findings is limited in the `gl-dast-report.json` [browserker!528](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/528)
  - Add support for `request_form_value` matcher in vulnerability checks [browserker!539](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/539)
  - Matchers can partially match content so they can be combined with `not` matchers to create findings [browserker!537](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/537)
  - Add support for `has_request_body` matcher in vulnerability checks [browserker!538](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/538)
  - Add support for `has_request_query_parameters` matcher in vulnerability checks [browserker!538](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/538)
  - Enable `598.1` passive check [browserker!536](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/536)
  - Upgrade vulnerability checks to version `1.0.8` [browserker!536](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/536)
    - Convert 598.1 from active to passive check [dast-cwe-checks!77](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/77)
  - User can generate the crawl graph as an SVG [browserker!507](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/507)

## v2.17.0
- Limit the number of reported vulnerabilities in `gl-dast-report.json` to 100,000 (!573)
- Sort browser-based detected vulnerabilities and ZAP detected vulnerabilities together (!572)
- Upgrade ZAP add-on `Active scanner rules` to [44.0.0](https://github.com/zaproxy/zap-extensions/releases/ascanrules-v44) (!571)
- Upgrade ZAP add-on `Automation Framework` to [0.12.0](https://github.com/zaproxy/zap-extensions/releases/automation-v0.12.0) (!571)
- Upgrade ZAP add-on `Call Home` to [0.3.0](https://github.com/zaproxy/zap-extensions/releases/callhome-v0.3.0) (!571)
- Upgrade ZAP add-on `Common Library` to [1.7.0](https://github.com/zaproxy/zap-extensions/releases/commonlib-v1.7.0) (!571)
- Upgrade ZAP add-on `Fuzzer` to [13.6.0](https://github.com/zaproxy/zap-extensions/releases/fuzz-v13.6.0) (!571)
- Upgrade ZAP add-on `GraphQL Support` to [0.8.0](https://github.com/zaproxy/zap-extensions/releases/graphql-v0.8.0) (!571)
- Upgrade ZAP add-on `Import files containing URLs` to [9.0.0](https://github.com/zaproxy/zap-extensions/releases/importurls-v9) (!571)
- Upgrade ZAP add-on `Network` to [0.1.0](https://github.com/zaproxy/zap-extensions/releases/network-v0.1.0) (!571)
- Upgrade ZAP add-on `OAST Support` to [0.10.0](https://github.com/zaproxy/zap-extensions/releases/oast-v0.10.0) (!571)
- Upgrade ZAP add-on `OpenAPI Support` to [26.0.0](https://github.com/zaproxy/zap-extensions/releases/openapi-v26) (!571)
- Upgrade ZAP add-on `Passive scanner rules` to [38.0.0](https://github.com/zaproxy/zap-extensions/releases/pscanrules-v38) (!571)
- Upgrade ZAP add-on `Report Generation` to [0.12.0](https://github.com/zaproxy/zap-extensions/releases/reports-v0.12.0) (!571)
- Upgrade ZAP add-on `Retire.js` to [0.10.0](https://github.com/zaproxy/zap-extensions/releases/retire-v0.10.0) (!571)
- Upgrade ZAP add-on `Save Raw Message` to [7.0.0](https://github.com/zaproxy/zap-extensions/releases/saverawmessage-v7) (!571)
- Upgrade ZAP add-on `Save XML Message` to [0.3.0](https://github.com/zaproxy/zap-extensions/releases/savexmlmessage-v0.3.0) (!571)
- Upgrade ZAP add-on `Selenium` to [15.7.0](https://github.com/zaproxy/zap-extensions/releases/selenium-v15.7.0) (!571)
- Upgrade ZAP add-on `SOAP Support` to [13.0.0](https://github.com/zaproxy/zap-extensions/releases/soap-v13) (!571)
- Upgrade ZAP add-on `Linux WebDrivers` to [35.0.0](https://github.com/zaproxy/zap-extensions/releases/webdriverlinux-v35) (!571)
- Upgrade Browserker to version 0.0.68 (!574)
  - Upgrade vulnerability checks to version `1.0.7` [browserker!520](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/520)
    - Update capitalization based matchers to use `(?i)` [dast-cwe-checks!74](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/74)
    - Add `request_body_parameters` to `match_locations` [dast-cwe-checks!72](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/72)
    - Add `request_body_parameter_name` to `match_locations` [dast-cwe-checks!72](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/72)
    - Add `request_body_parameter_value` to `match_locations` [dast-cwe-checks!72](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/72)
    - Field `report_uniqueness.template` is required in the CWE checks schema [dast-cwe-checks!75](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/75)
    - Add uniqueness template of request path to check `16.7` [dast-cwe-checks!75](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/75)
    - Add uniqueness template of request path to check `601.1` [dast-cwe-checks!75](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/75)
    - Add uniqueness template of request path to check `829.1` [dast-cwe-checks!75](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/75)
    - Add uniqueness template of request path to check `829.2` [dast-cwe-checks!75](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/75)
  - Restrict `response_body` matcher by disallowing more binary content types [browserker!474](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/474)
  - Allow user to configure the name of authentication cookies using `Cookies` in `AuthDetails` TOML [browserker!509](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/509)
  - Run passive checks for HTTP messages where there is no response [browserker!517](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/517)
- Upgrade Browserker to version 0.0.67 (!570)
  - Fetch cookies from all domains [browserker!500](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/500)
  - Add support for passive check `16.7` [browserker!506](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/506)
- Upgrade Browserker to version 0.0.66 (!568)
  - Upgrade vulnerability checks to version `1.0.6` [browserker!501](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/501)
    - Define the matcher logic for the `200.1` check [dast-cwe-checks!73](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/73)
  - Change Debian repository to snapshot.debian.org to pin libraries and versions for ubuntu images [browserker!508](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/508)
- Upgrade Browserker to version 0.0.65 (!568)
  - Add support for basic and digest authentication, configurable via `AuthType: \"basic-digest\"` in `AuthDetails` TOML [browserker!481](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/481)
  - Update Chromium to 98.0.4758.80-1 [browserker!496](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/496)
  - Parse matcher `name` when reading check definitions [browserker!491](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/491)
  - Use matcher `name` to generate uniqueness templates [browserker!495](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/495)
  - Upgrade vulnerability checks to version `1.0.5` [browserker!467](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/467)
    - Add uniqueness template using request path and private IP address to check 200.1 [dast-cwe-checks!70](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/70)
    - Add uniqueness template of request path to check 352.1 [dast-cwe-checks!70](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/70)
    - Remove requirements from check 1004.1 [dast-cwe-checks!71](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/71)
    - Update check 1004.1 to use has_authentication_cookie instead of requirements [dast-cwe-checks!71](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/71)
    - Update check 1004.1 to use authentication_cookie_attribute [dast-cwe-checks!71](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/71)

## v2.16.0
- Upgrade Browserker to version 0.0.64 (!566)
  - Remove requirement for the cookie to be in the HTTP request in the `has_authentication_cookie` matcher [browserker!490](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/490)
  - Parse `UniqueBy` templates prior to the scan starting [browserker!492](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/492)
  - Upgrade Go to version `1.17.7` to include latest security patches [browserker!494](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/494)

## v2.15.0
- Upgrade Browserker to version 0.0.63 (!565)
  - Add support for `request_url` matcher in vulnerability checks [browserker!475](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/475)
  - Add support for `session_cookie_attribute` matcher in vulnerability checks [browserker!456](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/456)
  - Remove `session_cookie_attribute` matcher in favor of `authentication_cookie_attribute` [browserker!466](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/465) [browserker!482](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/482)
  - Remove `has-session-cookie` matcher in favor of `has_authentication_cookie` [browserker!465](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/465) [browserker!467](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/467) [browserker!482](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/482)
  - Remove unused `cookie_attribute` matcher [browserker!460](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/460)
  - Upgrade vulnerability checks to version `1.0.3` [browserker!463](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/463)
  - Upgrade vulnerability checks to version `1.0.4` [browserker!476](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/476)
- Replace [ZAP rule 10011](https://www.zaproxy.org/docs/alerts/10011/) with [614.1](https://docs.gitlab.com/ee/user/application_security/dast/checks/614.1.html) in browser based scan (!565)

## v2.14.0
- Add `DAST_BROWSER_FILE_LOG` to allows users to configure the log written to file (!563)
- Add `DAST_BROWSER_DEVTOOLS_LOG` to allows users to log Chrome DevTools messages (!563)

## v2.14.0
- Upgrade Browserker to version 0.0.62 (!561)
  - Log when the Crawl Report is skipped [browserker!446](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/446)
  - Sort the `vulnerabilities[].evidence.summary` field in the Secure report [browserker!450](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/450)
  - Add support for `has_session_cookie` matcher in vulnerability checks [browserker!452](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/452)
  - Add support for passive check `201.1` [browserker!426](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/426)
  - Add support for `request_body` matcher in vulnerability checks [browserker!453](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/453)
- Replace [ZAP rule 10033](https://www.zaproxy.org/docs/alerts/10033/) with [548.1](https://docs.gitlab.com/ee/user/application_security/dast/checks/548.1.html) in browser based scan (!561)
- Replace [ZAP rule 2](https://www.zaproxy.org/docs/alerts/2/) with [200.1](https://docs.gitlab.com/ee/user/application_security/dast/checks/200.1.html) in browser based scan (!562)

## v2.13.0
- Upgrade Browserker to version 0.0.60 (!555)
  - Upgrade vulnerability checks to version `1.0.1` [browserker!440](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/440)
    - Add `requirement` matcher [dast-cwe-checks!58](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/58)
    - Add `has_response` `requirement` to `16.1` [dast-cwe-checks!58](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/58)
  - Rename `has-response` requirement matcher to `has_response` (!440)
- Upgrade Browserker to version 0.0.61 (!555)
  - Associate all request headers and non-blocked cookies with a HTTP request [browserker!442](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/442)
  - Add support for `request_method` matcher in vulnerability checks [browserker!438](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/438)
  - Add support for `request_parameter_name` matcher in vulnerability checks [browserker!438](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/438)
  - All DevTools messages for a DevTools Domain can be logged using a single line of configuration [browserker!444](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/444)
  - Upgrade vulnerability checks to version `1.0.2` [browserker!445](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/445)
    - Remove markdown from titles [dast-cwe-checks!55](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/55)
    - Don't match `16.1` on `OPTIONS` [dast-cwe-checks!60](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/60)
- Replace [ZAP rule 10019](https://www.zaproxy.org/docs/alerts/10019/) with [16.1](https://docs.gitlab.com/ee/user/application_security/dast/checks/16.1.html) in browser based scan (!555)

## v2.12.0
- User must supply Mutual TLS client certificate as a base64 encoded variable `DAST_PKCS12_CERTIFICATE_BASE64` (!556)
- Aggregate findings found by the [Content Security Policy rule](https://www.zaproxy.org/docs/alerts/10055) (!558)
- Mutual TLS client certificates can have an empty password (!557)
- Upgrade Browserker to version 0.0.58 (!559)
  - Remove duplicate URLs in Secure report `vulnerabilities[].details.urls.items[]` [browserker!424](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/424)
  - Ensure Secure report field `vulnerabilities[]` is never null [browserker!424](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/424)
  - Ensure Secure report field `vulnerabilities[].links` is never null [browserker!424](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/424)
  - Sort findings prior to Secure report generation for more deterministic results [browserker!429](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/429)
  - Upgrade vulnerability checks to version `1.0.0` [browserker!418](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/418)
    - Add `548.1` [dast-cwe-checks!45](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/45)
    - Add execution mode `once_per_path_excluding_last_segment` [dast-cwe-checks!49](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/49)
    - Remove execution modes `once_per_file` and `once_per_request` [dast-cwe-checks!49](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/49)
    - Add `request_path_excluding_last_segment` as match location [dast-cwe-checks!49](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/49)
    - Add `200.1` [dast-cwe-checks!50](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/50)
- Upgrade Browserker to version 0.0.59 (!559)
  - Add support for requirement matchers in vulnerability checks [browserker!437](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/437)
  - Add support for `has-response` requirement matcher in vulnerability checks [browserker!437](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/437)
  - Add Crawl Report [browserker!384](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/384)
- Add `DAST_BROWSER_CRAWL_REPORT` to generate a report showing the screenshots visited during crawl phase (!559)

## v2.11.0
- Update ZAP to [w2022-01-04](https://github.com/zaproxy/zaproxy/releases/tag/w2022-01-04/) (!547) (!552) (!553)
- Enable client certificates for mutual TLS in legacy scans (!540)
- Upgrade ZAP add-on `Active scanner rules (beta)` to [32.0.0](https://github.com/zaproxy/zap-extensions/releases/ascanrulesBeta-v32) (!352)
- Upgrade ZAP add-on `Access Control Testing` to [7](https://github.com/zaproxy/zap-extensions/releases/accessControl-v7/) (!549)
- Upgrade ZAP add-on `Alert Filters` to [13](https://github.com/zaproxy/zap-extensions/releases/alertFilters-v13/) (!549)
- Upgrade ZAP add-on `Active scanner rules` to [43](https://github.com/zaproxy/zap-extensions/releases/ascanrules-v43/) (!549)
- Upgrade ZAP add-on `Active scanner rules (beta)` to [v39](https://github.com/zaproxy/zap-extensions/releases/ascanrulesBeta-v39/) (!549)
- Upgrade ZAP add-on `Forced Browse` to [11](https://github.com/zaproxy/zap-extensions/releases/bruteforce-v11/) (!549)
- Upgrade ZAP add-on `Call Home` to [0.1.0](https://github.com/zaproxy/zap-extensions/releases/callhome-v0.1.0/) (!549) (!553)
- Upgrade ZAP add-on `Common Library` to [1.6.0](https://github.com/zaproxy/zap-extensions/releases/commonlib-v1.6.0/) (!549)
- Upgrade ZAP add-on `Diff` to [11](https://github.com/zaproxy/zap-extensions/releases/diff-v11/) (!549)
- Upgrade ZAP add-on `Directory List v1.0` to [5](https://github.com/zaproxy/zap-extensions/releases/directorylistv1-v5/) (!549)
- Upgrade ZAP add-on `DOM XSS Active scanner rule` to [12](https://github.com/zaproxy/zap-extensions/releases/domxss-v12/) (!549)
- Upgrade ZAP add-on `Encoder` to [0.6.0](https://github.com/zaproxy/zap-extensions/releases/encoder-v0.6.0/) (!549)
- Upgrade ZAP add-on `Form Handler` to [4](https://github.com/zaproxy/zap-extensions/releases/formhandler-v4/) (!549)
- Upgrade ZAP add-on `Fuzzer` to [13.5.0](https://github.com/zaproxy/zap-extensions/releases/fuzz-v13.5.0/) (!549)
- Upgrade ZAP add-on `FuzzDB Files` to [8](https://github.com/zaproxy/zap-extensions/releases/fuzzdb-v8/) (!549)
- Upgrade ZAP add-on `Getting Started with ZAP Guide` to [13](https://github.com/zaproxy/zap-extensions/releases/gettingStarted-v13/) (!549)
- Upgrade ZAP add-on `GraalVM JavaScript` to [0.2.0](https://github.com/zaproxy/zap-extensions/releases/graaljs-v0.2.0/) (!549)
- Upgrade ZAP add-on `GraphQL Support` to [0.7.0](https://github.com/zaproxy/zap-extensions/releases/graphql-v0.7.0/) (!549)
- Upgrade ZAP add-on `Help - English` to [14](https://github.com/zaproxy/zap-core-help/releases/help-v14/) (!549)
- Upgrade ZAP add-on `HUD - Heads Up Display` to [0.13.0](https://github.com/zaproxy/zap-hud/releases/v0.13.0/) (!549)
- Upgrade ZAP add-on `Import files containing URLs` to [8](https://github.com/zaproxy/zap-extensions/releases/importurls-v8/) (!549)
- Upgrade ZAP add-on `Invoke Applications` to [11](https://github.com/zaproxy/zap-extensions/releases/invoke-v11/) (!549)
- Upgrade ZAP add-on `Network` to [0.0.1](https://github.com/zaproxy/zap-extensions/releases/network-v0.0.1/) (!549)
- Upgrade ZAP add-on `OAST Support` to [0.7.0](https://github.com/zaproxy/zap-extensions/releases/oast-v0.7.0/) (!549)
- Upgrade ZAP add-on `Online menus` to [9](https://github.com/zaproxy/zap-extensions/releases/onlineMenu-v9/) (!549)
- Upgrade ZAP add-on `OpenAPI Support` to [24](https://github.com/zaproxy/zap-extensions/releases/openapi-v24/) (!549)
- Upgrade ZAP add-on `Plug-n-Hack Configuration` to [12](https://github.com/zaproxy/zap-extensions/releases/plugnhack-v12/) (!549)
- Upgrade ZAP add-on `Port Scanner` to [9](https://github.com/zaproxy/zap-extensions/releases/portscan-v9/) (!549)
- Upgrade ZAP add-on `Passive scanner rules` to [37](https://github.com/zaproxy/zap-extensions/releases/pscanrules-v37/) (!549)
- Upgrade ZAP add-on `Passive scanner rules (beta)` to [28](https://github.com/zaproxy/zap-extensions/releases/pscanrulesBeta-v28/) (!549)
- Upgrade ZAP add-on `Quick Start` to [33](https://github.com/zaproxy/zap-extensions/releases/quickstart-v33/) (!549)
- Upgrade ZAP add-on `Replacer` to [9](https://github.com/zaproxy/zap-extensions/releases/replacer-v9/) (!549)
- Upgrade ZAP add-on `Report Generation` to [0.10.0](https://github.com/zaproxy/zap-extensions/releases/reports-v0.10.0/) (!549)
- Upgrade ZAP add-on `Retest` to [0.2.0](https://github.com/zaproxy/zap-extensions/releases/retest-v0.2.0/) (!549)
- Upgrade ZAP add-on `Retire.js` to [0.9.0](https://github.com/zaproxy/zap-extensions/releases/retire-v0.9.0/) (!549)
- Upgrade ZAP add-on `Reveal` to [4](https://github.com/zaproxy/zap-extensions/releases/reveal-v4/) (!549)
- Upgrade ZAP add-on `Save Raw Message` to [6](https://github.com/zaproxy/zap-extensions/releases/saverawmessage-v6/) (!549)
- Upgrade ZAP add-on `Save XML Message` to [0.2.0](https://github.com/zaproxy/zap-extensions/releases/savexmlmessage-v0.2.0/) (!549)
- Upgrade ZAP add-on `Script Console` to [29](https://github.com/zaproxy/zap-extensions/releases/scripts-v29/) (!549)
- Upgrade ZAP add-on `Selenium` to [15.6.0](https://github.com/zaproxy/zap-extensions/releases/selenium-v15.6.0/) (!549)
- Upgrade ZAP add-on `SOAP Support` to [12](https://github.com/zaproxy/zap-extensions/releases/soap-v12/) (!549)
- Upgrade ZAP add-on `Ajax Spider` to [23.7.0](https://github.com/zaproxy/zap-extensions/releases/spiderAjax-v23.7.0/) (!549)
- Upgrade ZAP add-on `Tips and Tricks` to [9](https://github.com/zaproxy/zap-extensions/releases/tips-v9/) (!549)
- Upgrade ZAP add-on `Linux WebDrivers` to [34](https://github.com/zaproxy/zap-extensions/releases/webdriverlinux-v34/) (!549) (!553)
- Upgrade ZAP add-on `WebSockets` to [24](https://github.com/zaproxy/zap-extensions/releases/websocket-v24/) (!549)
- Upgrade ZAP add-on `Zest - Graphical Security Scripting Language` to [35](https://github.com/zaproxy/zap-extensions/releases/zest-v35/) (!549)
- Upgrade Browserker to version 0.0.56 (!550)
  - Add support for request_file in vulnerability checks uniqueBy.template [browserker!394](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/394)
  - Remove libcanberra-gtk* python* libpython* from the Docker image [browserker!399](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/399)
- Upgrade Browserker to version 0.0.57 (!550)
  - Add support for the SecureReport option in the TOML configuration [browserker!400](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/400)
  - Fix bug in redirect handling [browserker!410](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/410)
  - Add support for capturing security event logs from browser [browserker!415](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/415)
  - Add support for CWE check cookie_attribute matcher [browserker!396](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/396)
  - Add support for request_path_excluding_last_segment in vulnerability checks uniqueBy.template [browserker!409](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/409)
  - Add support for execution modes in checks [browserker!412](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/412)

## v2.10.0
- Disable Log4j message lookups to mitigate vulnerability CVE-2021-44228 (!541)

## v2.9.0
- Upgrade Browserker to version 0.0.55 (!537)
  - Fail a navigation if the load response is empty [browserker!385](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/385)
  - Add `PageReadySelector` to allows users to define an element that should be visible before the scan continues [browserker!389](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/389)
- Add `DAST_BROWSER_PAGE_READY_SELECTOR` to allows users to define an element that should be visible before the scan continues (!537)

## v2.8.0
- Upgrade Browserker to version 0.0.54 (!536)
  - Add support for `AlwaysRelogin` which when set to `true` forces the browser to relogin for every navigation path [browserker!387](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/387)
- Add `DAST_BROWSER_ALWAYS_RELOGIN` to force browser-based scans to relogin for every navigation path (!536)

## v2.7.0
- Upgrade Browserker to version 0.0.53 (!535)
  - Upgrade vulnerability checks to version `0.0.6` [browserker!382](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/382)
  - Enables new checks [browserker!382](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/382)
    - [16.3](https://docs.gitlab.com/ee/user/application_security/dast/checks/16.3.html)
    - [16.4](https://docs.gitlab.com/ee/user/application_security/dast/checks/16.4.html)
    - [16.5](https://docs.gitlab.com/ee/user/application_security/dast/checks/16.5.html)
    - [16.6](https://docs.gitlab.com/ee/user/application_security/dast/checks/16.6.html)
- Replace [ZAP rule 10037](https://www.zaproxy.org/docs/alerts/10037/) with [16.3](https://docs.gitlab.com/ee/user/application_security/dast/checks/16.3.html) in browser based scan (!535)
- Replace [ZAP rule 10039](https://www.zaproxy.org/docs/alerts/10036/) with [16.4](https://docs.gitlab.com/ee/user/application_security/dast/checks/16.4.html) in browser based scan (!535)
- Replace [ZAP rule 10061](https://www.zaproxy.org/docs/alerts/10036/) with:
  - [16.5](https://docs.gitlab.com/ee/user/application_security/dast/checks/16.5.html) in browser based scan (!535)
  - [16.6](https://docs.gitlab.com/ee/user/application_security/dast/checks/16.6.html) in browser based scan (!535)

## v2.6.0
- Upgrade Browserker to version 0.0.52 (!534)
  - Fix bug in secure report `vulnerabilities[]` sort order [browserker!381](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/381)

## v2.5.0
- Upgrade Browserker to version 0.0.51 (!532)
  - Fix bug that prevented all timeout configuration options from being overridden [browserker!380](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/380)
- Replace [ZAP rule 10036](https://www.zaproxy.org/docs/alerts/10036/) with [16.2](https://docs.gitlab.com/ee/user/application_security/dast/checks/16.2.html) in browser based scan (!526)
- Upgrade Browserker to version 0.0.50 (!529)
  - Update badger dependency to v3 [browserker!376](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/376)
  - Update secure report to sort `vulnerabilities[]` [browserker!379](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/379)
- Upgrade Browserker to version 0.0.49 (!529)
  - Fix authentication bug that prevented login submit button from being clicked [browserker!375](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/375)
- Upgrade Browserker to version 0.0.48 (!529)
  - Add support for `response_header_value` in check template [browserker!363](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/363)

## v2.4.0
- Upgrade Browserker to version 0.0.47 (!525)
  - Upgrade vulnerability checks to version `0.0.4` [browserker!373](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/373)
  - Add support for CWE check `name_value_match` type and `request_headers` matcher [browserker!373](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/373)

## v2.3.0
- Upgrade Browserker to version 0.0.46 (!524)
  - Update vulnerability scanner to only run checks on HTTP requests [browserker!369](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/369)
  - Fix bug that allowed `data:` URLs to be cached [browserker!371](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/371)
  - Update `vulnerabilities[].evidence.summary` to truncate when the field exceeds 20000 characters in length [browserker!372](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/372)

## v2.2.0
- Upgrade Browserker to version 0.0.45 (!522)
  - Fix a bug where the submit button was not found due to dynamically modified elements in login form [browserker!366](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/366)

## v2.1.1
- Upgrade Browserker to version 0.0.44 (!521)
  - Update `vulnerabilities[].identifiers[].url` to link to check documentation on GitLab.com [browserker!361](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/361)
  - Upgrade vulnerability checks to version `0.0.3` [browserker!361](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/361)

## v2.1.0
- Replace ZAP rule [X-Content-Type-Options Header Missing](https://www.zaproxy.org/docs/alerts/10021/) with [Missing X-Content-Type-Options: nosniff](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/blob/2c353e817df629928679bca3d977555777740e95/checks/693/693.1.yaml) for all Browser Based DAST scans (!518)

## v2.0.11
- Upgrade Browserker to version 0.0.43 (!520)
  - Add `vulnerabilities.links` to the secure report [browserker!356](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/356)
  - Add `vulnerabilities.severity` to the secure report [browserker!358](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/358)

## v2.0.10
- Upgrade Browserker to version 0.0.42 (!517)
  - Add `vulnerabilities[].discovered_at` to the secure report [browserker!346](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/346)
  - Add `vulnerabilities[].id` to the secure report [browserker!350](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/350)
  - Fix a potential race closing the browser [browserker!351](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/351)
  - Add `vulnerabilities.evidence.summary` to secure report [browserker!348](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/348)
  - Add `scan.analyzer` to the secure report [browserker!349](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/349)
  - Add `vulnerabilities.solution` to the secure report [browserker!354](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/354)
  - Add vulnerabilities.message to the secure report [browserker!355](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/355)

## v2.0.9
- Upgrade Browserker to version 0.0.41 (!505)
  - Add `vulnerabilities[].location.hostname` to the secure report [browserker!337](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/337)
  - Add `vulnerabilities[].location.method` to the secure report [browserker!337](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/337)
  - Add `vulnerabilities[].location.param` to the secure report [browserker!337](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/337)
  - Add `vulnerabilities[].location.path` to the secure report [browserker!337](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/337)
  - Add `scan.status` to the secure report [browserker!338](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/338)
  - Add `scan.type` to the secure report [browserker!338](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/338)
  - Sort `scan.scanned_resources[]` by `scan.scanned_resources[].url` and `scan.scanned_resources[].method` [browserker!341](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/341)
  - Report noisy vulnerabilities as a single finding [browserker!345](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/345)
- Upgrade ZAP add-on `Linux WebDrivers` to [29.0.0](https://github.com/zaproxy/zap-extensions/releases/webdriverlinux-v29) (!506)
- Treat HTTP `401` response as successful during probe (!511)

## v2.0.8
- Remove Firefox and Gecko from DAST image (!502)
- Update `vulnerabilities[].discovered_at` to be empty when reported time is unknown (!500)
- Add `jq` to DAST image (!503)

## v2.0.7
- Upgrade Browserker to version 0.0.38 (!499)
  - Escape backslash characters when setting local/session storage [browserker!281](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/281)
  - Allow user to configure logging for Chrome Dev Tool interactions [browserker!282](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/282)
  - Fix a bug where network messages were coming in out of order [browserker!280](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/280)
  - Add an HTTP response caching mechanism on by default [browserker!278](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/278)
  - Allow user to configure disabling cache mechanism with `--disablecache` [browserker!278](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/278)
  - Update GCD to version `2.2.1` for supporting ChromeResponseErrors as go errors [browserker!289](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/289)
- Upgrade Browserker to version 0.0.39 (!499)
  - Incorporate GitLab vulnerability checks into Browserker for future parsing and execution [browserker!290](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/290)
  - Log vulnerability definitions that are not supported [browserker!298](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/298)
  - Add `vulnerabilities[].confidence` hardcoded to `Medium` to the secure report [browserker!300](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/300)
  - Allow user to log the GoRoutineID with each log message [browserker!305](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/305)
  - Add `vulnerabilities[].cve` to the secure report [browserker!302](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/302)
  - Add `vulnerabilities[].evidence.request.url` to the secure report [browserker!304](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/304)
  - Add `vulnerabilities[].evidence.request.method` to the secure report [browserker!304](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/304)
  - Add `vulnerabilities[].evidence.request.headers.name` to the secure report [browserker!304](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/304)
  - Add `vulnerabilities[].evidence.request.headers.value` to the secure report [browserker!304](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/304)
  - Add `vulnerabilities[].evidence.response.reason_phrase` to the secure report [browserker!304](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/304)
  - Add `vulnerabilities[].evidence.response.status_code` to the secure report [browserker!304](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/304)
  - Add `vulnerabilities[].evidence.response.headers.name` to the secure report [browserker!304](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/304)
  - Add `vulnerabilities[].evidence.response.headers.value` to the secure report [browserker!304](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/304)
  - Fix a bug where elements were not properly found due to document updates [browserker!307](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/307)
  - Allow users to configure which vulnerability checks run with `OnlyIncludeChecks` [browserker!312](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/312)
  - Fix a bug where `DisableCache` was not being read [browserker!319](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/319)
- Upgrade Browserker to version 0.0.40 (!499)
  - Add support for `request_path` in vulnerability checks `uniqueBy.template` [browserker!318](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/318)
  - Add `version` to the secure report [browserker!328](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/328)
  - Add `vulnerabilities[].scanner.id` to the secure report [browserker!328](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/328)
  - Add `vulnerabilities[].scanner.name` to the secure report [browserker!328](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/328)
  - Process HTTP messages when they are confirmed loaded by Chromium [browserker!325](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/325)
  - Added `ng-click` to the list of attributes to hash on for determining uniqueness in crawling [browserker!329](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/329)
  - Allow users to configure which element attributes should be used for determining uniqueness in crawling with `CustomHashAttributes` or command line `--customhashattributes` [browserker!329](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/329)
  - Upgrade GCD to version `2.2.3` [browserker!331](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/331)
  - Allow user to log the STDOUT and STDERR of the chromium process with `LogChromiumProcessOutput` [browserker!331](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/331)
  - Add `vulnerabilities[].identifiers[].type` to the secure report [browserker!333](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/333)
  - Add `vulnerabilities[].identifiers[].name` to the secure report [browserker!333](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/333)
  - Add `vulnerabilities[].identifiers[].url` to the secure report [browserker!333](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/333)
  - Add `vulnerabilities[].identifiers[].value` to the secure report [browserker!333](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/333)
- Users can configure the browser-based cache with `DAST_BROWSER_CACHE` (!499)
- Users can configure logging browser-based Chromium output with `DAST_BROWSER_LOG_CHROMIUM_OUTPUT` (!499)

## v2.0.6
- Aggregate noisy vulnerability 10054 "Cookie set without SameSite attribute" findings and report as a single finding (!496)

## v2.0.5
- Update DAST to cancel the scan and fail when the Target site does not respond (!495)

## v2.0.4
- Include the `scan.analyzer` metadata in the JSON report output and updated schema version to 14.0.3 (!492)

## v2.0.3
- Users can configure the browser-based scans to wait for a navigation to complete with `DAST_BROWSER_NAVIGATION_TIMEOUT` (!486)
- Users can configure the browser-based scans to wait for an action to complete with `DAST_BROWSER_ACTION_TIMEOUT` (!486)
- Users can configure the browser-based scans to wait for the DOM to be stable with `DAST_BROWSER_STABILITY_TIMEOUT` (!486)
- Users can configure the browser-based scans to wait for the DOM to be stable after a navigation with `DAST_BROWSER_NAVIGATION_STABILITY_TIMEOUT` (!486)
- Users can configure the browser-based scans to wait for the DOM to be stable after a browser action executes with `DAST_BROWSER_ACTION_STABILITY_TIMEOUT` (!486)
- Users can configure the browser-based scans to restrict how long to spend searching for elements for analysis with `DAST_BROWSER_SEARCH_ELEMENT_TIMEOUT` (!486)
- Users can configure the browser-based scans to restrict how long to spend extracting for elements for analysis with `DAST_BROWSER_EXTRACT_ELEMENT_TIMEOUT` (!486)
- Users can configure the browser-based scans to wait for an element to be considered ready for analysis with `DAST_BROWSER_ELEMENT_TIMEOUT` (!486)
- Upgrade Browserker to version 0.0.36 (!486)
  - Return an internal error to user in the event a scan gets stuck for more than 15 minutes [browserker!264](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/264)
  - Allow user to configure how long to wait for a navigation to complete with `--navigationtimeout` [browserker!264](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/264)
  - Allow user to configure how long to wait for searching elements with `--searchelementtimeout` [browserker!264](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/264)
  - Allow user to configure how long to wait for extracting elements with `--extractelementtimeout` [browserker!264](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/264)
  - Allow user to configure how long to wait after executing a browser action with `--actiontimeout` [browserker!264](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/264)
  - Allow user to configure how long to wait for elements to be considered ready for analysis with `--elementreadytimeout` [browserker!264](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/264)
  - Allow user to configure how long to wait for the DOM to be stable with `--stabilitytimeout` [browserker!264](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/264)
  - Allow user to configure how long to wait after a navigation with `--waitafternavigation` [browserker!264](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/264)
  - Allow user to configure how long to wait after an action with `--waitafteraction` [browserker!264](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/264)
  - Add `vulnerabilities[].evidence.request.url` to the secure report [browserker!260](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/260)
- Upgrade Browserker to version 0.0.37 (!487)
  - Selectors use an ID, name or css selector by default when the selector type is not set [browserker!271](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/271)

## v2.0.2
- Update DAST to handle Browserker errors gracefully (!476)
- Don't advertise DAST scans with the 'Via'/'Via-Scanner' headers by default (!482)
- User can choose to advertise DAST scans using `DAST_ADVERTISE_SCAN` (!482)
- Search for the first submit button or button when using manual authentication and `DAST_SUBMIT_FIELD` is not present (!483)

## v2.0.1
- Upgrade Browserker to version 0.0.35 (!473)
  - Add `vulnerabilities[].category` to the secure report [browserker!253](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/253)
  - Add `vulnerabilities[].confidence` to the secure report [browserker!253](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/253)
  - Add `vulnerabilities[].cve` to the secure report [browserker!253](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/253)
  - Add `vulnerabilities[].description` to the secure report [browserker!253](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/253)
  - Redact the password from the authentication report [browserker!258](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/258)
- Upgrade ZAP to weekly version 2020-09-14 (!478)

## v2.0.0
- Upgrade Browserker to version 0.0.33 (!465)
  - Add the `PathToLoginForm` navigations to the authentication report [browserker!236](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/236)
  - Redact the username and password from all logs [browserker!239](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/239)
  - The `auth` command saves the cookie report when cookie report path is configured  [browserker!242](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/242)
  - User can configure browser viewport size [browserker!241](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/241)
  - Selectors use a name selector by default when the selector type is not set [browserker!252](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/252)
  - Update to Chromium browser 90.0.4430.212, fix for scans can sometimes fail to exit [browserker!200](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/200)
  - User can export the Chromium browser log [browserker!251](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/251)
  - Stability fixes for the browser resource pool [browserker!251](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/251)
  - Add support for GitLab Secure Report format generation using `--secure-report` [browserker!244](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/244)
- Upgrade ZAP add-on `Linux WebDrivers` to [28.0.0](https://github.com/zaproxy/zap-extensions/releases/webdriverlinux-v28) (!465)
- Remove ZAP add-on `MacOS WebDrivers` as it is not used (!465)
- Remove ZAP add-on `Windows WebDrivers` as it is not used (!465)
- Replace DAST selenium-based authentication with browser-based authentication (!465)
- Redact the logged password when using Browserker authentication (!465)
- Update ChromeDriver to version `90.0.4430.24` to support Chromium 90 (!465)
- Remove `-n`, `-s`, `-p`, `-D`, and `--auth-display` config options (!460)
- Remove `@generated`, `@version`, `site` and `spider` fields from DAST report (!460)
- Remove `DAST_AUTH_EXCLUDE_URLS`, `AUTH_EXCLUDE_URLS`, `AUTH_URL`, `AUTH_USERNAME`, `AUTH_PASSWORD`, `AUTH_USERNAME_FIELD`, `AUTH_PASSWORD_FIELD`, `AUTH_SUBMIT_FIELD`, `AUTH_FIRST_SUBMIT_FIELD`, `AUTH_AUTO`, and `DAST_REQUEST_HEADER` config options (!460)
- Remove domain validation (!460)
- Replace `-T` argument with `--zap-max-connection-attempts` and `--passive-scan-max-wait-time` (!460)
- Replace Firefox with Chrome for ZAP Crawljax (!460)
- Set `DAST_SPIDER_START_AT_HOST` default to false (!460)
- Add environment variable alias `DAST_AUTH_REPORT` for `DAST_BROWSER_AUTH_REPORT` (!468)
- Add environment variable alias `DAST_AUTH_VERIFICATION_LOGIN_FORM` for `DAST_BROWSER_AUTH_VERIFICATION_LOGIN_FORM` (!468)
- Add environment variable alias `DAST_AUTH_VERIFICATION_SELECTOR` for `DAST_BROWSER_AUTH_VERIFICATION_SELECTOR` (!468)
- Aggregate noisy vulnerabilities findings and report as a single finding (!466)
- Show Browserker log output in real time (!465)
- Users can configure the browser-based chrome debug log directory using `DAST_CHROME_DEBUG_LOG_DIR` (!469)
- Users can configure the browser-based maximum accepted response size using `DAST_MAX_RESPONSE_SIZE_MB` (!469)
- Upgrade Browserker to version 0.0.34 (!469)
  - Allow configuration of response body limit, defaulting to 10MB [browserker!254](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/254)
  - Selectors use a name or ID selector by default when the selector type is not set [browserker!255](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/255)
  - Allow user to set `--proxy`, `--customheaders`, and `--customcookies` via command line [browserker!256](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/256)

## v1.54.0
- Add `DAST_BROWSER_PATH_TO_LOGIN_FORM` to click on elements prior to filling in a Browserker login form (!454)
- Exclude several rules by default (!456)
- Upgrade Browserker to version 0.0.32 (!459)
  - Styled the authentication report to look more like a GitLab artifact [browserker!234](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/234)
- Raise an error when authentication is incorrectly configured (!462)

## v1.53.0
- Requests sent by DAST include a `Via: GitLab DAST/ZAP v[version]` header (!450)
- Add `DAST_ONLY_INCLUDE_RULES` to allow users to restrict the rules that are run to the given list (!444)

## v1.52.0
- Upgrade Browserker to version 0.0.31 (!452)
  - Fix a bug where cached response bodies were empty [browserker!229](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/229)
  - Users can click on elements to show the login form when authenticating using the PathToLoginForm configuration [browserker!222](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/222)
  - Redact the username and password from the JSON report [browserker!224](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/224)

## v1.51.0
- Upgrade ZAP to weekly version 2020-08-26 (!445)
- Upgrade ZAP add-on `Forced Browse` to [10.0.0](https://github.com/zaproxy/zap-extensions/releases/bruteforce-v10) (!445)
- Upgrade ZAP add-on `Common Library` to [1.2.0](https://github.com/zaproxy/zap-extensions/releases/commonlib-v1.2.0) (!445)
- Upgrade ZAP add-on `Fuzzer` to [13.1.0](https://github.com/zaproxy/zap-extensions/releases/fuzz-v13.1.0) (!445)
- Upgrade ZAP add-on `Getting Started with ZAP Guide` to [12.0.0](https://github.com/zaproxy/zap-extensions/releases/gettingStarted-v12) (!445)
- Upgrade ZAP add-on `Help - English` to [11.0.0](https://github.com/zaproxy/zap-core-help/releases/help-v11) (!445)
- Upgrade ZAP add-on `Online menus` to [8.0.0](https://github.com/zaproxy/zap-extensions/releases/onlineMenu-v8) (!445)
- Upgrade ZAP add-on `Open API Support` to [17.0.0](https://github.com/zaproxy/zap-extensions/releases/openapi-v17) (!445)
- Upgrade ZAP add-on `Quick Start` to [29.0.0](https://github.com/zaproxy/zap-extensions/releases/quickstart-v29) (!445)
- Upgrade ZAP add-on `Script Console` to [27.0.0](https://github.com/zaproxy/zap-extensions/releases/scripts-v27) (!445)
- Upgrade ZAP add-on `Selenium` to [15.3.0](https://github.com/zaproxy/zap-extensions/releases/selenium-v15.3.0) (!445)
- Upgrade ZAP add-on `Websockets` to [23.0.0](https://github.com/zaproxy/zap-extensions/releases/websocket-v23) (!445)
- Upgrade Browserker to version 0.0.30 (!451)
  - Fix a bug where manual authentication was not reporting selector failures [browserker!209](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/209)
  - Fix a bug where large response bodies cause navigation failures [browserker!212](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/212)
  - Authentication report will output result message [browserker!215](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/215)
  - Enhance auto login to check cookie and storage value lengths [browserker!217](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/217)
  - Enhance auto login to check cookie and storage value randomness [browserker!220](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/220)
  - Advertise Browserker scanner by adding a request header to all requests [browserker!216](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/216)
- Update Browserker scans to aggregate vulnerabilities by default (!449)

## v1.50.0
- The report field `vulnerabilities[].evidence.summary` is truncated to 20,000 characters (!432)
- Remove all references to WASC from the DAST report (!442)
- Browserker scans no longer ping the target as part of the scan (!443)
- Redact `DAST_PASSWORD` from Selenium logs when debug mode is enabled (!431)
- Disable `https://www.zaproxy.org/docs/alerts/10109` for all scans (!446)
- Add `DAST_API_OPENAPI` configuration variable (!441)
- Add ZAP add-on `Access Control Testing` [6.0.0](https://github.com/zaproxy/zap-extensions/releases/accessControl-v6) (!445)
- Add ZAP add-on `Form Handler` [3.0.0](https://github.com/zaproxy/zap-extensions/releases/formhandler-v3) (!445)

## v1.49.0
- Remove auto-tagging of requests to reduce scan time (!424)
- Upgrade Browserker to version `0.0.29` (!438)
- Users can create a report to help identify authentication issues for Browserker scans (!436)

## v1.48.0
- Load external resources when running the AJAX spider (!415)
- Add `DAST_MAX_URLS_PER_VULNERABILITY` to allow users to adjust and turn off limiting for reported aggregated vulnerabilities (!429)
- Update aggregated vulnerabilities `cve` to include `-aggregated` to enable the vulnerability dashboard to differentiate the two types (!434)

## v1.47.0
- Browserker configuration options can be configured using the naming convention to `DAST_BROWSER_*` (!425)
- Redact `DAST_PASSWORD` from evidence summary (!428)

## v1.46.0
- Upgrade Browserker to version `0.0.27` (!413)
- Support `DAST_PATHS_FILE` for Browserker scans (!413)
- Add support for encoded environment variables (`DAST_PASSWORD_BASE64` and `DAST_REQUEST_HEADERS_BASE64`) for DAST on-demand scans (!418)
- User can verify authentication using a URL using `DAST_AUTH_VERIFICATION_URL` for Browserker scans (!422)
- User can verify authentication using a selector using `DAST_BROWSERKER_AUTH_VERIFICATION_SELECTOR` for Browserker scans (!422)
- User can verify authentication by detecting login forms using `DAST_BROWSERKER_AUTH_VERIFICATION_LOGIN_FORM` for Browserker scans (!422)
- Raise errors for invalid API scan configuration (!420)

## v1.45.0
- Upgrade Browserker to version `0.0.25` (!402)
- Support `DAST_PATHS` for Browserker scans (!402)
- Check `CI_PROJECT_DIR` for the file specified in `DAST_PATHS_FILE` (!399)

## v1.44.0
- Updates field `vulnerabilities.scanner` within the `gl-dast-report.json` to show as Browserker when a Browserker scan runs (!406)
- User can configure the Browserker log using `DAST_BROWSERKER_LOG` (!405)

## v1.43.0
- Update the `BrowserkerScan` to use the cookies created when Browserker authenticates when scanning (!398)
- Update Browserker to use AuthDetails instead of FormData for auth (!397)
- Enable Code Source Disclosure API rules (!403)

## v1.42.0
- Updated DAST to scan all URLs found by Browserker (!387)

## v1.41.0
- Upgrade Browserker to version `0.0.20` (!385)

## v1.40.0
- Build the next version of DAST as an alpha release (!373)
- Updated API Scanning policy rules to include higher risk checks and disable lower risk checks (!372)
- Update `gl-dast-report.json` schema version to `13.1.0` (!378)
- Upgrade Browserker to version `0.0.17` including renaming `DisableHeadless` to `Headless` (!380)

## v1.39.0
- Improved the readability of `/analyze --help` by displaying the environment variable name next to each configuration option (!374)
- Update Browserker scan to only scan hosts included in `DAST_BROWSERKER_ALLOWED_HOSTS` (!366)

## v1.38.0
- Add `DAST_EXCLUDE_URLS` which is an alias of `DAST_AUTH_EXCLUDE_URLS` (!367)

## v1.37.0
- Update `analyze` script to error if the user has insufficient permissions (!360)
- Add `DAST_AUTH_VERIFICATION_URL` to verify that DAST authentication succeeded (!364)
- Abort the scan and exit if the target returns a 500 during the access check (!363)
- Add `DAST_SKIP_TARGET_CHECK` to allow for skipping the target check (!365)

## v1.36.0
- Fix issue in Dockerfile where user was asked for input during build (!358)
- Upgrade Python to version `3.9.1` (!358)
- Upgrade Browserker to version `0.0.16` (!356)
- Browserker runs as the `gitlab` user (!356)
- Add URL validation to `DAST_WEBSITE` (!354)

## v1.35.0
- Upgrade ZAP add-on `Active scanner rules (beta)` to [32.0.0](https://github.com/zaproxy/zap-extensions/releases/ascanrulesBeta-v32) (!352)
- Upgrade ZAP add-on `Passive scanner rules` to [30.0.0](https://github.com/zaproxy/zap-extensions/releases/pscanrules-v30) (!352)
- Upgrade ZAP add-on `Passive scanner rules (beta)` to [23.0.0](https://github.com/zaproxy/zap-extensions/releases/pscanrulesBeta-v23) (!352)
- Upgrade ZAP add-on `Ajax Spider` to [23.2.0](https://github.com/zaproxy/zap-extensions/releases/spiderAjax-v23.2.0) (!352)
- Upgrade ZAP add-on `Linux WebDrivers` to [23.0.0](https://github.com/zaproxy/zap-extensions/releases/webdriverlinux-v23) (!352)
- Upgrade ZAP add-on `MacOS WebDrivers` to [22.0.0](https://github.com/zaproxy/zap-extensions/releases/webdrivermacos-v22) (!352)
- Upgrade ZAP add-on `Windows WebDrivers` to [23.0.0](https://github.com/zaproxy/zap-extensions/releases/webdriverwindows-v23) (!352)
- Upgrade ZAP add-on `Zest - Graphical Security Scripting Language` to [33.0.0](https://github.com/zaproxy/zap-extensions/releases/zest-v33) (!352)

## v1.34.0
- Add `scan.scanner.vendor.name` to the JSON report output (!334)
- Make the zap and browserker user's uid configurable at build time (!350)
- Improve `identify-addon-updates.sh` to display the changes required in the Changelog, Dockerfile and MR description (!343)
- Update release script to prevent failure when unexpected characters are included in the Changelog (!345)
- Document the new process of updating the ZAP plugins (!353)

## v1.33.0
- Improve ability to find the login button when running authenticated scans (!342)

## v1.32.1
- Update the help text for `DAST_SUBMIT_FIELD`, `DAST_FIRST_SUBMIT_FIELD`, `DAST_USERNAME_FIELD`, and `DAST_PASSWORD_FIELD` (!338)

## v1.32.0
- Revert: `Remove the duplicate target check prior to a scan (!323)` (!336)

## v1.31.0
- Remove the duplicate target check prior to a scan (!323)
- Upgrade ZAP add-on `ascanrules` to [v36.0.0](https://github.com/zaproxy/zap-extensions/releases/ascanrules-v36) (!333)
- Upgrade ZAP add-on `ascanrulesBeta` to [v31.0.0](https://github.com/zaproxy/zap-extensions/releases/ascanrulesBeta-v31) (!333)
- Upgrade ZAP add-on `commonlib` to [v1.1.0](https://github.com/zaproxy/zap-extensions/releases/commonlib-v1.1.0) (!333)
- Upgrade ZAP add-on `fuzz` to [v13.0.1](https://github.com/zaproxy/zap-extensions/releases/fuzz-v13.0.1) (!333)
- Upgrade ZAP add-on `hud` to [v0.12.0](https://github.com/zaproxy/zap-hud/releases/v0.12.0) (!333)
- Upgrade ZAP add-on `retire` to [v0.5.0](https://github.com/zaproxy/zap-extensions/releases/retire-v0.5.0) (!333)
- Upgrade ZAP add-on `webdriverlinux` to [v21.0.0](https://github.com/zaproxy/zap-extensions/releases/webdriverlinux-v21) (!333)
- Upgrade ZAP add-on `webdrivermacos` to [v20.0.0](https://github.com/zaproxy/zap-extensions/releases/webdrivermacos-v20) (!333)
- Upgrade ZAP add-on `webdriverwindows` to [v21.0.0](https://github.com/zaproxy/zap-extensions/releases/webdriverwindows-v21) (!333)
- Upgrade ZAP add-on `websocket` to [v22.0.0](https://github.com/zaproxy/zap-extensions/releases/websocket-v22) (!333)
- Disable the DAST Unix Timestamp Vulnerability Check on all scans (!335)

## v1.30.0
- Log active scan rules that were run if `DAST_DEBUG=true` (!315)
- Add `DAST_SPIDER_START_AT_HOST` to allow scans to start at given target (!317)
- Run a passive or active scan with predetermined URLs using the environment variable `DAST_PATHS_FILE` (!319)
- Prevent DAST from accessing API specification files outside of the /zap/wrk directory (!322)
- Optimize DAST image to be smaller in size (!320)
- Upgrade Firefox to version 81 (!327)
- Upgrade Geckodriver to version 0.27.0 (!320)
- Firefox runs headless when authenticating user using Selenium WebDriver (!320)

## v1.29.0
- Improve error messaging when target website check fails (!314)
- DAST can be run as a user other than `zap` for RedHat OpenShift support (!316)

## v1.28.0
- Paths added to `DAST_PATHS` are excluded if they are contained in `DAST_AUTH_EXCLUDE_URLS` (!311)
- Remove ZAP config options logging statement as DAST no longer passes control to the ZAP scripts (!313)

## v1.27.0
- Reset target to host without trailing slash to prevent duplicate vulnerabilities (!304)
- Throw an error if both the AJAX Spider and an API scan are configured (!303)
- Use a consistent strategy for handling and communicating errors (!302)
- Print the version of DAST at the start of the log (!306)
- Remove unused JAR dependencies from the Docker image that flag as an issue in some vulnerability scanners (!307)
- Promote spider logging statements to INFO (!304)

## v1.26.0
- Run a passive scan with predetermined URLs using the environment variable `DAST_PATHS` (!284)
- Run an active scan with predetermined URLs using the environment variable `DAST_PATHS` (!293) (!300)

## v1.25.0
- Add deprecation notice to -D (!291)
- Output an error when required configuration parameters have not been set (!280)
- Passive Scan rules excluded with `DAST_EXCLUDE_RULES` are marked as skipped in the log summary (!290)
- Fix bug where headers that contain colons cause DAST to crash (!296)
- Set site availability timeout using `DAST_TARGET_AVAILABILITY_TIMEOUT` environment variable (!287)

## v1.24.0
- Fix bug where `DAST_DEBUG` is ignored (!285)
- Improve logging output during active scan (!273)
- Log warning when `ZAP_API_HOST_OVERRIDE` used when importing an API specification from a file (!274)
- Results printed at the end of a scan only include rules that were run (!268)
- DAST exits with a non-zero exit code when ZAP crashes (!265)
- Errors thrown during a scan cause the scan to abort (!264)
- Remove tagging of URLs because they take a long time to execute and don't appear to be used (!229)
- Show skipped rules in log output (!269)

## v1.23.0
- Set the directory location of custom ZAP scripts using `DAST_SCRIPT_DIRS` (!234)
- Add deprecation notice to `-p` configuration option (!246)
- Add deprecation notice to the `-n` configuration option (!252)
- Set how long for DAST to wait for a passive scan to execute with the environment variable `DAST_PASSIVE_SCAN_MAX_WAIT_TIME` (!252)
- Set how many attempts DAST should make to connect to the ZAP Server with the environment variable `DAST_ZAP_MAX_CONNECTION_ATTEMPTS` (!252)
- Set how many seconds DAST should sleep in between ZAP server connection attempts with the environment variable `DAST_ZAP_CONNECT_SLEEP_SECONDS` (!252)

## v1.22.1
- Use an explicit session name to ensure the correct ZAP database is used (!241)

## v1.22.0
- Remove duplicate request for ZAP alerts to reduce memory usage and time to build report (!239)
- Retrieve ZAP messages for alerts from the ZAP database to reduce the memory required to run DAST (!239)
- Retrieve ZAP messages for scanned resources from the ZAP database to reduce the memory required to run DAST (!240)

## v1.21.1
- Add deprecation notice to `-s` configuration option (!219)
- HSQLDB logs are logged at `WARN` to reduce noise in the log file (!222)

## v1.20.0
- Rules excluded using the `DAST_EXCLUDE_RULES` environment variable do not run (!216)

## v1.19.0
- Update DAST to use the ZAP Docker base image `owasp/zap2docker-weekly:w2020-06-30` (!208)
- Upgrade Python to version `3.8.2` (!208)
- Prevent ZAP from failing when running in other container engines than Docker (!197)
- Upgrade ZAP add-on `ascanrules` to [v35.0.0](https://github.com/zaproxy/zap-extensions/releases/ascanrules-v35) (!211)
- Upgrade ZAP add-on `ascanrulesBeta` to [v28.0.0](https://github.com/zaproxy/zap-extensions/releases/ascanrulesBeta-v28) (!211)
- Upgrade ZAP add-on `commonlib` to [v1.0.0](https://github.com/zaproxy/zap-extensions/releases/commonlib-v1.0.0) (!211)
- Upgrade ZAP add-on `fuzzdb` to [v7.0.0](https://github.com/zaproxy/zap-extensions/releases/fuzzdb-v7) (!211)
- Upgrade ZAP add-on `openapi` to [v16.0.0](https://github.com/zaproxy/zap-extensions/releases/openapi-v16) (!211)
- Upgrade ZAP add-on `pscanrules` to [v29.0.0](https://github.com/zaproxy/zap-extensions/releases/pscanrules-v29) (!211)
- Upgrade ZAP add-on `pscanrulesBeta` to [v22.0.0](https://github.com/zaproxy/zap-extensions/releases/pscanrulesBeta-v22) (!211)
- Upgrade ZAP add-on `webdriverlinux` to [v18.0.0](https://github.com/zaproxy/zap-extensions/releases/webdriverlinux-v18) (!211)
- Upgrade ZAP add-on `webdrivermacos` to [v17.0.0](https://github.com/zaproxy/zap-extensions/releases/webdrivermacos-v17) (!211)
- Upgrade ZAP add-on `webdriverwindows` to [v18.0.0](https://github.com/zaproxy/zap-extensions/releases/webdriverwindows-v18) (!211)

## v1.18.1
- Reduce memory usage by excluding HTTP request and response bodies from alerts (!201)

## v1.18.0
- Remove support for the ZAP config file because it doesn't work as intended (!185)
- Remove environment variables `DAST_ZAP_CONFIG_FILE`, `DAST_ZAP_CONFIG_URL`, `DAST_ZAP_GENERATE_CONFIG` (!185)
- Run DAST as the `zap` user, not as `root` (!186)
- Update Firefox to version `77.0` and Geckodriver to version `0.26.0` (!106)
- Update Python Selenium to version `3.141.0` (!191)

## v1.17.1
- Default spidering in a full scan to have an unlimited maximum duration (!184)

## v1.17.0
- Remove `INFO` log4j properties from base logging configuration for ZAP Server (!182)
- Include request and response headers in the vulnerability evidence (!168)
- Mask sensitive request and response header values with environment variable DAST_MASK_HTTP_HEADERS (!179)

## v1.16.0
- Set ZAP Server log4j log levels using `DAST_ZAP_LOG_CONFIGURATION` (!166)
- Remove GitLab Runner style `x-x-stable` tags from future DAST Docker images (!174)
- Remove legacy DAST entrypoints, please use `/analyze` instead (!172)

## v1.15.0
- Include context of the scan in the DAST JSON Report (!165)

## v1.14.0
- The DAST JSON report is created using information from the ZAP REST API, not the ZAP JSON report (!142)
- Set the maximum duration of the spider scan with environment variable `DAST_SPIDER_MINS` (!153)
- Include alpha passive and active scan rules with environment variable `DAST_INCLUDE_ALPHA_VULNERABILITIES` (!153)
- Set the ZAP config URL to configure vulnerability finding risk levels with environment variable `DAST_ZAP_CONFIG_URL` (!153)
- Set the name of the ZAP config file to configure vulnerability finding risk levels with environment variable `DAST_ZAP_CONFIG_FILE` (!163)
- Generate sample config file with environment variable `DAST_ZAP_GENERATE_CONFIG` (!163)
- Set the ZAP Server command-line options with environment variable  `DAST_ZAP_CLI_OPTIONS` (!163)
- Enable DAST debug messages with environment variable `DAST_DEBUG` (!163)
- Set the file name of the ZAP HTML report written at the end of a scan using `DAST_HTML_REPORT` (!159)
- Set the file name of the ZAP Markdown report written at the end of a scan using `DAST_MARKDOWN_REPORT` (!159)
- Set the file name of the ZAP XML report written at the end of a scan using `DAST_XML_REPORT` (!159)
- Copy contents of `/zap/wrk` to the working directory in order to make them available as CI job artifacts (!160)

## v1.13.3
- Enable Ajax spider using the environment variable `DAST_ZAP_USE_AJAX_SPIDER` (!147)

## v1.13.2
- ZAP does not make external HTTP requests when `auto-update-addons` is false (!146)
- Upgrade ZAP add-on `fuzzdb` to [v6](https://github.com/zaproxy/zap-extensions/releases/tag/fuzzdb-v6) (!144)
- Upgrade ZAP add-on `pscanrules-release` to [v28](https://github.com/zaproxy/zap-extensions/releases/tag/pscanrules-v28) (!144)
- Upgrade ZAP add-on `selenium-release` to [v15.2.0](https://github.com/zaproxy/zap-extensions/releases/tag/selenium-v15.2.0) (!144)

## v1.13.1
- Disable auto-updating ZAP addons by default (!143)

## v1.13.0
- Each Vulnerability is now identifiable by a unique id in the JSON report (!128)

## v1.12.1
- Rename `CommonReportFormatter` to `SecurityReportFormatter` (!138)

## v1.12.0
- `vulnerabilities[].evidence.summary` is added to the report to provide more context about the vulnerability (!132)

## v1.11.1
- `severity` and `confidence` report values are title case to match the DAST schema (!131)

## v1.11.0
- OpenAPI specifications can be referenced using the `DAST_API_SPECIFICATION` environment variable to trigger an API scan (!92)(!112)(!125)(!126)
- Hosts defined in API specifications can be overridden using the `DAST_API_HOST_OVERRIDE` environment variable (!112)(!123)
- Validation rules can be excluded from the scan using the `DAST_EXCLUDE_RULES` environment variable (!115)
- Request headers can be added to every request using the `DAST_REQUEST_HEADERS` environment variable (!124)

## v1.10.0
- Limit URLs displayed in console output to those captured during spider scan (!113)
- Output URLs spidered by DAST JSON report as `scan.scanned_resources` (!113)
- Logging is `INFO` level by default (!110)

## v1.9.0
- Include URLs scanned by DAST in the console output (!112)

## v1.8.0
- DAST depends on ZAP weekly release w2020-02-10 (!101)
- DAST Python scripts run using Python 3.6.9 in anticipation of Python 2 EOL (!102)
- Upgrade pinned add-on versions: alertFilters to [v10](https://github.com/zaproxy/zap-extensions/releases/tag/alertFilters-v10), ascanrulesBeta to [v27](https://github.com/zaproxy/zap-extensions/releases/tag/ascanrulesBeta-v27) and pscanrules to [v27](https://github.com/zaproxy/zap-extensions/releases/tag/pscanrules-v27) (!107)

## v1.7.0
- Include recent ZAP add-ons into the DAST Docker image so they don't have to be downloaded every scan (!105)
- Auto-update of ZAP add-ons can be disabled with the `--auto-update-addons false` command line argument (!105)

## v1.6.1
- Fix issue where AJAX spider scans could not start Firefox (!87)

## v1.6.0
- Add initial Secure Common Report Format fields to DAST report (!81)

## v1.5.4
- Validate URL command line arguments (!69)
- Exit code is zero when a scan runs successfully, non zero when a scan fails or arguments are invalid (!69)
- The DAST report is deterministic, keys in the JSON document are in alphabetical order (!70)
- Ajax scans start from the target URL (!71)
- Don't verify HTTPS certificates when determining if a target site is ready for scanning (!76)

## v1.5.3
- Fixed support for `--auth-exclude-urls` parameter (!52)

## v1.5.2
- DAST depends on ZAP weekly release w2019-09-24, re-enabling `ascanrulesBeta` rules
- Removed Python 3 to fix Full Scans

## v1.5.1
- DAST depends on ZAP release 2.8.0 (!50)

## v1.5.0
- Running `/analyze --help` shows all options and environment variables supported by DAST (!39)
- Expose ZAP logs while executing scans (!42)

## v1.4.0
- Implement [domain validation](https://docs.gitlab.com/ee/user/application_security/dast/index.html#domain-validation) option for full scans (!35)

## v1.3.0
- Report which URLs were scanned (!24)

## v1.2.7
- Fix passing of optional params to ZAP (!27)

## v1.2.6
- Fix max. curl timeout to be longer than 150 seconds (!26)

## v1.2.5
- Fix curl timeout (!25)

## v1.2.4
- Fix timeout when $DAST_TARGET_AVAILABILITY_TIMEOUT is used (!21)

## v1.2.3
- Fix auto login functionality. Auto login is used if the HTML elements for username, password, or submit button have not been specified.

## v1.2.2
- Fix a bug where `analyze` would fail if only `DAST_WEBSITE` was used. https://gitlab.com/gitlab-org/gitlab-ee/issues/11744

## v1.2.1
- Accept $DAST_WEBSITE env var instead of `-t` parameter (still supported for backward compatibility)

## v1.2.0
- Add [ZAP Full Scan](https://github.com/zaproxy/zaproxy/wiki/ZAP-Full-Scan) support (!14)

## v1.1.2
- Add workaround for supporting long CLI auth options (!9)

## v1.1.1
- Fix a problem with multiple login buttons on the login page.

## v1.1.0
- First release of the DAST GitLab image.
