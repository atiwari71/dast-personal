from sys import exit
from typing import Callable

from .container import Container
from .dast_error_handler import DASTErrorHandler
from .initialize import initialize_dast_logs


class DAST:

    def __init__(self, container_factory: Callable[[], Container]):
        self._container_factory = container_factory

    def start(self) -> None:
        try:
            container = self._container_factory()

            scan = container.scan
            config = container.config

            initialize_dast_logs(config)

            scan.run()
        except Exception as error:
            DASTErrorHandler().handle(error)
            exit(1)
