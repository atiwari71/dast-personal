import functools

from six.moves.urllib.parse import urlparse


@functools.total_ordering
class URL:

    def __init__(self, url_parts):
        self.url_parts = url_parts

    @property
    def scheme(self):
        return self.url_parts.scheme

    @property
    def host(self):
        return self.url_parts.hostname

    @property
    def port(self):
        explicit_port = self.url_parts.port

        if explicit_port:
            return explicit_port

        return 443 if self.is_secure else 80

    @property
    def is_secure(self):
        return self.scheme == 'https'

    @property
    def path_and_query_fragment(self):
        path = self.url_parts.path

        if self.url_parts.query:
            path += f'?{self.url_parts.query}'

        if self.url_parts.fragment:
            path += f'#{self.url_parts.fragment}'

        return path

    @property
    def scheme_and_host(self):
        name = f'{self.url_parts.scheme}://{self.url_parts.hostname}'

        if self.url_parts.port and (
                (self.url_parts.port != 80 and self.url_parts.port != 443) or
                (self.url_parts.port == 80 and self.url_parts.scheme != 'http') or
                (self.url_parts.port == 443 and self.url_parts.scheme != 'https')):
            name = f'{name}:{self.url_parts.port}'

        return name

    def __lt__(self, other):
        return str(self) < str(other)

    def __str__(self):
        return f'{self.scheme_and_host}{self.path_and_query_fragment}'

    @classmethod
    def parse(cls, url):
        url_parts = urlparse(url)

        if url_parts.scheme and url_parts.netloc:
            return URL(url_parts)

        raise RuntimeError(f"Failed to parse '{url}' as it is not a valid URL")
