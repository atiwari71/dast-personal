from functools import cached_property
from tempfile import mkstemp
from typing import Any, Dict, List, Optional

from .models import Limits


class Configuration:

    def __init__(self, settings: Any):
        self.advertise_scan = settings.advertise_scan
        self.aggregate_vulnerabilities: bool = settings.aggregate_vulnerabilities
        self.api_specification: Optional[str] = settings.api_specification
        self.auth_auto: Optional[bool] = settings.auth_auto
        self.auth_cookies: List[str] = settings.browserker_auth_cookies
        self.auth_first_submit_field: Optional[str] = settings.auth_first_submit_field
        self.auth_path_to_login_form: List[str] = settings.browserker_path_to_login_form
        self.auth_password: Optional[str] = settings.auth_password
        self.auth_password_field: Optional[str] = settings.auth_password_field
        self.auth_report: bool = settings.browserker_auth_report
        self.auth_submit_field: Optional[str] = settings.auth_submit_field
        self.auth_url: Optional[str] = settings.auth_url
        self.auth_username: Optional[str] = settings.auth_username
        self.auth_username_field: Optional[str] = settings.auth_username_field
        self.auth_verification_login_form: str = settings.browserker_auth_verification_login_form
        self.auth_verification_selector: str = settings.browserker_auth_verification_selector
        self.auth_verification_url: str = settings.auth_verification_url
        self.auto_update_addons: bool = settings.auto_update_addons
        self.availability_timeout: int = settings.availability_timeout
        self.browserker_allowed_hosts: List[str] = settings.browserker_all_allowed_hosts
        self.browserker_cookies: Dict[str, str] = settings.browserker_cookies
        self.browserker_crawl_graph: bool = settings.browserker_crawl_graph
        self.browserker_crawl_report: bool = settings.browserker_crawl_report
        self.browserker_excluded_elements: List[str] = settings.browserker_excluded_elements
        self.browserker_excluded_hosts: List[str] = settings.browserker_excluded_hosts
        self.browserker_hash_attributes: List[str] = settings.browserker_hash_attributes
        self.browserker_include_only_rules: List[str] = settings.browserker_include_only_rules
        self.browserker_ignored_hosts: List[str] = settings.browserker_ignored_hosts
        self.browserker_devtools_log: Dict[str, str] = settings.browserker_devtools_log
        self.browserker_log: Dict[str, str] = settings.browserker_log
        self.browserker_file_log_path: Optional[str] = settings.browserker_file_log_path
        self.browserker_file_log: Dict[str, str] = settings.browserker_file_log
        self.browserker_log_chromium_output: Dict[str, str] = settings.browserker_log_chromium_output
        self.browserker_log_request_error_report: bool = settings.browserker_log_request_error_report
        self.browserker_max_actions: int = settings.browserker_max_actions
        self.browserker_max_attack_failures: int = settings.browserker_max_attack_failures
        self.browserker_max_depth: int = settings.browserker_max_depth
        self.browserker_max_response_size_mb: int = settings.browserker_max_response_size_mb
        self.browserker_cache: bool = settings.browserker_cache
        self.browserker_chrome_debug_log_dir: str = settings.browserker_chrome_debug_log_dir
        self.browserker_number_of_browsers: int = settings.browserker_number_of_browsers
        self.browserker_scan: bool = settings.browserker_scan
        self.browserker_navigation_timeout: str = settings.browserker_navigation_timeout
        self.browserker_action_timeout: str = settings.browserker_action_timeout
        self.browserker_secure_report_extra_info: bool = settings.browserker_secure_report_extra_info
        self.browserker_stability_timeout: str = settings.browserker_stability_timeout
        self.browserker_navigation_stability_timeout: str = settings.browserker_navigation_stability_timeout
        self.browserker_action_stability_timeout: str = settings.browserker_action_stability_timeout
        self.browserker_search_element_timeout: str = settings.browserker_search_element_timeout
        self.browserker_extract_element_timeout: str = settings.browserker_extract_element_timeout
        self.browserker_element_timeout: str = settings.browserker_element_timeout
        self.browserker_always_relogin: bool = settings.browserker_always_relogin
        self.browserker_page_ready_selector: str = settings.browserker_page_ready_selector
        self.exclude_rules: List[str] = settings.exclude_rules
        self.exclude_urls: List[str] = settings.exclude_urls
        self.ff_browser_passive_scan_mode: bool = settings.ff_browser_passive_scan_mode
        self.full_scan: Optional[bool] = settings.full_scan
        self.http_headers_to_mask: Optional[List[str]] = settings.http_headers_to_mask
        self.is_api_scan: bool = settings.is_api_scan
        self.max_findings: int = 100000
        self.max_urls_per_vulnerability: int = settings.max_urls_per_vulnerability
        self.only_include_rules: List[str] = settings.only_include_rules
        self.passive_scan_max_wait_time: int = settings.passive_scan_max_wait_time
        self.paths_to_scan_file_path: str = settings.paths_to_scan_file_path
        self.paths_to_scan_list: List[str] = settings.paths_to_scan_list
        self.pkcs12_cert: Optional[bytes] = settings.pkcs12_cert
        self.pkcs12_password: str = settings.pkcs12_password
        self.request_headers: Dict[str, str] = settings.request_headers
        self.script_dirs: List[str] = settings.script_dirs
        self.silent: bool = settings.silent
        self.skip_target_check: bool = settings.skip_target_check
        self.spider_mins: int = settings.spider_mins
        self.spider_start_at_host: bool = settings.spider_start_at_host
        self.target: str = settings.target
        self.urls_to_scan: List[str] = settings.urls_to_scan
        self.write_addons_to_update_file: Optional[bool] = settings.write_addons_to_update_file
        self.zap_api_host_override: Optional[str] = settings.zap_api_host_override
        self.zap_connect_sleep_seconds: Optional[int] = settings.zap_connect_sleep_seconds
        self.zap_debug: Optional[bool] = settings.zap_debug
        self.zap_default_info: Optional[str] = settings.zap_default_info
        self.zap_include_alpha: Optional[bool] = settings.zap_include_alpha
        self.zap_log_configuration: Optional[str] = settings.zap_log_configuration
        self.zap_max_connection_attempts: int = settings.zap_max_connection_attempts
        self.zap_min_level: Optional[str] = settings.zap_min_level
        self.zap_no_fail_on_warn: Optional[str] = settings.zap_no_fail_on_warn
        self.zap_other_options: Optional[str] = settings.zap_other_options
        self.zap_port: int = settings.zap_port
        self.zap_report_html: Optional[str] = settings.zap_report_html
        self.zap_report_md: Optional[str] = settings.zap_report_md
        self.zap_report_xml: Optional[str] = settings.zap_report_xml
        self.zap_use_ajax_spider: Optional[bool] = settings.zap_use_ajax_spider

        self.security_report_character_limits = Limits(summary=20000)

        self.dast_major_version: int = settings.dast_major_version
        if settings.dast_major_version < 2:
            self.auth_display: Optional[bool] = settings.auth_display
            self.full_scan_domain_validation_required: Optional[bool] = settings.full_scan_domain_validation_required
            self.zap_context_file: Optional[str] = settings.zap_context_file
            self.zap_delay_in_seconds: Optional[str] = settings.zap_delay_in_seconds
            self.zap_progress_file: Optional[str] = settings.zap_progress_file
            self.zap_short_format: Optional[bool] = settings.zap_short_format

    @cached_property
    def pkcs12_cert_filename(self) -> str:
        if not self.pkcs12_cert:
            return ''

        file_descriptor, cert_file_path = mkstemp()

        with open(file_descriptor, 'wb') as f:
            f.write(self.pkcs12_cert)

        return cert_file_path
