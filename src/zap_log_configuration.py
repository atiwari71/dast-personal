class ZAPLogConfiguration:

    PATH = '/app/zap/log4j2.properties'

    HEADER = [
        '# Properties below added programmtically by DAST',
    ]

    BASE_PROPERTIES = [
        'name = ZAP Home Config',

        'rootLogger.level = info',
        'rootLogger.appenderRef.stdout.ref = stdout',
        'rootLogger.appenderRef.rolling.ref = RollingFile',

        'appender.console.type = Console',
        'appender.console.name = stdout',
        'appender.console.layout.type = PatternLayout',
        'appender.console.layout.pattern = %r [%t] %-5level %logger{36} - %msg%n',

        'property.filename = ${sys:zap.user.log}/zap.log',

        'appender.rolling.type = RollingFile',
        'appender.rolling.name = RollingFile',
        'appender.rolling.fileName = ${filename}',
        'appender.rolling.filePattern = ${filename}.%i',
        'appender.rolling.immediateFlush = true',
        'appender.rolling.layout.type = PatternLayout',
        'appender.rolling.layout.pattern = %d [%-5t] %-5p %c{1} - %m%n',
        'appender.rolling.policies.type = Policies',
        'appender.rolling.policies.size.type = SizeBasedTriggeringPolicy',
        'appender.rolling.policies.size.size=1024MB',
        'appender.rolling.strategy.type = DefaultRolloverStrategy',
        'appender.rolling.strategy.max = 3',

        'logger.commonshttpclient.name = org.apache.commons.httpclient',
        'logger.commonshttpclient.level = error',

        # Disable Jericho log, it logs HTML parsing issues as errors.
        'logger.jericho.name = net.htmlparser.jericho',
        'logger.jericho.level = off',

        # Prevent Crawljax from logging too many, not so useful, INFO messages.'
        # For example:',
        # INFO  Crawler - New DOM is a new state! crawl depth is now 10'
        # INFO  Crawler - Crawl depth is now 1'
        # INFO  Crawler - Crawl depth is now 2'
        'logger.crawljaxCrawler.name = com.crawljax.core.Crawler',
        'logger.crawljaxCrawler.level = warn',
        # INFO  UnfiredCandidateActions - There are 64 states with unfired actions
        'logger.crawljaxStateMachine.name = com.crawljax.core.state.StateMachine',
        'logger.crawljaxStateMachine.level = warn',
        # INFO  StateMachine - State state106 added to the StateMachine.
        'logger.crawljaxUnfired.name = com.crawljax.core.UnfiredCandidateActions',
        'logger.crawljaxUnfired.level = warn',

        'logger.hsqldb.name = hsqldb.db.HSQLDB379AF3DEBD.ENGINE',
        'logger.hsqldb.level = warn',
    ]

    def __init__(self, config):
        self.config = config

    def get_path(self):
        return self.PATH

    def write_log_properties(self, io):
        contents = '\n'.join(self.HEADER + self.BASE_PROPERTIES + self._get_extra_properties())
        io.write(contents)

    def _get_extra_properties(self):
        properties = str(self.config.zap_log_configuration or '').split(';')
        return list(filter(None, properties))
