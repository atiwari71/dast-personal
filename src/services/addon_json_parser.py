import json
from typing import List

from markdownify import markdownify


class AddonJSONParser:

    CHANGELOG_LOG_TEMPLATE = '- Upgrade ZAP add-on `{name}` to [{version}]({url}) (!__MR_ID__)'
    MR_DESCRIPTION_TEMPLATE = '### Upgraded ZAP add-on `{name}` to [{version}]({url}) \n\n{release_notes}'
    DOCKERFILE_LINE = 'rm -rf {id}-* && wget {url} && \\'

    def __init__(self, file_name: str):
        self._file_name = file_name
        self._urls_to_update: List[str] = []
        self._changelog_lines: List[str] = []
        self._mr_lines: List[str] = []

    def parse(self) -> str:
        with open(self._file_name) as f:
            data = json.load(f)

        for addon in data:
            self._urls_to_update.append(self.DOCKERFILE_LINE.format(id=addon['id'], url=addon['url']))
            self._changelog_lines.append(self._changelog_line(addon['url'], addon['name'], addon['version']))
            self._mr_lines.append(self._mr_line(addon['url'], addon['name'], addon['version'], addon['changes']))

        return self._output()

    def _mr_line(self, url: str, name: str, version: str, changes: str) -> str:
        md_release_notes = markdownify(changes).replace('###', '####')
        return self.MR_DESCRIPTION_TEMPLATE.format(
            name=name, version=version, url=self._info_url(url), release_notes=md_release_notes)

    def _changelog_line(self, url: str, name: str, version: str) -> str:
        info_url = self._info_url(url)
        changelog_line = self.CHANGELOG_LOG_TEMPLATE.format(name=name, version=version, url=info_url)
        return changelog_line.replace('__URL__', info_url)

    def _output(self) -> str:
        output = '\nAdd or replace these addons in the Docker file:'+'\n'.join(self._urls_to_update)
        output += '\nCHANGE LOG ENTRY:'+'\n'.join(self._changelog_lines)
        output += '\nMR Description:'+'\n'.join(self._mr_lines)

        return output

    def _info_url(self, full_url: str) -> str:
        info_url_parts = full_url.split('/')
        info_url_parts.pop()
        return '/'.join(info_url_parts).replace('download/', '')
