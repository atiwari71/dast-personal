from .id import ID


class CweID(ID):

    INVALID_INT_MESSAGE = 'ID must be a non-zero integer'

    def valid(self) -> bool:
        return super().valid() and not self._id == '0'
