from src.http_headers import HttpHeaders


class HttpResponse:

    def __init__(self, status: int, reason_phrase: str, headers: HttpHeaders):
        self._status = status
        self._reason_phrase = reason_phrase
        self._headers = headers

    @property
    def status(self):
        return self._status

    @property
    def reason_phrase(self):
        return self._reason_phrase

    @property
    def headers(self):
        return self._headers
