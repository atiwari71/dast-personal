from typing import Any, Dict, List

from src.models import Alert, Alerts
from src.models.http import HttpMessages


class AlertsParser:

    def parse(self, alerts: List[Dict[str, Any]], messages: HttpMessages) -> Alerts:
        return Alerts([self._parse_alert(alert, messages) for alert in alerts])

    def _parse_alert(self, alert: Dict[str, Any], messages: HttpMessages) -> Alert:
        alert['message'] = messages.message_with_id(alert.get('messageId', ''))
        return Alert(alert)
