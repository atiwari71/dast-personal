import logging
import time
import types
from typing import Any, Dict, List, Optional

from six import binary_type
from zapv2 import ZAPv2 as ZAP

from src.configuration import Configuration
from src.models import (
    Context, ContextID, Rules, Script, ScriptLanguage, Target,
)
from src.models.errors import SpiderProgressInvalidError
from src.models.http import HttpMessages
from .alerts_parser import AlertsParser
from .settings import Settings
from .spider_id import SpiderID
from .zap_api import RulesParser
from .zap_database import HttpMessagesQuery, ZAPDatabase
from .zap_message_types import ZAP_MESSAGE_TYPES


class ScanNotStartedException(Exception):
    pass


class ZAProxy:

    def __init__(self, exclude_urls: List[str], alerts_parser: AlertsParser, config: Configuration):
        self.context_id = None
        self.exclude_urls = exclude_urls or ['(logout|signout)']
        self._zap = None
        self._zap_database = ZAPDatabase()
        self._alerts_parser = alerts_parser
        self._config = config

    def updated_addons(self):
        return self._zap.autoupdate.updated_addons

    def new_session(self, zap) -> None:
        # Hooks are used to integrate with the ZAProxy tool, so we don't have direct access to the underlying
        # zap (zapv2) variable. This is our first opportunity to use it, so we save it as an instance variable.
        self._zap = zap
        self._zap.core.new_session(Settings.SESSION_NAME)

    def new_context(self, target: Target) -> None:
        # Unfortunately the zap_common.py code in the ZAProxy Docker image doesn't give us control over how
        # key APIs are called. We monkey patch them to ensure we can pass the options we want through.
        self._monkey_patch_ajax_spider_scan(target)

        self.context_id = self.create_context(Settings.CONTEXT_NAME)

        # include / exclude urls
        self.include_in_context(f'{target.regex}.*')

        if self._config.dast_major_version >= 2:
            self._zap.ajaxSpider.set_option_browser_id('chrome-headless')
            self._zap.selenium.set_option_chrome_driver_path('/usr/bin/chromedriver')

        for url_regex in self.exclude_urls:
            self.exclude_from_context(url_regex)

        if self.context_details(Settings.DEFAULT_CONTEXT_NAME):
            self.disable_context(Settings.DEFAULT_CONTEXT_NAME)

        self._zap.pscan.set_scan_only_in_scope(True)
        self._zap.ajaxSpider.add_allowed_resource(target.external_js())
        self._zap.ajaxSpider.add_allowed_resource(target.external_css())

    def context_details(self, context_name) -> Optional[Context]:
        details = self._zap.context.context(context_name)

        if details == 'Does Not Exist':
            return None

        return details

    def create_context(self, context_name) -> ContextID:
        logging.debug('Context - create ' + context_name)
        context_id: ContextID = self._zap.context.new_context(Settings.CONTEXT_NAME)

        self.enable_context(Settings.CONTEXT_NAME)
        return context_id

    def include_in_context(self, url_regex):
        logging.debug('Context - include ' + url_regex)
        self._zap.context.include_in_context(Settings.CONTEXT_NAME, url_regex)

    def exclude_from_context(self, url_regex):
        logging.debug('Context - exclude ' + url_regex)
        self._zap.context.exclude_from_context(Settings.CONTEXT_NAME, url_regex)

    def enable_context(self, context_name):
        logging.debug('Context - enable ' + context_name)
        self._zap.context.set_context_in_scope(context_name, True)

    def disable_context(self, context_name):
        logging.debug('Context - disable ' + context_name)
        self._zap.context.set_context_in_scope(context_name, False)

    def exclude_rules(self, rules: List[str], policy_name: str) -> None:
        exclude_rules = ','.join(rules)

        self._zap.pscan.disable_scanners(exclude_rules)
        self._zap.ascan.disable_scanners(exclude_rules, policy_name)

    def only_include_rules(self, rules: List[str], policy_name: str) -> None:
        self._zap.pscan.disable_all_scanners()
        self._zap.ascan.disable_all_scanners(policy_name)

        include_rules = ','.join(rules)

        self._zap.pscan.enable_scanners(include_rules)
        self._zap.ascan.enable_scanners(include_rules, policy_name)

    def alerts(self, target: Target):
        alerts = self._zap.alert.alerts()

        message_ids = [alert.get('messageId') for alert in alerts if 'messageId' in alert]
        return self._alerts_parser.parse(alerts, self.messages_with_ids(message_ids, target))

    def scanned_resources(self, target: Target) -> HttpMessages:
        return HttpMessagesQuery(self._zap_database, target).select('histtype', ZAP_MESSAGE_TYPES)

    def messages_with_ids(self, message_ids: List[str], target: Target) -> HttpMessages:
        if not message_ids:
            return HttpMessages()

        return HttpMessagesQuery(self._zap_database, target).select('historyid', message_ids)

    def zap_version(self):
        return self._zap.core.version

    def set_global_exclude_urls(self, regexp: str):
        return self._zap.core.exclude_from_proxy(regexp)

    def create_zap_httpsession(self, cookies: List[Dict[str, str]], target: Target) -> None:
        # Adding a session token is required to use set_session_token_value but the name does not seem to matter.
        self._zap.httpsessions.add_session_token(target, 'dast_session')

        # Create a new session using the acquired cookies from the authentication
        self._zap.httpsessions.create_empty_session(target, 'auth-session')

        # add all found cookies as session cookies
        for cookie in cookies:
            self._zap.httpsessions.set_session_token_value(target, 'auth-session', cookie['name'], cookie['value'])
            logging.debug('Cookie found: ' + cookie['name'] + ' - Value: ' + cookie['value'])

        # Mark the session as active
        self._zap.httpsessions.set_active_session(target, 'auth-session')
        logging.debug('Active session: ' + self._zap.httpsessions.active_session(target))

    def urls(self) -> List[str]:
        return self._zap.core.urls()

    def import_api_spec_from_file(self, filepath: str) -> List[str]:
        return self._zap.openapi.import_file(filepath)

    def import_api_spec_from_url(self, url: str, host_override: Optional[str]) -> List[str]:
        return self._zap.openapi.import_url(url, host_override)

    def spider_scans(self):
        return self._zap.spider.scans

    def scan_results(self, scan_id):
        return self._zap.spider.full_results(scan_id)

    def executed_rules(self, scan_policy_name: str) -> Rules:
        rules = self._zap.pscan.scanners

        if self._config.full_scan:
            rules.extend(self._zap.ascan.scanners(scan_policy_name))

        return RulesParser().parse(rules)

    def remaining_records_to_passive_scan(self) -> int:
        return int(self._zap.pscan.records_to_scan)

    def write_html_report(self, filename):
        self._write_report(self._zap.core.htmlreport(), filename)

    def write_xml_report(self, filename):
        self._write_report(self._zap.core.xmlreport(), filename)

    def write_md_report(self, filename):
        self._write_report(self._zap.core.mdreport(), filename)

    def run_url_scan(self, url_file_path) -> None:
        self._zap.importurls.importurls(url_file_path)

    def run_spider(self, target: Target) -> SpiderID:
        logging.debug(f'Spider context: {Settings.CONTEXT_NAME}')

        return SpiderID(self._zap.spider.scan(target, contextname=Settings.CONTEXT_NAME))

    def spider_progress_percentage(self, spider_id: SpiderID) -> int:
        spider_progress_percentage = self._zap.spider.status(spider_id)

        try:
            return int(spider_progress_percentage)
        except ValueError:
            raise SpiderProgressInvalidError(
                'Failed to spider progress. Identified spider progress percentage is invalid: '
                f'{spider_progress_percentage}',
            )

    def set_ajax_spider_maximum_run_time(self, max_mins_to_run: str) -> None:
        self._zap.ajaxSpider.set_option_max_duration(max_mins_to_run)

    def run_ajax_spider(self, target: Target) -> None:
        self._zap.ajaxSpider.scan(target)

    def ajax_spider_status(self) -> str:
        return self._zap.ajaxSpider.status

    def ajax_spider_results_count(self) -> str:
        return self._zap.ajaxSpider.number_of_results

    def run_active_scan(
        self, target: Target, policy_name: str, context_id: Optional[ContextID] = None,
    ) -> int:
        scan_id = self._zap.ascan.scan(
            target, recurse=True, scanpolicyname=policy_name, contextid=context_id,
        )

        try:
            return int(scan_id)
        except ValueError:
            raise ScanNotStartedException(
                f'Failed to start active scan. Identified scan progress value is invalid: {scan_id}',
            )

    def active_scan_percent_complete(self, scan_id: int) -> int:
        return int(self._zap.ascan.status(scan_id))

    def active_scan_progress(self, scan_id: int) -> List[Any]:
        return self._zap.ascan.scan_progress(scan_id)

    def load_script(self, script: Script) -> None:
        if not script.exists():
            raise RuntimeError(f'Failed to load script as file {script.file_path} cannot be found.')

        language = ScriptLanguage.detect_language(script.file_path)

        if not language:
            raise RuntimeError(f'Script language not supported for script {script.file_path}.')

        self._zap.script.load(script.name,
                              script.script_type.zap_name(),
                              language.zap_name(),
                              script.file_path)
        self._zap.script.enable(script.name)
        logging.info(f'Script loaded: {script.file_path}')

    def connect(self, proxy_endpoint: str) -> ZAP:
        max_attempts = self._config.zap_max_connection_attempts
        sleep_time = self._config.zap_connect_sleep_seconds
        zap = ZAP(proxies={'http': proxy_endpoint, 'https': proxy_endpoint})

        for _ in range(0, max_attempts):
            try:
                logging.info(f'looking for ZAP at {proxy_endpoint}...')
                version = zap.core.version
                logging.info(f'connected to ZAP with version {version}')
                return zap
            except IOError:
                time.sleep(sleep_time)

        raise RuntimeError(f'Failed to connect to ZAP after {max_attempts} attempts')

    def _write_report(self, report, filename):
        with open(f'{Settings.WRK_DIR}{filename}', mode='wb') as file:
            if not isinstance(report, binary_type):
                report = report.encode('utf-8')

            file.write(report)

    def _monkey_patch_ajax_spider_scan(self, target: Target) -> None:
        if hasattr(self._zap.ajaxSpider, 'original_scan'):
            return None

        def context_aware_scan(cls, _target: str):
            logging.debug('Context - ajax scan ' + Settings.CONTEXT_NAME)
            return cls.original_scan(url=target, contextname=Settings.CONTEXT_NAME)

        self._zap.ajaxSpider.original_scan = self._zap.ajaxSpider.scan
        self._zap.ajaxSpider.scan = types.MethodType(context_aware_scan, self._zap.ajaxSpider)
