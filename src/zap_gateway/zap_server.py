import logging
from itertools import chain

from src.configuration import Configuration
from src.system import System
from src.zap_gateway.server_configuration import RequestHeadersConfigurationBuilder, UserConfigurationBuilder
from .zap_server_daemon import ZAPServerDaemon


class ZAPServer:
    LOG_FILE = 'zap.out'

    def __init__(self,
                 config: Configuration,
                 system: System,
                 request_headers_configuration_builder: RequestHeadersConfigurationBuilder,
                 user_configuration_builder: UserConfigurationBuilder):
        self._config = config
        self.system = system
        self._request_headers_configuration_builder = request_headers_configuration_builder
        self._user_configuration_builder = user_configuration_builder

    def start(self) -> ZAPServerDaemon:
        logging.info('Starting the ZAP Server')

        settings = [['/zap/zap.sh'],
                    ['-daemon'],
                    ['-config', 'proxy.reverseProxy.use=1'],
                    ['-config', 'proxy.reverseProxy.ip=0.0.0.0'],
                    ['-config', f'proxy.reverseProxy.httpPort={self._config.zap_port}'],
                    ['-dir', '/app/zap'],
                    ['-config', 'api.disablekey=true'],
                    ['-config', 'api.addrs.addr.name=.*'],
                    ['-config', 'api.addrs.addr.regex=true'],
                    ['-config', 'selenium.firefoxDriver=/usr/bin/geckodriver'],
                    ['-config', f'spider.maxDuration={self._spider_mins()}'],
                    ['-addoninstall', 'pscanrulesAlpha'] if self._config.zap_include_alpha else [],
                    ['-addoninstall', 'ascanrulesAlpha'] if self._config.zap_include_alpha else [],
                    ['-addonupdate'] if self._config.auto_update_addons else [],
                    ['-silent'] if self._config.silent else [],
                    ['-config', 'certificate.use=true',
                     '-config', f'certificate.pkcs12.path={self._config.pkcs12_cert_filename}',
                     '-config', f'certificate.pkcs12.password={self._config.pkcs12_password}',
                     ] if self._config.pkcs12_cert_filename else [],
                    self._request_headers_configuration_builder.build(),
                    self._user_configuration_builder.build()]

        parameters = list(chain.from_iterable(settings))
        logging.info(f'Running ZAP with parameters {parameters}')
        daemon_process = self.system.run(parameters=parameters, output_file_name=ZAPServer.LOG_FILE)
        return ZAPServerDaemon(self._config.zap_port, daemon_process)

    def _spider_mins(self) -> int:
        if self._config.spider_mins:
            return self._config.spider_mins

        return 0 if self._config.full_scan else 1
