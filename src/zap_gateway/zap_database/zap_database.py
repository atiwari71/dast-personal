import logging
from os import path
from typing import Any, List, Optional, Tuple

from jaydebeapi import Connection, connect
from jpype import getDefaultJVMPath, isJVMStarted, startJVM

from .new_cursor import new_cursor
from ..settings import Settings


class ZAPDatabase:
    HSQLDB_JAR = r'/zap/lib/hsqldb-2.5.2.jar'
    USERNAME = 'SA'
    PASSWORD = ''

    def __init__(self,
                 database_dir: str = '/app/zap/session/',
                 hsqldb_jar: str = HSQLDB_JAR):
        self._database_dir: str = database_dir
        self._connection: Optional[Connection] = None
        self._hsqldb_jar = hsqldb_jar

    def execute(self, sql_query) -> None:
        logging.debug(f'ZAP database statement: {sql_query}')

        with new_cursor(self._connect()) as cursor:
            cursor.execute(sql_query)

    def select_many(self, sql_query: str) -> List[Tuple[str, ...]]:
        logging.debug(f'ZAP database query: {sql_query}')

        with new_cursor(self._connect()) as cursor:
            cursor.execute(sql_query)
            return self._normalize_results(cursor.fetchall())

    def _connect(self) -> Connection:
        if self._connection:
            return self._connection

        logging.debug('Checking JVM started')
        if not isJVMStarted():
            logging.debug('Getting JVM path')
            jvm_path = getDefaultJVMPath()

            logging.debug('Starting JVM')
            startJVM(jvm_path, f'-Djava.class.path={self._hsqldb_jar}')

        logging.debug('JVM has started')

        options = {'user': self.USERNAME,
                   'password': self.PASSWORD,
                   'hsqldb.lock_file': 'false'}

        database = self._database()
        logging.info(f'connecting to ZAP database {database}')
        self._connection = connect('org.hsqldb.jdbcDriver', f'jdbc:hsqldb:file:{database}', options)
        return self._connection

    def close(self) -> None:
        if self._connection:
            logging.debug('closing ZAP database')
            self._connection.close()

    def _database(self) -> str:
        database = path.join(self._database_dir, f'{Settings.SESSION_NAME}.session')
        data_file = f'{database}.data'

        if not path.exists(data_file):
            raise RuntimeError(f'Failed to find ZAP database {data_file}')

        return database

    def _normalize_results(self, results: List[Tuple[Any, ...]]) -> List[Tuple[str, ...]]:
        normalized = []

        for result in results:
            normalized.append(tuple(map(lambda value: value.__str__(), result)))

        return normalized
