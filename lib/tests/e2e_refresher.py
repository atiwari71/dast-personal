import glob
import os
import shutil


class EndToEndRefresher:

    def __init__(self, image, future_image) -> None:
        self.image = image
        self.future_image = future_image
        self.end_to_end_path = os.path.realpath('./test/end-to-end')

    def execute(self, skip_build):
        self._empty_output_directory()

        if not skip_build:
            # build dast and dast future
            os.system('invoke dast.build dast.build-future')

        self._run_all_tests('future')
        self._run_all_tests('current')

        self._normalizes_files()
        self._copy_output_to_expect()
        print('Script Complete!')

    def _empty_output_directory(self):
        output_glob = f'{self.end_to_end_path}/output/*'

        files = glob.glob(output_glob)
        for f in files:
            os.remove(f)

    def _run_all_tests(self, test_type):
        e2e_tests = ' '.join(self._current_end_to_end_tests())
        build_image = self.image

        if test_type == 'future':
            e2e_tests = ' '.join(self._future_end_to_end_tests())
            build_image = self.future_image

        os.system(f'BUILT_IMAGE={build_image} bash_unit {e2e_tests}')

    def _current_end_to_end_tests(self):
        return [os.path.join(self.end_to_end_path, file)
                for file in os.listdir(self.end_to_end_path) if file.endswith('.sh') and 'future' not in file]

    def _future_end_to_end_tests(self):
        return [os.path.join(self.end_to_end_path, file)
                for file in os.listdir(self.end_to_end_path) if file.endswith('.sh') and 'future' in file]

    def _copy_output_to_expect(self):
        output_glob = f'{self.end_to_end_path}/output/*.json'
        files = glob.glob(output_glob)
        for f in files:
            basename = os.path.basename(f)
            new_filename = basename.replace('report_', '')
            shutil.copy(f, f'{self.end_to_end_path}/expect/{new_filename}')

    def _normalizes_files(self):
        output_glob = f'{self.end_to_end_path}/output/*.json'
        files = glob.glob(output_glob)
        for f in files:
            try:
                os.system(f'{self.end_to_end_path}/normalize_dast_report.py {f} > {f}.tmp')
                os.system(f'mv {f}.tmp {f}')
            except:  # noqa: E722
                pass
