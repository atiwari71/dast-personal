import os
import sys
import time
from io import StringIO


class ZAPProject:
    ENV_PROJECT_PATH = 'ZAP_PROJECT_PATH'
    DEFAULT_PROJECT_PATH = f'{os.getenv("HOME", "/workspace")}/dev/zap'
    ZAP_REPOSITORY = 'https://github.com/zaproxy/zaproxy.git'
    ZAP_EXTENSIONS_REPOSITORY = 'https://github.com/zaproxy/zap-extensions.git'

    def __init__(self, context, run):
        self._context = context
        self._run = run
        self._install_dir = os.getenv(self.ENV_PROJECT_PATH, self.DEFAULT_PROJECT_PATH)
        self._zap_dir = os.path.join(self._install_dir, 'zaproxy')
        self._zap_extensions_dir = os.path.join(self._install_dir, 'zap-extensions')

    def install(self) -> None:
        if os.path.exists(self._install_dir):
            print(f'The directory {self._install_dir} already exists, assuming ZAP has already been installed.')
            return

        if not self.java_requirement_is_met():
            sys.exit(1)

        print('Starting the install of ZAP. You might like to get a cup of tea while you wait :^)')
        print('ETA 15 minutes. Pausing to give you a chance to quit...')
        time.sleep(10)

        self._run(f'mkdir -p {self._install_dir}')
        self.install_zap()
        self.install_zap_extensions()

        print()
        print(f'Successfully installed ZAP into {self._install_dir}.')

    def install_zap(self) -> None:
        self.git_clone(self.ZAP_REPOSITORY, self._zap_dir)
        self.configure_gradle(self._zap_dir)
        self.run_gradle_task(self._zap_dir, 'assemble')

    def install_zap_extensions(self) -> None:
        self.git_clone(self.ZAP_EXTENSIONS_REPOSITORY, self._zap_extensions_dir)
        self.configure_gradle(self._zap_extensions_dir)
        self.run_gradle_task(self._zap_extensions_dir, 'copyZapAddOn')

    def update(self) -> None:
        self.git_pull(self._zap_dir)
        self.git_pull(self._zap_extensions_dir)

    def start(self) -> None:
        self.run_gradle_task(self._zap_dir, 'run')

    def fetch_local_addon(self, addon: str) -> None:
        this_dir = os.path.dirname(os.path.realpath(__file__))
        addon_dir = os.path.join(self._zap_extensions_dir, 'addOns', addon)
        libs_dir = os.path.join(addon_dir, 'build', 'zapAddOn', 'bin', '*.zap')
        addons_dir = os.path.realpath(os.path.join(this_dir, '..', '..', 'resources', 'zap_plugins'))

        self.run_gradle_task(addon_dir, 'copyZapAddOn', gradle_location='../..')

        self._run(f'mkdir -p {addons_dir}')
        self._run(f'cp {libs_dir} {addons_dir}')

    def java_requirement_is_met(self) -> bool:
        error_stream = StringIO()
        self._run('java -version', err_stream=error_stream, warn=True)
        error = error_stream.getvalue()

        if '11.' not in error:
            print(error)
            print('Java 11 expected but not found. '
                  'Please install https://sdkman.io and run "sdk install java 11.0.4.hs-adpt"')
            return False

        return True

    def configure_gradle(self, work_dir: str) -> None:
        with open(os.path.join(work_dir, 'gradle.properties'), 'w') as properties:
            properties.write('org.gradle.caching=true\n')
            properties.write('org.gradle.parallel=true\n')
            properties.write('org.gradle.jvmargs=-Xmx4096m\n')

    def run_gradle_task(self, work_dir: str, task: str, gradle_location='.') -> None:
        self._run(f'cd {work_dir} && {gradle_location}/gradlew --max-workers 5 {task}')
        self._run(f'cd {work_dir} && {gradle_location}/gradlew --stop')

    def git_clone(self, from_repo: str, work_dir: str) -> None:
        self._run(f'git clone {from_repo} {work_dir}')

    def git_pull(self, work_dir: str) -> None:
        self._run(f'cd {work_dir} && git pull --ff-only')
