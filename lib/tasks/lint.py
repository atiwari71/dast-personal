import os

from invoke import Collection, run, task

from lib.typing import CoverageReport


@task
def python(context):
    """Lint Python code."""
    run('find . -name "*.py" -print0 | xargs -0 flake8 --show-source --config=./.flake8')
    print('Python lint found no errors.')


@task
def typing_progress(context):
    """Lint Python code for static type usage."""
    run('mypy src --txt-report .', warn=True)
    os.rename('index.txt', 'artifacts/mypy.txt')

    print('Moved mypy report index.txt to artifacts/mypy.txt')

    CoverageReport().generate_metrics()


@task
def markdown(context):
    """Lint Markdown documents."""
    dast_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', '..')

    run('docker run --rm -ti '
        f'-v {dast_dir}:/dast '
        '-w /dast '
        'registry.gitlab.com/gitlab-org/gitlab-docs:lint markdownlint --config .markdownlint.json **/*.md', pty=True)

    print('Markdown lint found no errors.')


lint = Collection(python, typing_progress, markdown)
