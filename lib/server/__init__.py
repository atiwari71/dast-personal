from .ajax_spider_server import AjaxSpiderServer
from .basic_site_server import BasicSiteServer
from .django_poll_server import DjangoPollServer
from .dvwa_server import DVWAServer
from .multi_page_server import MultiPageServer
from .mutual_tls_server import MutualTLSServer
from .pancakes_server import PancakesServer
from .rest_api_server import RestApiServer
from .webgoat_server import WebGoatServer

server_classes = [AjaxSpiderServer, BasicSiteServer, DjangoPollServer, DVWAServer, MultiPageServer,
                  MutualTLSServer, PancakesServer, RestApiServer, WebGoatServer]

__all__ = ['AjaxSpiderServer',
           'BasicSiteServer',
           'DVWAServer',
           'MultiPageServer',
           'MutualTLSServer',
           'PancakesServer',
           'RestApiServer',
           'WebGoatServer',
           'server_classes']
