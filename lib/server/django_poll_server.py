from .host import Host


class DjangoPollServer:

    def __init__(self, run, port=8000):
        self.run = run
        self.port = port
        self.host = Host()

    def start(self):
        self.run('docker run --rm '
                 '--name djangopoll '
                 f'-p {self.port}:8000 '
                 '-v "${PWD}/test/end-to-end/fixtures/djangopoll":/app/djangopoll '
                 '-v "${PWD}/requirements.txt":/app/requirements.txt '
                 '-v "${PWD}/requirements-test.txt":/app/requirements-test.txt '
                 '-d python:3.9.1 '
                 'bash -c "cd /app && pip install -r requirements-test.txt && '
                 'cd djangopoll && rm -f db.sqlite3 && ./manage.py migrate && ./manage.py runserver 0:8000"')

        print(f'Django site server started at http://{self.host.name()}:{self.port}/')
        return self

    def stop(self):
        self.run('docker rm --force djangopoll >/dev/null 2>&1 || true')
        return self
