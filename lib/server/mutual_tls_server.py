from .host import Host


class MutualTLSServer:

    def __init__(self, run, secure_port=80):
        self.run = run
        self.secure_port = secure_port
        self.host = Host()

    def start(self):
        self.run('docker run --rm '
                 '--name mutual_tls_server '
                 f'-p {self.secure_port}:443 '
                 '-v "${PWD}/test/end-to-end/fixtures/mutual-tls/src":/usr/share/nginx/html:ro '
                 '-v "${PWD}/test/end-to-end/fixtures/mutual-tls/nginx.conf":/etc/nginx/conf.d/default.conf '
                 '-v "${PWD}/test/end-to-end/fixtures/mutual-tls/certs":/etc/nginx/certs '
                 '-d nginx:1.17.6')

        print(f'Mutual TLS server started at https://{self.host.name()}:{self.secure_port}/')
        return self

    def stop(self):
        self.run('docker rm --force mutual_tls_server >/dev/null 2>&1 || true')
        return self
